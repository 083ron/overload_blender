import sys
import os
import argparse
from overload.obj2dmesh import load_obj, write_dmesh, TRIS, AUTO, POLYS

parser = argparse.ArgumentParser(description='OBJ to DMESH converter')
parser.add_argument('infile', help="OBJ input file")
parser.add_argument('-s', '--style', default="tris", action="store",
                    help="Style of OBJ expected: tris/polys/auto (tris expects a pre-triangulated OBJ. Auto is default and uses tris if possible, otherwise polys.")
parser.add_argument('--scale', default=1.0, action="store", type=float,
                    help="Scale factor for conversion")
parser.add_argument('-o', '--output', default="", action="store",
                    help="Output file. Defaults to originalname_obj.dmesh, in the same directory as the input.")
args = parser.parse_args(sys.argv[1:])

if args.style == "tris":
    style = TRIS
elif args.style == "both":
    style = AUTO
else:
    style = POLYS
obj = load_obj(args.infile)
write_dmesh(obj, style=style, scale=args.scale, outputfile=args.output)
