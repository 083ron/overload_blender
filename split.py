from __future__ import print_function

import sys

#import pdb
#pdb.set_trace()

if "win" in sys.platform:
    import hsplit.glworkaround

from hsplit.config import config
import sys, os
import json
import argparse
import hsplit.ui

if __name__ == "__main__":

    ltex = config['LEVEL_TEXTURES']
    #ltex = os.path.expandvars(ltex)

    #if ltex.startswith('"'):
    #    ltex = ltex[1:-1]
    
    #if ltex == "%LEVEL_TEXTURES%":
    #    print ("\nNo texture path set. Please either edit config.json",
    #           "\nand set LEVEL_TEXTURES to the LevelTextures folder",
    #           "\nin your overload installation, or set an",
    #           "\nenvironment variable point to the LevelTextures folder")
    #    sys.exit(1)
        
    if not os.path.exists(os.path.join(
            ltex, "Ice_01a.png")):
        print ("\nWARNING: Overload textures not found in location:",
               "\n\n  %s\n"%config['LEVEL_TEXTURES'],
               "\nPlease either edit %LOCALAPPDATA%/Overload_Splitter/config.json",
               "\nand set LEVEL_TEXTURES to the LevelTextures folder",
               "\nin your overload installation.")
    
    parser = argparse.ArgumentParser(description="Splitter")
    parser.add_argument('infile', nargs='?', help=".overload file to alter" )
    parser.add_argument('-l', '--label', action="store_true", help="Show labels")
    parser.add_argument('-u', '--unreal', action="store_true", help="Unreal style movement (overload is default)")

    args = parser.parse_args(sys.argv[1:])
    hsplit.config.LEVEL_TEXTURES = ltex

    hsplit.ui.build_ui(args)
    
    
