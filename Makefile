#src = $(wildcard *.c)

OBJ_PATH:=obj_models
DMESH_PATH:=dmeshes

SRC:=$(wildcard $(OBJ_PATH)/*.obj)
DMESH = $(patsubst $(OBJ_PATH)/%.obj, $(DMESH_PATH)/%_obj.dmesh, $(SRC))
#$(info $$SRC is [${SRC}])
#$(info $$DMESH is [${DMESH}])

.DELETE_ON_ERROR:

all: myprog

myprog: $(DMESH)

$(DMESH_PATH)/%_obj.dmesh: $(OBJ_PATH)/%.obj
	(python obj2dmesh.py --style auto -o $@ $<) || { mv $@ $@.failed; exit 1; }
