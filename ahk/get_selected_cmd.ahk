GetSelected() {
   WinGetText, txt,, Show Tunnel Builder,
   fp := InStr(txt, "Selected")
   v := SubStr(txt, fp+10, 50)
   fp2 := InStr(v, "OUTPUT")
   v2 := SubStr(v, 1, fp2-1)
   bits := StrSplit(v2,"/"," `r`n")
   return bits
}



#v::
{
tx := GetSelected()
WinActivate, Command Prompt
Send v
Send % tx[3]
}
return

#s::
{
tx := GetSelected()
WinActivate, Command Prompt
Send g
Send % tx[1]
Send s
Send % tx[2]
}
return

#g::
{
tx := GetSelected()
WinActivate, Command Prompt
Send g
Send % tx[1]
}
return