Overload Editor Addons
======================
This is a growing collection of extra tools for authoring overload maps:

 - obj2dmesh.py is a standalone obj to dmesh converter, run from the commandline.
 - split.py is a GUI companion tool for the Overload Level Editor that supports some extra snapping, splitting, and UV alignment tools
 - Blender addons:
   - for editing texture alignment on the walls of an overload level.
   - for exporting polygon models directly to overload decals (.dmesh).

Neither obj2dmesh.py or split.py require using blender.

Installation
------------

### Command line tools:
Download or clone the whole repository, and start the python scripts from the top level (obj2dmesh.py, split.py etc.)
The splitter tool will also require you to have Pillow and pyOpenGL installed, both of these should install easily using pip:
     python -m pip install Pillow
     python -m pip install pyopengl

### Blender Addons:
Download the overload folder and place it in your blender addons folder (Usually C:/Program Files/Blender Foundation/Blender/&lt;version number&gt;/scripts/addons).
Open Blender and enable the addon in File->User Preferences, under the Import-Export Category find 'Overload' and enable it.
By expanding the addon entry, you can give it the path for finding the overload texture folders, or it will guess a default location (assumes a standard steam library location).

### Both:
 - Install the overload subfolder into the blender addons folder as described above.
 - Put the top level python scripts (obj2dmesh.py, split.py etc.) wherever you like putting python scripts.
 - Add the blender addons folder (ie the one containing the overload folder) to your PYTHONPATH environment variable.

### Minimal OBJ2DMESH only install:
Download `obj2dmesh.py`, `overload/__init__.py`, `overload/obj2dmesh.py`, `overload/dmesh_template_strings.py` and `overload/texturelist.py`, keeping the folder structure the same:

     your_folder
       +-obj2dmesh.py
       +-overload
          +-__init__.py
          +-dmesh_template_strings.py
          +-obj2dmesh.py
          +-texturelist.py
	 
Use
---

### Blender texture alignment addon
This addon will load marked sides from an overload file, and load them into blender.

Once installed and enabled (see installation above), under the Import menu, there will be 'Import Overload Marked Sides'. In the overload editor, mark your sides and save the file. Import this file in blender, and the sides are read in. Overload scale is quite large so your imported geometry might be far away!

Edit texture maps with blender UV mapping tools.

'Export->Export back to previous Overload File' will write back to the overload file originally imported. A backup is written to a 'backups' folder, with a numbered .ovlbak extension. 9 Backups are maintained and are discarded automatically. Re-open this in the overload editor to see the modified UVs.


### Blender direct DMesh export
Once installed and enabled (see installation above), under the Export menu, use 'Export Overload DMesh File'.

The blender model should use the old blender renderer (not cycles/evee). It exports all meshes in the current blender file into one obj (although you can mark some to be skipped, see naming convention below). Each mesh can have multiple materials, and each material must be given a single texture slot, with a png file from the DecalTextures or LevelTextures folder in the overload level editor install location.

There is a naming convention for objects, if an object contains the following as a substring:

  - `collision`: This object will be invisible, but solid. Use for simplified collision volumes.
  - `noclip`: This object will be visible but non-solid. Use for complex geometry inside a collision volume.
  - `skip`: This object will not be exported to dmesh at all.
  - default: everything else is visible and solid.

![Object naming convention](images/object_naming.png "Object material naming convention")

### obj2dmesh command line tool (obj2dmesh.py)
Useful if you have another source of obj files and don't have blender.

     usage: obj2dmesh.py [-h] [-s STYLE] [-o OUTPUT] infile

     OBJ to DMESH converter

     positional arguments:
       infile                OBJ input file

     optional arguments:
       -h, --help            show this help message and exit
       -s STYLE, --style STYLE
                             Style of OBJ expected: tris/polys/auto (tris expects a
                             pre-triangulated OBJ. Auto is default and uses tris if
                             possible, otherwise polys.
       -o OUTPUT, --output OUTPUT
                             Output file. Defaults to originalname_obj.dmesh, in
                             the same directory as the input.

### Splitter tool (split.py)
Install Pillow and pyopengl as described above.

Run at the command line with

     python split.py yourmap.overload

View controls are the same as the overload level editor:

 - Alt+LMB rotates the view.
 - Alt+MMB slides left/right/up/down.
 - Mouse Wheel zooms.

Run with the -u flag to get behaviour similar to old versions of unrealEd:

 - LMB+drag moves forward/back and rotates left/right.
 - RMB+drag rotates on the spot in all directions.
 - LMB+RMB+drag slides left/right/up/down.

Selection works much the same as OLEs default 'visible' selection mode, with the following differences:

 - Sides are numbered explicitly, not as segment/side pairs.
 - Edges are numbered and selectable/markable
 - Clicking several times won't yet cycle the choices
 - No box selection yet

Keyboard shortcuts implemented so far:

 |     |     |
 | --- | --- |
 | Space         | Mark selected                           |
 | Shift-space   | Mark/unmark all                         |
 | c/Shift-c     | Cycle through all segments              |
 | n/Shift-n     | Next/prev segment (across current side) | 
 | x/Shift-x     | Next/prev side in surrent segment       |
 | q             | Mark all connected with same texture    |
 | Shift-q       | Mark all coplanar                       |
 | Tab/Shift-Tab | Cycle selection mode                    |

File Menu:

 - Open... : Open a .overload file from disk.
 - Reload  : Re-opens current .overload (e.g. if you have it open in OLE and save there)
 - Save    : Saves current .overload file
 - Save As...  : Saves a copy of current .overload file.
 - Set Texture Folder : Set the location of the OLE LevelTextures folder
 - Exit    : Exits the program

Edit Menu:

 - Square up Marked UVs : Sets all marked faces to have a square UV map

Debug Menu:

 - Dump Backbuffer : Outputs the backbuffer used for picking into a .png file


Tool buttons on left-hand panel:

 |     |     |
 | --- | --- |
 | Split Tool         | Click to activate, then hover over any edge to preview a split. Click to make the split |

Geometry edit tools:

Preview shows green lines connecting old and new vertex locations. Apply makes the change permanent.

Project Marked Verts to Selected Side - projects all the marked vertices onto the plane defined by the selected side. The vertices don't need to fall inside the side - the plane is extended outside it. Only three vertices are used to define the plane, so if the side is non-planar the plane may not be the one you want.
![Project Marked Verts to Selected Side](images/project_verts.png "Project Marked Verts to Selected Side")

Project Marked Sides to Selected Side
Same as above but with marked sides instead.
![Project Marked Sides to Selected Side](images/project_sides.png "Project Marked Verts to Selected Side") 

Project Marked Sides to Selected Side along Selected Edge
The selected edge constrains the projection direction. All vertices in the marked sides slide in the direction of the selected edge until they hit the selected side.
![Project Marked Sides to Selected Side along Selected Edge](images/project_by_edge.png "Project Marked Verts to Selected Side along Selected Edge")

General geometry edit tool:

Takes two text inputs to build more complex edits:

 - New Location: A string defining some computed vertex locations
 - Objects to move: The vertices to move to the locations in 'New Location'

Example - set New Location to the string 'vs' and Objects to move to 'vm' to move a single marked vertex to the position of the selected vertex.

The letters 'v', 'e', 's' and 'g' can be used to refer to verts, edges, sides and segments. Follow it with 'm' for marked, 's' for selected, or a number.

Operators can be used to combine objects as follows:

|     |     |
| --- | --- |
| `\|` | Projects left hand side onto right. Right hand side can be another object, or a python tuple containing an object and a constraining axis |
| `^` | Intersection. |
| `-` | Builds a 'displacement' object, representing the vector between two verts |
| `+` | Adds a displacement to another object |

Functions for building lines/planes etc out of other objects:

| Function |     |
| -------- | --- |
| `P( )`   | Takes 3 vertices, or a single side and builds an infinite plane |
| `L( )`   | Takes 2 verts and builds an infinte line. The defining verts are stored, see D() below. |
| `D( )`   | Builds a displacement (a vector of certain length). If given two verts, an edge or an infinite line, builds a displacement between the two verts or line defining verts. If given a side or an infinite plane, builds a displacement representing the plane normal. |
| `R( )`   | Rotation function: <br>R(object, axis, degrees) - rotate around given axis<br>R(object, origin, axis, degrees) - rotate using axis direction, but with a different origin point (doesn't have to be on the axis, the axis position doesn't matter, just the direction).<br>R(object, axis, start_vert, end_vert) - rotate around axis, until start_vert reaches end_vert<br>R(object, origin, axis, start_vert, end_vert) - Same as above with another origin|

There are also built in objects called 'X', 'Y', and 'Z' representing the coordinate axes.

Lastly the axis checkboxes for X, Y and Z restrict the updates made by this tool to the axes in question.

Warning - currently the axes don't match OLE's coordinate system - the Z axis is 'up', not OLEs default of the Y axis.

Examples:

| New Location &nbsp; &nbsp; &nbsp; &nbsp;   | Objects to move | Axes | Result |
| ---   | --- | --- | --- |
| `vs`  | `vm`  | XY  | Snaps marked verts X and Y coords to match selected vertex, leaving Z unchanged |
| `vs`  | `vm`  | Z   | Snaps marked verts to Z plane of selected vertex. |
| `vm\|ss` | `vm` | XYZ | Projects marked vertices to selected side (equivalent to the Geometry edit tool described above) |
| `sm\|ss` | `sm` | XYZ | Projects marked sides to selected side (equivalent to the Geometry edit tool described above) |
| `sm\|(ss,es)` | `sm` | XYZ | Projects marked sides to selected side, constrained along selected edge (equivalent to the Geometry edit tool described above) |
| `sm\|(ss,L(v123,v456))` | `sm` | XYZ | Projects marked sides to selected side, constrained along the lines connecting verts 123 and 456 |
| `s1^s2^s3` | `vs` | XYZ | Moves selected vertex to intersection point of sides 1,2 and 3 |
| `gm + (vs - vm)` | `gm` | XYZ | Moves marked segments by the distance separating the selected vertex and a single marked vertex (snap a whole section of level onto another) |
| `R(gs, vs, es, 45)` | `gs` | XYZ | Rotates selected segment 45 degrees around an axis in the direction of the selected edge, passing through the selected vertex |