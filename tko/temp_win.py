from ctypes import c_int, c_uint, Structure, windll, sizeof, byref, pointer, c_float
from ctypes.wintypes import WINFUNCTYPE, HWND, WPARAM, LPARAM, HANDLE, LPCWSTR, MSG

from win32_gdi import get_dc

from OpenGL.WGL.ARB.pixel_format import wglChoosePixelFormatARB, WGL_DRAW_TO_WINDOW_ARB, \
    WGL_SUPPORT_OPENGL_ARB, WGL_COLOR_BITS_ARB, WGL_RED_BITS_ARB, WGL_GREEN_BITS_ARB, WGL_BLUE_BITS_ARB, \
    WGL_ALPHA_BITS_ARB, WGL_DEPTH_BITS_ARB, WGL_STENCIL_BITS_ARB, WGL_DOUBLE_BUFFER_ARB, WGL_PIXEL_TYPE_ARB, \
    WGL_TYPE_RGBA_ARB

from OpenGL.WGL.ARB.multisample import WGL_SAMPLE_BUFFERS_ARB, WGL_SAMPLES_ARB

from win32_gdi import PFD_DRAW_TO_WINDOW, PFD_SUPPORT_OPENGL, PFD_DOUBLEBUFFER, PFD_TYPE_RGBA, \
    PixelFormatDescriptor, get_dc, choose_pixel_format, set_pixel_format, \
    get_pixel_format, swap_buffers, release_dc
from tko.ogl_hdr import wglCreateContext, wglDeleteContext, wglMakeCurrent, GL_TRUE


WNDPROCTYPE = WINFUNCTYPE(c_int, HWND, c_uint, WPARAM, LPARAM)

WS_EX_APPWINDOW     = 0x40000
WS_OVERLAPPEDWINDOW = 0xcf0000
WS_CAPTION          = 0xc00000
WS_POPUP            = 0x80000000
WS_DISABLED         = 0x08000000

SW_SHOWNORMAL = 1
SW_SHOW = 5

CS_HREDRAW = 2
CS_VREDRAW = 1

CW_USEDEFAULT = 0x80000000

WM_DESTROY = 2

WHITE_BRUSH = 0

class WNDCLASSEX(Structure):
    _fields_ = [("cbSize", c_uint),
                ("style", c_uint),
                ("lpfnWndProc", WNDPROCTYPE),
                ("cbClsExtra", c_int),
                ("cbWndExtra", c_int),
                ("hInstance", HANDLE),
                ("hIcon", HANDLE),
                ("hCursor", HANDLE),
                ("hBrush", HANDLE),
                ("lpszMenuName", LPCWSTR),
                ("lpszClassName", LPCWSTR),
                ("hIconSm", HANDLE)]

def PyWndProcedure(hWnd, Msg, wParam, lParam):
    if Msg == WM_DESTROY:
        windll.user32.PostQuitMessage(0)
    else:
        return windll.user32.DefWindowProcW(hWnd, Msg, wParam, lParam)
    return 0
  


def ms_pf():
    WndProc = WNDPROCTYPE(PyWndProcedure)
    hInst = windll.kernel32.GetModuleHandleW(0)
    wclassName = 'TempWindowClass'
    wname = 'TempWindow'
        
    wndClass = WNDCLASSEX()
    wndClass.cbSize = sizeof(WNDCLASSEX)
    wndClass.style = CS_HREDRAW | CS_VREDRAW
    wndClass.lpfnWndProc = WndProc
    wndClass.cbClsExtra = 0
    wndClass.cbWndExtra = 0
    wndClass.hInstance = hInst
    wndClass.hIcon = 0
    wndClass.hCursor = 0
    wndClass.hBrush = windll.gdi32.GetStockObject(WHITE_BRUSH)
    wndClass.lpszMenuName = 0
    wndClass.lpszClassName = wclassName
    wndClass.hIconSm = 0
    
    regRes = windll.user32.RegisterClassExW(byref(wndClass))
    
    hWnd = windll.user32.CreateWindowExA(
        0,wclassName,wname,
        WS_POPUP | WS_DISABLED,
        CW_USEDEFAULT, CW_USEDEFAULT,
        300,300,0,0,hInst,0)

    if not hWnd:
        print('Failed to create window')
        exit(0)

    #print('ShowWindow', windll.user32.ShowWindow(hWnd, SW_SHOW))
    #print('UpdateWindow', windll.user32.UpdateWindow(hWnd))

    msg = MSG()
    lpmsg = pointer(msg)

    n = 0

    while windll.user32.GetMessageA(lpmsg, 0, 0, 0) != 0:
        n+=1
        windll.user32.TranslateMessage(lpmsg)
        windll.user32.DispatchMessageA(lpmsg)
        if n > 3: break

    hdc = get_dc(hWnd)
    
    pfd = PixelFormatDescriptor()
    pfd.nSize = sizeof(PixelFormatDescriptor)
    pfd.nVersion = 1
    pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER
    pfd.iPixelType = PFD_TYPE_RGBA
    pfd.cColorBits = 24
    pfd.cDepthBits = 16

    pixel_format = choose_pixel_format(hdc, pfd)

    print("ChoosePixelFormat(tmp) returned", pixel_format)

    print("SetPixelFormat(tmp) returned", set_pixel_format(hdc, pixel_format, pfd))

    print("GetPixelFormat(tmp) returned", get_pixel_format(hdc), "!!!\n")

    rc = wglCreateContext(hdc)
    if rc:
        wglMakeCurrent(hdc, rc)
        
        iattr = [WGL_DRAW_TO_WINDOW_ARB, GL_TRUE,
         WGL_SUPPORT_OPENGL_ARB, GL_TRUE,
         WGL_COLOR_BITS_ARB, 24,
         WGL_RED_BITS_ARB, 8,
         WGL_GREEN_BITS_ARB, 8,
         WGL_BLUE_BITS_ARB, 8,
         WGL_ALPHA_BITS_ARB, 0,
         WGL_DEPTH_BITS_ARB, 16,
         WGL_STENCIL_BITS_ARB, 0,
         WGL_DOUBLE_BUFFER_ARB, GL_TRUE,
         WGL_PIXEL_TYPE_ARB, WGL_TYPE_RGBA_ARB,
         WGL_SAMPLE_BUFFERS_ARB, GL_TRUE,
         WGL_SAMPLES_ARB, 16,
         0]
        iattr_c = (c_int * len(iattr))(*iattr)
       
        fattr = [0,0]
        fattr_c = (c_float * len(fattr))(*fattr)

        pformats = (c_int * 16)()
        nformats = c_uint(16)
        
        wglChoosePixelFormatARB(hdc,
                                iattr_c,
                                None,
                                nformats,
                                pformats,
                                nformats)
        
        wglMakeCurrent(hdc, None)
        wglDeleteContext(rc)

    release_dc(hWnd,hdc)
    windll.user32.DestroyWindow(hWnd)

    n = 0
    print("before messages")
    while windll.user32.GetMessageA(lpmsg, 0, 0, 0) != 0:
        n+=1
        windll.user32.TranslateMessage(lpmsg)
        windll.user32.DispatchMessageA(lpmsg)
        if n > 3: break

    return pformats      
