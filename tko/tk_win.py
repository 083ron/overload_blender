""" define the minimal skeleton abstract class to create a Tkinter Window to use OpenGL

"""
from __future__ import print_function
from ctypes import sizeof
import sys
from Tkinter import Frame
import traceback
import sys

from tko.temp_win import ms_pf

if sys.platform.startswith('win32'):

    from tko.win32_gdi import PFD_DRAW_TO_WINDOW, PFD_SUPPORT_OPENGL, PFD_DOUBLEBUFFER, PFD_TYPE_RGBA, \
                            PixelFormatDescriptor, get_dc, choose_pixel_format, set_pixel_format, \
                            get_pixel_format, swap_buffers

    from tko.ogl_hdr import wglCreateContext, wglMakeCurrent
    
elif sys.platform.startswith('linux'):

    from tko.x11_gdi import X11_None, x_open_display

    from tko.ogl_hdr import PGLint, GLX_RGBA, GLX_DEPTH_SIZE, GLX_DOUBLEBUFFER, GL_TRUE, \
                            GLX_BLUE_SIZE, GLX_GREEN_SIZE, GLX_RED_SIZE, \
                            glXChooseVisual, glXCreateContext, glXMakeCurrent, glXSwapBuffers

pixel_format = None
    
def get_multisample_pixel_format():
    try:
        print("try")
        mspf = ms_pf()
        print(list(mspf))
        print("MSPF: ", mspf)
        return mspf[0]
    except:
        return 0

class TkOglWin(Frame):

    def __init__(self, parent, *args, **kwargs):
        self.parent = parent
        self.parent.title(kwargs.get('app_title', 'Opengl Test'))
        del kwargs['app_title']
        Frame.__init__(self, parent, *args, **kwargs)
        self.glready=False

        self.bind('<Configure>', self.on_resize)

        self.parent.after(100, self._cfg_tkogl)

    def _cfg_tkogl(self):
        global pixel_format
        if sys.platform.startswith('win32'):

            self.hdc = get_dc(self.winfo_id())

            pfd = PixelFormatDescriptor()
            pfd.nSize = sizeof(PixelFormatDescriptor)
            pfd.nVersion = 1
            pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER
            pfd.iPixelType = PFD_TYPE_RGBA
            pfd.cColorBits = 24
            pfd.cDepthBits = 16

            if pixel_format is None:
                pixel_format = get_multisample_pixel_format()
            default_pixel_format = choose_pixel_format(self.hdc, pfd)

            print("ChoosePixelFormat: ", default_pixel_format)

            #print("SetPixelFormat returned", set_pixel_format(self.hdc, pixel_format, pfd))

            if pixel_format == 0:
                pf = default_pixel_format
            else:
                pf = pixel_format
            
            print("SetPixelFormat: ", set_pixel_format(self.hdc, pf, pfd))

            print("GetPixelFormat: ", get_pixel_format(self.hdc))

            rc = wglCreateContext(self.hdc)
            print("context created")
            wglMakeCurrent(self.hdc, rc)
            self.glready=True

        elif sys.platform.startswith('linux'):

            att = PGLint(
                GLX_RGBA, GLX_DOUBLEBUFFER,
                GLX_RED_SIZE, 4,
                GLX_GREEN_SIZE, 4,
                GLX_BLUE_SIZE, 4,
                GLX_DEPTH_SIZE, 16,
                X11_None
            )

            self.dpy = x_open_display(None)

            vi = glXChooseVisual(self.dpy, 0, att)

            glc = glXCreateContext(self.dpy, vi, None, GL_TRUE)

            glXMakeCurrent(self.dpy, self.winfo_id(), glc)

    def start(self):
        self.set_ortho_view()
        self.parent.after(10, self._render_loop)

    def on_resize(self, event, arg=None):

        raise NotImplementedError

    def _render_loop(self):
        self.render_scene()
        self._swap_buffers()
        self.parent.after(5, self._render_loop)

    def _swap_buffers(self):
        if sys.platform.startswith('win32'):
            swap_buffers(self.hdc)
        elif sys.platform.startswith('linux'):
            glXSwapBuffers(self.dpy, self.winfo_id())
            
    def render_scene(self):

        raise NotImplementedError

    def set_ortho_view(self):

        raise NotImplementedError
