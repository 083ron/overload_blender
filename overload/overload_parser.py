# ***** BEGIN GPL LICENSE BLOCK *****
#
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software Foundation,
# Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
#
# ***** END GPL LICENCE BLOCK *****

import json
import os.path
import os
import re
import glob
from shutil import copyfile

def get_marked_sides(ovl_json):
    """
    Get a list of marked sides
    """
    marked_sides =[]
    segments = ovl_json['segments']
    for segid, seg in segments.items():
        for i, side in enumerate(seg['sides']):
            if side['marked']:
                marked_sides.append((segid, i, side))
    return marked_sides

def get_marked_exterior_sides(ovl_json):
    """
    Get a list of marked sides, eliminating interior sides
    that aren't on an exterior wall
    """
    marked_sides =[]
    segments = ovl_json['segments']
    for segid, seg in segments.items():
        neighbors = seg['neighbors']
        for i, side in enumerate(seg['sides']):
            if side['marked'] and neighbors[i] == -1:
                marked_sides.append((segid, i, side))
    return marked_sides

def get_exterior_sides_of_marked_segments(ovl_json):
    """
    Get a list of sides from the marked segments,
    eliminating interior sides
    that aren't on an exterior wall
    """
    sides = []
    segments = ovl_json['segments']
    for segid, seg in segments.items():
        if seg['marked']:
            neighbors = seg['neighbors']
            for i, side in enumerate(seg['sides']):
                if neighbors[i] == -1:
                    sides.append((segid, i, side))
    return sides

def get_marked_segments(ovl_json):
    return [int(k) for k in ovl_json['segments'] if ovl_json['segments'][k]['marked']==True]

def get_marked_verts(ovl_json):
    return [int(k) for k in ovl_json['verts'] if ovl_json['verts'][k]['marked']==True]

def get_vertex_xyzs(ovl_json, blender_flip=True, scale=1):
    """
    Load vertex XYZ coords. Vertex list in the ovl_json
    is not always contiguous so get the max vertex and
    build a list with the occasional None in
    """
    vertex_ids = [int(v) for v in ovl_json['verts']]
    max_vid = max(vertex_ids)
    vertex_xyzs = [None]*(max_vid+1)

    # Flip y->z since blender is Z up, Overload is Y up
    if blender_flip:
        crd_order = 'xzy'
    else:
        crd_order = 'xyz'
    
    for i in range(max_vid+1):
        if i in vertex_ids:
            vertex_xyzs[i] = tuple([ovl_json['verts'][str(i)][crd]*scale for crd in crd_order])
    return vertex_xyzs

def get_vertex_xyzs_used_by_sides(ovl_json, sides, blender_flip=True, scale=1):
    """
    Load vertex XYZ coords. Vertex list in the ovl_json
    is not always contiguous so get the max vertex and
    build a list with the occasional None in
    """
    vertex_map_ovl_2_internal = {}
    vertex_map_internal_2_ovl = []
    ivertex_number = 0
    for side in sides:
        for vid_s in side[2]['verts']:

            vid = int(vid_s)
            if vid not in vertex_map_ovl_2_internal:
                vertex_map_ovl_2_internal[vid] = ivertex_number
                vertex_map_internal_2_ovl.append(vid)
                ivertex_number += 1
    vertex_xyzs = []

    # Flip y->z since blender is Z up, Overload is Y up
    if blender_flip:
        crd_order = 'xzy'
    else:
        crd_order = 'xyz'

    for i in range(len(vertex_map_internal_2_ovl)):
        ovl_index = vertex_map_internal_2_ovl[i]
        vertex_xyzs.append(tuple([ovl_json['verts'][str(ovl_index)][crd]*scale for crd in crd_order]))

    return vertex_xyzs, vertex_map_ovl_2_internal, vertex_map_internal_2_ovl

def get_backup_folder(filename):
    dr, fl = os.path.split(filename)
    backup_path = os.path.join(dr,"backups")
    if not os.path.exists(backup_path):
        os.mkdir(backup_path)
    return backup_path
        
def write_backup(filename):
    """
    Copies a file into a 'backups' folder in the same folder
    as the target file. Keeps up to 9 old versions and renumbers
    them as new backups are made. The 10th one is discarded.
    """
    if not os.path.exists(filename): return

    backup_folder = get_backup_folder(filename)
    dr, fl = os.path.split(filename)
    fln, ext = os.path.splitext(fl)
    files = glob.glob(os.path.join(dr,"backups",fln+".ovlbak#*"))
    files.sort(reverse=True)
    print(files)
    for bk in files:
        n = int(bk[-1])+1
        if n < 10:
            os.rename(bk, bk[:-1]+"%s"%n)
        else:
            print("DELETED:, %s"%bk)
            os.unlink(bk)

    lastbak = os.path.join(dr, "backups", fln+".ovlbak#1")
    copyfile(filename, lastbak)
    return lastbak
    
def rewrite_uvs(origfile, newfile, replace_uvs):
    """
    Rewrites the UVs into an overload file.
    """
    fp = open(origfile, "r")
    fpout = open(newfile, "w")
    scan_segs = False
    hide = False
    side = 0
    for line in fp:
        if line.strip() == '"segments": {':
            scan_segs = True
        if scan_segs:
            m = re.match('    "(\d+)": {\n', line)
            if m:
                seg = int(m.groups()[0])
                side = -1
            if "uvs" in line:
                side += 1
                if(seg,side) in replace_uvs:
                    newuvs = replace_uvs[(seg,side)]
                    fpout.write('          "uvs" : [\n')
                    ii = 0
                    for n4 in newuvs:
                        ii+=1
                        fpout.write('            {\n')
                        fpout.write('              "u": %f,\n'%n4[0])
                        fpout.write('              "v": %f\n'%n4[1])
                        if ii == 4:
                            fpout.write('            }\n')
                        else:
                            fpout.write('            },\n')
                    hide = True
            if hide and "]" in line:
                hide = False
        if not hide:
            fpout.write(line)

    fp.close()
    fpout.close()

def rewrite_xyzs(origfile, newfile, replace_xyzs):
    """
    Rewrites the UVs into an overload file.
    """

    fp = open(origfile, "r")
    fpout = open(newfile, "w")

    scan_verts = False
    replace = False
    side = 0
    fpout.write("\n")
    for line in fp:
        if line.strip() == '"segments": {':
            scan_verts = False
        if line.strip() == '"verts": {':
            scan_verts = True
        if scan_verts:
            m = re.match(' *"(\d+)": {\n', line)
            if m:
                v = int(m.groups()[0])

                if v in replace_xyzs:
                    newxyz = replace_xyzs[v]
                    replace = True
                else:
                    replace = False

            if replace:
                bits = line.split(":")
                if '"x"' in line:
                    line = "%s: %f,\n"%(bits[0], newxyz[0])
                if '"y"' in line:
                    line = "%s: %f,\n"%(bits[0], newxyz[1])
                if '"z"' in line:
                    line = "%s: %f\n"%(bits[0], newxyz[2])

            if '"z"' in line:
                replace = False
        fpout.write(line)

    fp.close()
    fpout.close()

