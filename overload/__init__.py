# ***** BEGIN GPL LICENSE BLOCK *****
#
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software Foundation,
# Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
#
# ***** END GPL LICENCE BLOCK *****

bl_info = {
    "name": "Overload Importer",
    "author": "Robin Fairey (0beron)",
    "version": (0, 9, 1),
    "blender": (2, 7, 9),
    "location": "File > Import > Overload",
    "description": "Import Overload Files",
    "category": "Import-Export"}

try:
    import bpy
except:
    pass

if 'bpy' in globals():
    from overload.blender_ops import *

    def menu_func(self, context):
        """
        Add overload import to file import menu.
        """
        self.layout.operator_context = 'INVOKE_DEFAULT'
        self.layout.operator(ImportOverload.bl_idname, text="Import Overload Marked Sides")

    def menu_func_rl(self, context):
        self.layout.operator_context = 'INVOKE_DEFAULT'
        self.layout.operator(ReloadOverload.bl_idname, text="Reload Overload Module")

    def menu_func_ex(self, context):
        self.layout.operator_context = 'INVOKE_DEFAULT'
        self.layout.operator(ExportOverload.bl_idname, text="Export back to previous Overload File")

    def menu_func_exdmesh(self, context):
        self.layout.operator_context = 'INVOKE_DEFAULT'
        self.layout.operator(ExportDMesh.bl_idname, text="Export Overload DMesh File")

    def register():
        bpy.utils.register_module(__name__)
        bpy.types.INFO_MT_file_import.append(menu_func)
        bpy.types.INFO_MT_file_import.append(menu_func_rl)
        bpy.types.INFO_MT_file_export.append(menu_func_ex)
        bpy.types.INFO_MT_file_export.append(menu_func_exdmesh)

    def unregister():
        bpy.utils.unregister_module(__name__)
        bpy.types.INFO_MT_file_import.remove(menu_func)
        bpy.types.INFO_MT_file_import.remove(menu_func_rl)
        bpy.types.INFO_MT_file_export.remove(menu_func_ex)
        bpy.types.INFO_MT_file_export.remove(menu_func_exdmesh)

        
if __name__ == "__main__":
    register()
    
