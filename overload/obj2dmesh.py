# ***** BEGIN GPL LICENSE BLOCK *****
#
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software Foundation,
# Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
#
# ***** END GPL LICENCE BLOCK *****

import sys
from collections import namedtuple
import os
import argparse
from overload.dmesh_template_strings import *
from overload.texturelist import texturelist

class ObjError(Exception):
    pass

ObjFile = namedtuple('ObjFile', ['v','vn','vt','f','materials','filename','maxpolysize'])
Face = namedtuple('Face', ['verts','material', 'flag'])

DEFAULT_MATERIALS = ['solid_red', 'solid_aqua', 'solid_bright_blue', 'solid_cyan', 'solid_purple', 'solid_white', 'solid_blue', 'solid_bright_yellow', 'solid_orange', 'solid_yellow']
NUM_DEFAULTS = len(DEFAULT_MATERIALS)
TRIS, POLYS, AUTO = 0,1,2
GEOM, NOCLIP, COLLISION, SKIP = 0,1,2,3

def objindex(st):
    if st == '':
        return None
    else:
        return int(st)-1
    
def load_mtllib(mtlfilename, objfilename):
    """
    Load the material library if the obj file references one. Extract the
    mapping from material name to the png file for the texture. All the
    dmesh files need are the name of the png file, not the whole path.
    """
    mtls = {}

    objdir = os.path.dirname(os.path.abspath(objfilename))
    try:
        mtl = open(os.path.join(objdir,mtlfilename), "r")
    except:
        print("Material file %s not found"%mtlfilename)
        return mtls

    idefault = 1
    
    mtlname = None
    mtltex = None
    
    for line in mtl:
        bits = line.strip().split()
        if len(bits) < 1:
            continue
        if bits[0] == "newmtl":
            # Starting a new material - record the current one before parsing this one.
            # This is needed if eg material 1 has a name but no texture file.
            record_mtl_pair(mtls, mtlname, mtltex, idefault)
            if len(bits) < 2:
                continue
            # Get the name for this material
            mtlname = bits[1]
            mtltex = None
        elif bits[0] == "map_Kd":
            # Get the texture path
            mtltex = None
            if len(bits) >= 2:
                path = line[7:]
                print(path)
                pngfile = os.path.split(path)[1].strip()
                if pngfile in texturelist:
                    mtltex = os.path.splitext(pngfile)[0]
                else:
                    print("Warning: Texture [%s] not found in stock texture list. Placeholder used."%pngfile)
            record_mtl_pair(mtls, mtlname, mtltex, idefault)
            mtlname = None
            mtltex = None

    mtl.close()
    # Record pairs one last time, in case we hit end of file without a final texture file
    record_mtl_pair(mtls, mtlname, mtltex, idefault)
    print('Material summary:')
    for k,v in mtls.items():
        print(k, '->', v)
    return mtls

def record_mtl_pair(mtls, mtlname, mtltex, idefault):
    if mtlname is None:
        return

    if mtltex is None:
        idefault += 1
        print("Placeholder texture used")
        if idefault >= NUM_DEFAULTS:
            idefault = NUM_DEFAULTS-1
        mtltex = DEFAULT_MATERIALS[idefault]

    mtls[mtlname] = mtltex

def load_obj(objfilename):
    """
    Loads an obj file, and extracts vertex positions, normals, uvs and materials.
    """
    obj = open(objfilename, "r")

    v = []  # Vertex coords
    vn = [] # Vertex normals
    vt = [] # Vertex texture coords (UVs)
    arrays = {'v':v,'vt':vt,'vn':vn}

    materials = set()
    mtls = {}
    faces = []

    currmtl = "None"
    currflag = GEOM

    maxpolysize = 0

    accum_line = ""
    for partial_line in obj:
        
        if partial_line.strip().endswith("\\"):
            end = partial_line.rindex("\\")
            if end > 0:
                accum_line+= partial_line[:end-1]
            continue
        else:
            accum_line+= partial_line
            line = accum_line
            accum_line = ""
            
        bits = line.split()
        if len(bits) < 1:
            continue
        if bits[0] == 'o':
            # Start of a new object. Use a naming convention to apply flags
            if 'collision' in bits[1].lower():
                currflag = COLLISION
            elif 'noclip' in bits[1].lower():
                currflag = NOCLIP
            elif 'skip' in bits[1].lower():
                currflag = SKIP
            else:
                currflag = GEOM
            continue
        elif bits[0] == 'v' or bits[0] == 'vn':
            # Vertex position or normal - convert to float and store
            coords = [float(f) for f in bits[1:]]
            coords[2] = -coords[2]
            arrays[bits[0]].append(tuple(coords))
        elif bits[0] == 'vt':
            # UV - convert to float and store
            uv = [float(f) for f in bits[1:]]
            uv[1] = -uv[1]
            arrays[bits[0]].append(tuple(uv))
        elif bits[0] == 'mtllib':
            # Reference to mtl file - open it up and load the contents
            mtls = load_mtllib(bits[1], objfilename)
        elif bits[0] == 'usemtl':
            print(bits)
            # Material to use for subsequent tris/polys. Look it up in
            # the material library to find the relevant texture PNG
            currmtl = bits[1]
            if currmtl == "None":
                mtls['None'] = "_default"

            if currmtl in mtls:
                currmtl = mtls[currmtl]
                materials.add(currmtl)
            else:
                print("Export used unknown material: ", currmtl)

        elif bits[0] == 'f':
            if currflag == SKIP:
                continue
            # Face (a poly or a tri)
            fvs = [tuple([objindex(a) for a in fv.split('/')]) for fv in bits[1:]]
            faces.append(Face(verts=fvs[::-1], material = currmtl, flag = currflag))
            maxpolysize = max(maxpolysize, len(fvs))

    print(materials, mtls)
    if vn == []:
        vn = [(0.0,1.0,0.0)]
    if vt == []:
        vt = [(0.0,0.0)]       
    return ObjFile(v,vn,vt,faces,list(materials),objfilename, maxpolysize)


def consume_trailing_comma(fp):
    fp.seek(fp.tell()-1)


def write_dmesh(objfile, style=TRIS, scale=1.0, outputfile=""):
    """
    Writes an ObjFile object into an overload .dmesh file.
    """
    if outputfile == "":
        filename = os.path.splitext(objfile.filename)[0]+"_obj.dmesh"
        
    else:
        filename = outputfile
        
    fp = open(filename, "w")
    
    if objfile.maxpolysize == 3:
        if style == AUTO:
            style = TRIS
    else:
        if style == AUTO:
            style = POLYS
        elif style == TRIS:
            raise ObjError("Triangle conversion, but obj has polys." +
                           "Triangulate the obj or use --style polys.")

    fp.write(VERTS_HEADER)

    for v in objfile.v:
        vscaled = tuple([c*scale for c in v])
        fp.write(VERT%vscaled)
    consume_trailing_comma(fp)

    fp.write(VERTS_FOOTER)

    if style == POLYS:
        fp.write(POLYS_HEADER)
    else:
        fp.write(EMPTY_POLYS_HEADER)
        fp.write(TRIS_HEADER)

    materials = objfile.materials
        
    for i,f in enumerate(objfile.f):
        if f.material not in objfile.materials:
            materials = ['_default'] + objfile.materials
            break
        
    for i,f in enumerate(objfile.f):
        fp.write(POLY_HEADER%i)
        if style == POLYS:
            write_poly_props(objfile, materials, f, fp)
        else:
            write_tri_props(objfile, materials, f, fp)
        write_poly_normals(objfile,f,fp)
        write_poly_uvs(objfile,f,fp)       
        fp.write(POLY_FOOTER)

    consume_trailing_comma(fp)
    fp.write(POLYS_FOOTER)

    fp.write(TEXNAMES_HEADER)
    for m in materials:
        fp.write(TEXNAME%m)
    consume_trailing_comma(fp)
    fp.write(TEXNAMES_FOOTER)

    fp.write(LIGHTS_COLORS_ETC)
    return filename

def write_poly_props(objfile, materials, f, fp):
    if f.material not in materials:
        txindex = 0
    else:
        txindex = materials.index(f.material)

    num_verts = len(f.verts)
    vertlist = ",".join([str(v[0]) for v in f.verts])
    fp.write(POLY_PROPS%(txindex, f.flag, num_verts, vertlist))

def write_tri_props(objfile, materials, f, fp):
    if f.material not in materials:
        txindex = 0
    else:
        txindex = materials.index(f.material)

    if len(f.verts) != 3:
        raise ObjError("Triangle conversion, but obj has polys." +
                       "Triangulate the obj or use --style polys.")
    vertlist = ",".join([str(v[0]) for v in f.verts])
    fp.write(TRI_PROPS%(txindex, f.flag, vertlist))
    
def write_poly_normals(objfile,f, fp):
    fp.write(NORMAL_HEADER)
    for v in f.verts:
        if len(v) > 2:
            ind = v[2] or 0
        else:
            ind = 0
        coords = objfile.vn[ind][:3]
        fp.write(NORMAL%coords)
    consume_trailing_comma(fp)
    fp.write(NORMAL_FOOTER)


def write_poly_uvs(objfile,f, fp):
    fp.write(UVS_HEADER)
    for v in f.verts:
        if len(v) > 1:
            ind = v[1] or 0
        else:
            ind = 0
        uv = objfile.vt[ind][:2]
        fp.write(UV%uv)
    consume_trailing_comma(fp)
    fp.write(UVS_FOOTER)


