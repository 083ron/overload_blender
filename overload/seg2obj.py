import sys
from collections import defaultdict, namedtuple
import os
import argparse
import overload.overload_parser
from overload.obj_template_strings import *
from overload.texturelist import texturelist
import hsplit.vmath as vmath

from overload.seg2obj_config import config

class ObjError(Exception):
    pass

def write_obj(orig_filepath, ovl_json, marked_segs, marked_sides, outputfile=""):
    """
    Writes a .overload json structure into an obj file
    """
    if outputfile == "":
        filestub = os.path.splitext(orig_filepath)[0]
        filename = filestub+"_segs.obj"
    else:
        filestub = os.path.splitext(outputfile)[0]
        filename = outputfile
        
    if marked_sides:
        sides = overload.overload_parser.get_marked_exterior_sides(ovl_json)
    elif marked_segs:
        sides = overload.overload_parser.get_exterior_sides_of_marked_segments(ovl_json)

    msg = "Converted %d sides"%len(sides)
        
    mtl_sides = defaultdict(list)
    for i, side in enumerate(sides):
        mtl_sides[side[2]['tex_name']].append(i)

    mtl_filename = filestub+"_segs.mtl"
        
    fp_mtl = open(mtl_filename, "w")
    fp_mtl.write("# Seg2Obj MTL File: '%s'\n"%filename)
    fp_mtl.write("# Material Count: %d\n"%len(mtl_sides))
    for mtl in mtl_sides.keys():
        fp_mtl.write(MATERIAL_DEF%(mtl, os.path.join(config['LEVEL_TEXTURES'],mtl+".png")))
    fp_mtl.close()

    fp = open(filename, "w")
    
    fp.write('# Seg2Obj (Overload level geometry as obj)\n')
    fp.write('mtllib %s\n'%mtl_filename)
    fp.write('o Segments\n')
   
    vertex_xyzs, vertex_map_ovl_2_internal, vertex_map_internal_2_ovl = \
            overload.overload_parser.get_vertex_xyzs_used_by_sides(ovl_json, sides, blender_flip=False, scale=1)
    
    for i in range(len(vertex_map_internal_2_ovl)):
        json_vert = ovl_json['verts'][str(vertex_map_internal_2_ovl[i])]
        jvf = [float(json_vert[c]) for c in 'xyz']
        jvf[0] = -jvf[0]
        fp.write('v %g %g %g\n'%tuple(jvf))
    
    vts = []
    vns = []
    polys = []

    for mtl, side_ids_for_mtl in mtl_sides.iteritems():
        for mside_id in side_ids_for_mtl:
            mside = sides[mside_id][2]
            vts += [(uv['u'],-uv['v']) for uv in mside['uvs']]

            xyzs = [vertex_xyzs[vertex_map_ovl_2_internal[int(vid)]] for vid in mside['verts'][:3]]

            tp = vmath.normalise(vmath.plane_normal(xyzs))
            tp[0] = -tp[0]
            vns.append(tuple(tp))

    for vt in vts:
        fp.write('vt %g %g\n'%vt)

    for vn in vns:
        fp.write('vn %g %g %g\n'%vn)

    icount = 0
    for mtl, side_ids_for_mtl in mtl_sides.iteritems():
        fp.write('usemtl %s\n'%mtl)
        fp.write('s off\n')
        for mside_id in side_ids_for_mtl:
            mside = sides[mside_id][2]
            fp.write('f ')
            local_verts = [vertex_map_ovl_2_internal[int(vid)]+1 for vid in mside['verts']]
            poly_corners = reversed(zip(local_verts, range(icount*4+1, icount*4+5), [icount+1] *4))
            for corner in poly_corners:
                fp.write('%d/%d/%d '%corner)
            fp.write('\n')
            icount += 1

    return filename, msg

