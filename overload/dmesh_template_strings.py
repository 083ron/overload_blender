VERTS_HEADER = """{
  "verts": ["""

VERT = """
    {
      "x": %.8f,
      "y": %.8f,
      "z": %.8f
    },"""
               
VERTS_FOOTER = """
  ],"""

POLYS_HEADER = """
  "polys": {"""

POLY_HEADER = """
    "%d": {"""

EMPTY_POLYS_HEADER = """
  "polys": {},"""

POLY_PROPS= """
      "tex_index": %d,
      "flags": %d,
      "smoothing": 0,
      "num_verts": %d,
      "verts": [%s],"""

TRIS_HEADER = """
  "triangles": {"""

TRI_PROPS = """
      "tex_index": %d,
      "flags": %d,
      "verts": [%s],"""


NORMAL_HEADER = """
      "normals": ["""

NORMAL = """
       {
         "x": %.8f,
         "y": %.8f,
         "z": %.8f
       },"""

NORMAL_FOOTER = """
     ],"""

UVS_HEADER = """
      "uvs": ["""

UV = """
       {
         "u": %.8f,
         "v": %.8f
       },"""

UVS_FOOTER = """
      ]
"""

POLY_FOOTER = """
  },"""

POLYS_FOOTER = """
 },"""

TEXNAMES_HEADER = """
   "tex_names": ["""

TEXNAME = """
   "%s","""

TEXNAMES_FOOTER = """
  ],"""

LIGHTS_COLORS_ETC = """
      "lights": {
    "0": {
      "enabled": false,
      "style": "POINT",
      "flare": "NONE",
      "position": [
        0.0,
        0.0,
        0.0
      ],
      "rot_yaw": 0.0,
      "rot_pitch": 0.0,
      "color_index": 0,
      "intensity": 1.0,
      "range": 10.0,
      "angle": 45.0
    },
    "1": {
      "enabled": false,
      "style": "POINT",
      "flare": "NONE",
      "position": [
        0.0,
        0.0,
        0.0
      ],
      "rot_yaw": 0.0,
      "rot_pitch": 0.0,
      "color_index": 0,
      "intensity": 1.0,
      "range": 10.0,
      "angle": 45.0
    },
    "2": {
      "enabled": false,
      "style": "POINT",
      "flare": "NONE",
      "position": [
        0.0,
        0.0,
        0.0
      ],
      "rot_yaw": 0.0,
      "rot_pitch": 0.0,
      "color_index": 0,
      "intensity": 1.0,
      "range": 10.0,
      "angle": 45.0
    },
    "3": {
      "enabled": false,
      "style": "POINT",
      "flare": "NONE",
      "position": [
        0.0,
        0.0,
        0.0
      ],
      "rot_yaw": 0.0,
      "rot_pitch": 0.0,
      "color_index": 0,
      "intensity": 1.0,
      "range": 10.0,
      "angle": 45.0
    }
  },
  "colors": [
    {
      "r": 255,
      "g": 255,
      "b": 255
    },
    {
      "r": 255,
      "g": 255,
      "b": 255
    },
    {
      "r": 255,
      "g": 255,
      "b": 255
    },
    {
      "r": 255,
      "g": 255,
      "b": 255
    }
  ],
  "smooth_diff": 0,
  "smooth_same": 0
}"""

