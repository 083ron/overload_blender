# ***** BEGIN GPL LICENSE BLOCK *****
#
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software Foundation,
# Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
#
# ***** END GPL LICENCE BLOCK *****

import bpy
import bmesh
import overload.overload_parser
import overload.materials
import os.path
import json
import sys
from overload.obj2dmesh import load_obj, write_dmesh, AUTO

def loadOverloadFile(filepath):
    """
    Loads marked, exterior sides from a .overload file into a blender mesh.
    """
    fp = open(filepath, "r")
    ovl_name = os.path.splitext(os.path.basename(filepath))[0]
    ovl_json = json.load(fp)

    segments = ovl_json['segments']

    marked_sides = overload.overload_parser.get_marked_exterior_sides(ovl_json)
    vertex_xyzs = overload.overload_parser.get_vertex_xyzs(ovl_json)
    mesh = new_mesh(ovl_name, filepath)

    mesh.uv_textures.new("ovltexmap")
    bm = bmesh.new()

    # Index numbers of vertices in the bmesh
    vlocal = {}

    segid_layer = bm.faces.layers.int.new('segid')
    sideid_layer = bm.faces.layers.int.new('sideid')

    flocal = {}
    texnames_used_by_mesh = set()
    uv_layer = bm.loops.layers.uv.verify()
    bm.faces.layers.tex.verify()


    material_ids = {}
    for segid, sideid, side_data in marked_sides:
        sverts = side_data['verts']

        face_verts = []
        for sv in sverts:
            if sv not in vlocal:
                vlocal[sv] = bm.verts.new(vertex_xyzs[sv])

            face_verts.append(vlocal[sv])

        bface = bm.faces.new(tuple(face_verts))
        bface[segid_layer] = int(segid)
        bface[sideid_layer] = sideid
        
        texname = side_data['tex_name']
        bface.material_index = texname_to_matindex(mesh, texname, material_ids)
        uvs = side_data['uvs']

        fuvs = []
        for uv in uvs:
            fuvs.append((uv['u'], -uv['v']))

        for i in range(4):
            l = bface.loops[i]
            luv = l[uv_layer]
            luv.uv = fuvs[i]
            
    bm.to_mesh(mesh)
    bm.free()


def remove_old_mesh():
    """
    Delete any meshes/objects previously imported.
    """
    objs = bpy.data.objects
    mshs = bpy.data.meshes
    odel = [k for k in objs.keys() if k.startswith('OVL_OBJ')]
    mdel = [k for k in mshs.keys() if k.startswith('OVL_MESH')]

    for obj in odel:
        objs.remove(objs[obj], True)
    for msh in mdel:
        mshs.remove(mshs[msh], True)

    
def new_mesh(name, filepath):
    """
    Builds a new mesh object, and tags it
    with the originating filepath.
    """
    remove_old_mesh()

    mesh = bpy.data.meshes.new("OVL_MESH_"+name)
    obj = bpy.data.objects.new("OVL_OBJ_"+name, mesh)
    scene = bpy.context.scene
    scene.objects.link(obj)
    scene.objects.active = obj
    obj.select = True
    mesh = bpy.context.object.data
    mesh['filepath'] = filepath
    return mesh


def texname_to_matindex(mesh, texname, material_ids):
    """
    Reports the material slot this material is in on mesh, 
    or appends a new one if not.
    """
    if texname in material_ids:
        return material_ids[texname]
    else:
        mat = overload.materials.get_mat(texname)
        mat.use_shadeless = True
        mesh.materials.append(mat)
        txid = len(mesh.materials)-1
        material_ids[texname] = txid
        return txid

    
def export_uvs():
    for n, m in bpy.data.meshes.items():
        if n.startswith("OVL_MESH_"):
            mesh = m
    if not m:
        return

    bpy.ops.object.mode_set(mode = 'OBJECT')
    bm = bmesh.new()
    bm.from_mesh(mesh)
    bm.faces.ensure_lookup_table()

    segid = bm.faces.layers.int['segid']
    sideid = bm.faces.layers.int['sideid']
    uv_layer = bm.loops.layers.uv.verify()

    replace_uvs = {}
    for face in bm.faces:
        uv = []
        for i in range(4):
            l = face.loops[i]
            uv.append((l[uv_layer].uv[0], -l[uv_layer].uv[1]))

        replace_uvs[(face[segid], face[sideid])] = uv

    origfile = mesh['filepath']
    backup = overload.overload_parser.write_backup(origfile)

    overload.overload_parser.rewrite_uvs(backup, origfile, replace_uvs)
    bm.free()



def export_dmesh(filepath):
    dirname, basename = os.path.split(filepath)
    fn, ext = os.path.splitext(basename)
    print(dirname, fn, ext)

    obj_temp_name = os.path.join(dirname, fn+"_objexp.obj")
    mtl_temp_name = os.path.join(dirname, fn+"_objexp.mtl")

    # -- The following is required to strip all the 'tex_face'
    #    assignments. If you have loaded an image in the UV editor
    #    the obj export uses this instead of the texture name in
    #    the assigned material. Object mode is forced otherwise
    #    the mesh data is not updated by the time the export happens.
    bpy.ops.object.mode_set(mode='OBJECT')

    # Strip all the tex_face data, otherwise the obj export will use it
    # We want it to use the material textures.
    for mesh in bpy.data.meshes:
        if mesh.uv_textures.active is not None:
            for tf in mesh.uv_textures.active.data:
                tf.image = None

    for mesh in bpy.data.meshes:
        for texlay in mesh.uv_textures:
            for tf in texlay.data:
                tf.image = None

    # --

    bpy.context.scene.update()
    # Defaults used here except for:
    #  - use_triangles - triangulate on export, otherwise you need to load the dmesh
    #                    in the dmesh editor to get it to work, and it will damage
    #                    vertex normals.
    #  - path_mode     - Strip down to just the image name. OBJ/MTL files don't play
    #                    nice with paths containing spaces, and the paths for the
    #                    overload pngs often have 'Program Files' in them somewhere.
    #                    Overload itself only needs the image name.
    bpy.ops.export_scene.obj(filepath=obj_temp_name, check_existing=True, axis_forward='-Z', axis_up='Y',
                             filter_glob="*.obj;*.mtl",use_normals=True, use_uvs=True,
                             use_materials=True,
                             use_triangles=True,
                             path_mode='STRIP')

    obj = load_obj(obj_temp_name)
    write_dmesh(obj, style=AUTO, outputfile=filepath)

        
    
