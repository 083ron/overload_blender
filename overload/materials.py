# ***** BEGIN GPL LICENSE BLOCK *****
#
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software Foundation,
# Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
#
# ***** END GPL LICENCE BLOCK *****

import bpy
import os
DEFAULT_DECALTEXPATH = "C:\\Program Files (x86)\\Steam\\SteamApps\\common\\Overload\\OverloadLevelEditor\\DecalTextures\\"
DEFAULT_LEVELTEXPATH = "C:\\Program Files (x86)\\Steam\\SteamApps\\common\\Overload\\OverloadLevelEditor\\LevelTextures\\"

MTL_FILE_HEADER = """# Overload MTL File (texture file names are all that matter)
# Material Count: %d
"""

MTL_BLOCK = """
newmtl %s
map_Kd %s
"""


def master_mat():
    """
    Make a dummy material with a texture. We duplicate
    this one to make all the others.
    """
    mat = bpy.data.materials.get("ovl_master_tex")
    if mat is None:
        mat = bpy.data.materials.new("ovl_master_tex")
        mat.texture_slots.add()
        tex = bpy.data.textures.new('placeholder', type= 'IMAGE')
        mat.texture_slots[0].texture = tex
        mat.use_shadeless = True

    return mat


def get_image(imgname_in):
    """
    Get the png preview for an overload texture.
    """
    if imgname_in == 'concrete_test':
        # Concrete test is the name for the default white/green material.
        # There's no preview png so just load a green wall.
        imgname = "om_wall_19b"
    else:
        imgname = imgname_in

    pngname = imgname+".png"
    
    img = bpy.data.images.get(pngname)
    if img is None:
        user_preferences = bpy.context.user_preferences
        addon_prefs = user_preferences.addons[__package__].preferences

        leveltexpath = addon_prefs.leveltexpath
        if leveltexpath == "":
            addon_prefs.leveltexpath = DEFAULT_LEVELTEXPATH
            leveltexpath = addon_prefs.leveltexpath

        decaltexpath = addon_prefs.decaltexpath
        if decaltexpath == "":
            addon_prefs.decaltexpath = DEFAULT_DECALTEXPATH
            decaltexpath = addon_prefs.decaltexpath
        
        realpath = os.path.expanduser(leveltexpath+pngname)
        try:
            img = bpy.data.images.load(realpath)
        except:
            realpath2 = os.path.expanduser(decaltexpath+pngname)
            try:
                img = bpy.data.images.load(realpath2)
            except:
                raise NameError("Cannot load image %s from %s or %s" %
                                (pngname, realpath, realpath2) )
    return img


def get_mat(imgname):
    """
    Gets a material for the given texture name, by copying
    the master material and changing the image.
    """
    mat = bpy.data.materials.get("MAT_"+imgname)
    if mat is None:
        mat = master_mat().copy()
        tex = mat.texture_slots[0].texture.copy()
        mat.texture_slots[0].texture = tex
        tex.image = get_image(imgname)
        tex.name = "TEX_"+imgname
        mat.name = "MAT_"+imgname
    return mat

def repair_broken_mtl_file(mtlfilename):
    material_textures = {}
    for name, mesh in bpy.data.meshes.items():
        for mat in mesh.materials:
            try:
                material_textures[mat.name] = os.path.split(mat.texture_slots[0].texture.image.filepath)[1]
            except:
                material_textures[mat.name] = "None"
                
    with open(mtlfilename, "w") as mtlfile:
        mtlfile.write(MTL_FILE_HEADER%len(material_textures.keys()))
        for mtlname, texname in material_textures.items():
            mtlfile.write(MTL_BLOCK%(mtlname, texname))
            
    print("MTEX:", mtlfilename, material_textures)
        
