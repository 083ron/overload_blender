import bpy
from bpy.types import AddonPreferences, Operator
from bpy.props import BoolProperty
from bpy.props import EnumProperty
from bpy.props import StringProperty
import overload.overload_parser
import overload.materials
import overload.blender
import importlib
from pathlib import Path

class OverloadPrefs(AddonPreferences):
    # this must match the addon name, use '__package__'
    # when defining this in a submodule of a python package.
    bl_idname = __package__

    leveltexpath = StringProperty(
            name="Level Texture File Path",
            subtype='FILE_PATH',
            )

    decaltexpath = StringProperty(
            name="Decal Texture File Path",
            subtype='FILE_PATH',
            )

    
    def draw(self, context):
        layout = self.layout
        layout.label(text="Preferences For Overload")
        layout.prop(self, "leveltexpath")
        layout.prop(self, "decaltexpath")

        
class OBJECT_OT_addon_prefs_example(Operator):
    """Display overload preferences"""
    bl_idname = "object.addon_prefs_overload"
    bl_label = "Overload Preferences"
    bl_options = {'REGISTER', 'UNDO'}

    def execute(self, context):
        user_preferences = context.user_preferences
        addon_prefs = user_preferences.addons[__package__].preferences

        info = ("Path: %s" % addon_prefs.filepath)

        self.report({'INFO'}, info)
        print(info)

        return {'FINISHED'}

    
class ImportOverload(bpy.types.Operator):
    """Import Overload files"""

    bl_idname = "overload.load_file"
    bl_label = "Load Overload marked sides"
    filepath = bpy.props.StringProperty(subtype="FILE_PATH")

    def execute(self, context):
        overload.blender.loadOverloadFile(self.filepath)
        return {'FINISHED'}

    def invoke(self, context, event):
        context.window_manager.fileselect_add(self)
        return {'RUNNING_MODAL'}

class ExportOverload(bpy.types.Operator):
    """Export To Previous Overload file"""

    bl_idname = "overload.export_file"
    bl_label = "Export Overload"
    filepath = bpy.props.StringProperty(subtype="FILE_PATH")

    def execute(self, context):
        return self.invoke(context, None)
        return {'FINISHED'}

    def invoke(self, context, event):
        overload.blender.export_uvs()
        return {'RUNNING_MODAL'}

class ReloadOverload(bpy.types.Operator):
    """Reload the python modules for overload import/export"""
    bl_idname = "overload.reload"
    bl_label = "ReLoad Overload Module"

    def execute(self, context):
        return self.invoke(context, None)

    def invoke(self, context, event):
        overload.blender = importlib.reload(overload.blender)
        overload.overload_parser = importlib.reload(overload.overload_parser)
        overload.materials = importlib.reload(overload.materials)
        return {'FINISHED'}


class ExportDMesh(bpy.types.Operator):
    """Export To a Dmesh file"""

    bl_idname = "overload.export_dmesh"
    bl_label = "Export Dmesh"
    filename_ext = ".dmesh"
    filepath = bpy.props.StringProperty(subtype="FILE_PATH")
    
    def execute(self, context):
        overload.blender.export_dmesh(self.filepath)
        return {'FINISHED'}

    def invoke(self, context, event):
        if context.blend_data.filepath == "":
            self.filepath = "untitled.dmesh"
        else:
            p = Path(context.blend_data.filepath)
            self.filepath = p.with_suffix(ExportDMesh.filename_ext).name
        context.window_manager.fileselect_add(self)
        return {'RUNNING_MODAL'}
