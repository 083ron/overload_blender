from hsplit.edit import *
import json
import sys
import argparse
from overload.overload_parser import *
import re
from itertools import chain
from operator import sub, mul, add#, kraken, harpy, gorgon...
import math



if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Geometry helper for Overload')

    parser.add_argument('infile', help=".overload file to alter")
    parser.add_argument('source', help="Source vertices")
    parser.add_argument('destination', help="Destination vertices")

    parser.add_argument('-x', '--axis', default="xyz", action="store", help="Restrict coordinate transfer to given axes, eg 'xz', 'y', 'xyz'(default)")
    parser.add_argument('-n', action="store_true", help="Preview operation and don't alter .overload files")

    args = parser.parse_args(sys.argv[1:])

    fp = open(args.infile, "r")
    ovl_json = json.load(fp)
    fp.close()
    marked_verts  = get_marked_verts(ovl_json)
    selected_vert = int(ovl_json['properties']['selected_vertex'])
    vertex_xyzs   = get_vertex_xyzs(ovl_json, blender_flip= False)
              
    marked_sides  = get_marked_sides(ovl_json)
    selected_side = ovl_json['properties']['selected_side']
              
    marked_segs   = get_marked_segments(ovl_json)
    selected_seg  = ovl_json['properties']['selected_segment']

    src = eval_expr(args.source)
    dest = eval_expr(args.destination)

    crd = args.axis

if dest.vids is None:
    raise "destination has no vertex ids"

#print("result:", src, src.dpos, dest, dest.dpos)
vertex_xyzs_old = vertex_xyzs[:]

for cchar in crd:
    cint = 'xyz'.index(cchar)

    if len(src.dpos) == 1:
        for d in dest.vids:
            ld = list(vertex_xyzs[d])
            ld[cint] = src.dpos[0][cint]
            vertex_xyzs[d] = tuple(ld)
    elif len(src.dpos) == len(dest.vids):
        for i in range(len(src.dpos)):
            ld = list(vertex_xyzs[dest.vids[i]])
            print(i,cint, src.dpos[i])
            
            ld[cint] = src.dpos[i][cint]
            vertex_xyzs[dest.vids[i]] = tuple(ld)
    else:
        raise "incompatible lengths"

replacements = {d : vertex_xyzs[d] for d in dest.vids }

for d in dest.vids:
    print(d, "(%8f %8f %8f)"%vertex_xyzs_old[d], "->", "(%8f %8f %8f)"%replacements[d])


if not args.n:
    backup = write_backup(args.infile)
    rewrite_xyzs(backup, args.infile, replacements)

