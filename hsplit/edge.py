from .Constants import *
from OpenGL.GL import *
from .glfun import glPrint
from .entity import Entity
from .face import *
from .vertex import *
from .types import PVert
import numpy as np
from .vmath import lb

class Edge(Entity):

    def __init__(self, cc, isvs):
        
        self.isvs = isvs
        self.cc = cc
        self.faces = []
        cc.edges.append(self)
        self.index = len(cc.edges)-1
        for isv in isvs:
            cc.verts[isv].edges.add(self.index)
        self.marked_on_load = False
        self.clipped_coords = [None, None]

    def draw(self, mode, opts):
        old_col = None

        if LABEL in mode:
            self.draw_label()
            return

        if (EDGE in mode or EDGE_SPLIT in mode) and PICK in mode:
            #g = struct.unpack('f', struct.pack('i0f', self.index+1))[0]
            g = (self.index+1)/ID_MAP
            start_col = (0,g,0,1.0)
            end_col = (1,g,0,1)
        else:
            start_col = None
            end_col = None

        if MARK in mode or SELECT in mode:
            set_select_colour(mode, EDGE)
            
        glBegin(GL_LINES)
        if start_col is not None: glColor4fv(start_col)
        glVertex3fv(self.cc.verts[self.isvs[0]].dpos)
        if end_col is not None: glColor4fv(end_col)
        glVertex3fv(self.cc.verts[self.isvs[1]].dpos)
        glEnd()

        if old_col is not None: glColor4fv(old_col)

    def contained_in_screen_rect(self, verts2d, x,y,x2, y2):
        return self.cc.verts[self.isvs[0]].contained_in_screen_rect(verts2d, x,y,x2, y2)\
           and self.cc.verts[self.isvs[1]].contained_in_screen_rect(verts2d, x,y,x2, y2)

    def overlaps_screen_rect(self, verts2d, x,y, x2, y2):
        v2d1 = self.clipped_coords[0][:2]
        v2d2 = self.clipped_coords[1][:2]

        r = lb(v2d1[0], v2d1[1], v2d2[0], v2d2[1], x,y2, x2, y)
        return r

    def draw_label(self):
        glColor3f(1,0,0)
        ct = self.eval(0.5)
        glPrint(ct,str(self.index))

    def eval(self, t, snap_dist=None):
        pt = []
        v1 = np.array(self.cc.verts[self.isvs[0]].dpos)
        v2 = np.array(self.cc.verts[self.isvs[1]].dpos)

        if snap_dist is not None:
            t, pt = snap(v1, v2, t, snap_dist)
        else:
            pt = v1*(1.0-t) + t*v2            

        return pt

    def split(self, v):
        return (Edge(self.cc, (self.isvs[0],v)),
                Edge(self.cc, (v, self.isvs[1])))

    def split_preview(self, sp, t, sns, lev):
        cc = self.cc

        if self.index not in sp.edges:
            #self, sp, t, sns, lev
            sv = Vertex(self.cc, self.eval(t))
            sp.edges[self.index] = sv
            sp.pverts[sv.index] = PVert(self, t, sns)

            self.start_split(sp,t, sns, lev+1)

        return sp.edges[self.index]

    def start_split(self, sp, t, sns, lev):
        #print("start", self, t, sns, lev)
        cc = self.cc
        for iface in self.faces:
            face = cc.faces[iface]

            for icube in face.cubes:

                cube = cc.cubes[icube]
                cube.split_preview(sp, self, t, sns, lev+1)

    def __repr__(self):
        st = ""
        st+="Edge %d: v=%s, par=%s\n"%(self.index, self.isvs, self.faces)
        return st
