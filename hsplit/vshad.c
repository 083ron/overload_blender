const float LOG2 = 1.442695;
//uniform vec3 shrink;
varying float fogFactor;

void main() {

    vec3 ecPosition = vec3(gl_ModelViewMatrix * gl_Vertex);

    gl_FogFragCoord = length(ecPosition);

    fogFactor = exp2( -gl_Fog.density *
					   gl_Fog.density *
					   gl_FogFragCoord *
					   gl_FogFragCoord *
					   LOG2 );
    clamp(fogFactor, 0.0, 0.7);
    gl_FrontColor = gl_Color;
    gl_BackColor = gl_Color;
    gl_Position = gl_ModelViewProjectionMatrix * gl_Vertex; //((0.99 * gl_Vertex) + (0.01*vec4(shrink,1.0)));
}
