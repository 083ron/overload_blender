from hsplit.Constants import *
from OpenGL.GL import *
from hsplit.util import *
from .glfun import orthoquad, screenCoords

class Vertex:

    def __init__(self, cc, dpos):
        self.dpos = dpos
        cc.verts.append(self)
        self.index = len(cc.verts)-1
        self.edges = set()
        self.marked_on_load = False
        
    def draw(self, mode, opts):
        if LABEL in mode:
            self.draw_label()
        elif VERTEX in mode and PICK in mode:
            g = (self.index+1)/ID_MAP
            glColor3f(0.0,g,0.0)
            glBegin(GL_POINTS)
            glVertex3f(*self.dpos)
            glEnd()
        elif SELECT in mode or MARK in mode:
            set_select_colour(mode, VERTEX)
            glBegin(GL_POINTS)
            glVertex3f(*self.dpos)
            glEnd()

    def contained_in_screen_rect(self, verts2d, x,y,x2, y2):
        v2d = verts2d[self.index]
        xx,yy,zz,ww = verts2d[self.index]
        xx/=ww
        yy/=ww
        return xx > x and xx < x2 and \
               yy > y2 and yy < y and ww > 0.1

    def overlaps_screen_rect(self, v2d, x,y,x2, y2):
        return self.contained_in_screen_rect(v2d, x,y, x2, y2)

    def draw_label(self):
        if self.dpos is None:
            return
        glColor3f(0,1,1)
        glPrint(self.dpos,str(self.index))

    def __repr__(self):
        st = ""
        st+="Vertex %d: e=%s, dpos=%s\n"%(self.index, self.edges, self.dpos)
        return st
