from OpenGL.GL import *
from OpenGL.GLU import gluPerspective
#from OpenGL.GLUT import *
import hsplit.shader_init
import hsplit.shaders as shaders
#import pygame.locals

def include(fn):
    fp = open(fn, "r")
    st = fp.readlines()
    fp.close()
    return st

def build_viewport(sx,sy):
    glViewport(0, 0, sx, sy)
    glMatrixMode(GL_PROJECTION)
    glLoadIdentity()
    gluPerspective(45.0, float(sx)/float(sy), 0.1, 100.0)
    #gluPerspective(45.0, float(sx)/float(sy), 1.0, 100.0)

def defaults(screenx, screeny):
    glClearColor(0.0, 0.0, 0.0, 1.0)
    glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT)
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    glEnable(GL_DEPTH_TEST)
    build_viewport(screenx, screeny)
    #glPolygonMode( GL_FRONT_AND_BACK, GL_LINE )
    glLineWidth(3.0)

def fog():
    glFogi(GL_FOG_COORD_SRC, GL_FOG_COORDINATE)
    glFogi (GL_FOG_MODE, GL_EXP2)
    glFogfv (GL_FOG_COLOR, [0.0,0.0,0.0])
    glFogf (GL_FOG_DENSITY, 0.11)
    glHint (GL_FOG_HINT, GL_DONT_CARE)

def print_platform():
    for st in ["VENDOR", "RENDERER", "VERSION", "SHADING_LANGUAGE_VERSION"]:
        st2 = eval('st + ": " + str(glGetString(GL_%s))'%st)
        print(st2)
    print("Max line width1: "+str(glGetIntegerv(GL_ALIASED_LINE_WIDTH_RANGE)))
    print("Max line width2: "+str(glGetIntegerv(GL_SMOOTH_LINE_WIDTH_RANGE)))
    print("Max line width3: "+str(glGetIntegerv(GL_SMOOTH_LINE_WIDTH_GRANULARITY)))

def build_buffers(sx, sy):

    sbo = glGenRenderbuffers(1)
    fbo = glGenFramebuffers(1)
    fbo_depth = glGenRenderbuffers(1)

    glBindRenderbuffer(GL_RENDERBUFFER, sbo)
    glRenderbufferStorage(GL_RENDERBUFFER, GL_RGB32F, sx, sy)
    glBindRenderbuffer(GL_RENDERBUFFER, 0)
    glBindRenderbuffer(GL_RENDERBUFFER, fbo_depth)
    glRenderbufferStorage(GL_RENDERBUFFER,
                          GL_DEPTH_COMPONENT,
                          sx, sy)

    glBindFramebuffer(GL_FRAMEBUFFER, fbo)
    glFramebufferRenderbuffer(GL_FRAMEBUFFER,
                              GL_COLOR_ATTACHMENT0,
                              GL_RENDERBUFFER,
                              sbo)
    glFramebufferRenderbuffer(GL_FRAMEBUFFER,
                              GL_DEPTH_ATTACHMENT,
                              GL_RENDERBUFFER,
                              fbo_depth)
    glBindFramebuffer(GL_FRAMEBUFFER, 0)
    return sbo, fbo, fbo_depth
    
#def setup_glut(splitter):
#    glutInit()
#    glutInitDisplayMode(GLUT_RGBA | GLUT_DOUBLE | GLUT_ALPHA | GLUT_DEPTH)
#    glutInitWindowSize(splitter.screenx, splitter.screeny)
#    glutInitWindowPosition(0, 0)
#    window = glutCreateWindow("Splitter")
#    glutDisplayFunc(splitter.glutDrawFun)
#    glutReshapeFunc(splitter.resizeGL)
#    glutMouseFunc(splitter.glutMouse)
#    glutKeyboardFunc(splitter.glutKbd)
#    glutKeyboardUpFunc(splitter.glutKbdUp)
#    glutPassiveMotionFunc(splitter.mouse_move)
#    glutMotionFunc(splitter.mouse_move)
#    setup_common(splitter)
    
#def setup_pygame(splitter):
#    import pygame
#    glutInit()
#    pygame.init()
#    pygame.display.gl_set_attribute(pygame.locals.GL_MULTISAMPLEBUFFERS,1)
#    pygame.display.gl_set_attribute(pygame.locals.GL_MULTISAMPLESAMPLES,16)
#    scrn = pygame.display.set_mode((splitter.screenx, splitter.screeny),
#                                   pygame.RESIZABLE|
#                                   pygame.HWSURFACE|
#                                   pygame.OPENGL|
#                                   pygame.DOUBLEBUF|
#                                   pygame.NOFRAME)
#    setup_common(splitter)
    
    
def setup_common(splitter):
    print_platform()
    hsplit.shader_init.init_shaders()
    defaults(splitter.screenx, splitter.screeny)
    fog()
    glUseProgram(shaders.textureprog)
    glActiveTexture(GL_TEXTURE0)
    texture_location = glGetUniformLocation(shaders.textureprog, "colorMap");
    glUniform1i(texture_location, 0);
    glUseProgram(0)


