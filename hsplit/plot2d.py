from OpenGL.GL import *
import shaders
import numpy
from itertools import chain

class Plot2D:

    def __init__(self):
        self.show = False
        self.glactive = False
        self.data = []
        
    def glsetup(self):
        vert_loc = glGetAttribLocation(shaders.marqueeprog, "vert")

        self.vao = glGenVertexArrays(1)
        glBindVertexArray(self.vao)

        self.vertexBuffer = glGenBuffers(1)
        glBindBuffer(GL_ARRAY_BUFFER, self.vertexBuffer)
        glEnableVertexAttribArray(vert_loc)
        glVertexAttribPointer(vert_loc, 2, GL_FLOAT, GL_FALSE, 0, None)

#        iBuffer = glGenBuffers(1)
#        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, iBuffer)
#        indexData = numpy.array([0,1,2,2,3,0], numpy.int32)
#        glBufferData(GL_ELEMENT_ARRAY_BUFFER, 4 * 6, indexData, GL_STATIC_DRAW)

    def update(self, xys):
        if not self.glactive:
            self.glsetup()
            self.glactive = True

        glBindBuffer(GL_ARRAY_BUFFER, self.vertexBuffer)

        self.data = list(chain(*xys))
        vertexData = numpy.array(self.data, numpy.float32)

        glBufferData(GL_ARRAY_BUFFER, 4*len(self.data), vertexData, GL_STATIC_DRAW)


    def draw(self):
        if self.show and self.glactive:
            with shaders.draw_info_marquee:
                glUseProgram(shaders.marqueeprog)
                glBindVertexArray(self.vao)
                glDisable(GL_BLEND)
                glDrawArrays(GL_LINES, 0, len(self.data)/2)
                glBindVertexArray(0)
                glUseProgram(0)
