from OpenGL.GL import *
import shaders
import numpy

class Marquee:

    def __init__(self):
        self.show = False
        self.glactive = False
        
    def glsetup(self):
        vert_loc = glGetAttribLocation(shaders.marqueeprog, "vert")

        self.vao = glGenVertexArrays(1)
        glBindVertexArray(self.vao)

        self.vertexBuffer = glGenBuffers(1)
        glBindBuffer(GL_ARRAY_BUFFER, self.vertexBuffer)
        glEnableVertexAttribArray(vert_loc)
        glVertexAttribPointer(vert_loc, 2, GL_FLOAT, GL_FALSE, 0, None)

        iBuffer = glGenBuffers(1)
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, iBuffer)
        indexData = numpy.array([0,1,2,2,3,0], numpy.int32)
        glBufferData(GL_ELEMENT_ARRAY_BUFFER, 4 * 6, indexData, GL_STATIC_DRAW)

    def update(self, x,y,x2,y2):
        if not self.glactive:
            self.glsetup()
            self.glactive = True

        glBindBuffer(GL_ARRAY_BUFFER, self.vertexBuffer)
        vertexData = numpy.array([x,y,x,y2,x2,y2,x2,y], numpy.float32)
        x= -.8
        y = -.8
        x2 = .8
        y2 = .8
        glBufferData(GL_ARRAY_BUFFER, 4*8, vertexData, GL_STATIC_DRAW)


    def draw(self):
        if self.show and self.glactive:
            with shaders.draw_info_marquee:
                glUseProgram(shaders.marqueeprog)

                glBindVertexArray(self.vao)
                glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, None)
                glDisable(GL_BLEND)
                glDrawArrays(GL_LINE_LOOP, 0, 4)
                
            glBindVertexArray(0)
            glUseProgram(0)
