from .util import *
from OpenGL.GL import *
import struct
import traceback
from .face import *

CORNERMAP = {
    (1,2,5):0,
    (2,3,5):1,
    (0,3,5):2,
    (0,1,5):3,
    (1,2,4):4,
    (2,3,4):5,
    (0,3,4):6,
    (0,1,4):7
    }

class Cube:

    def __init__(self, cc, faces=None):
        self.faces = list(faces)
        self.cc = cc
        self.orig_cube = None
        cc.cubes.append(self)
        self.index = len(cc.cubes)-1
        for f in self.faces:
            cc.faces[f].cubes.append(self.index)
        self.guess_structure_directions()
        self.marked_on_load = False

    def draw(self, mode, opts):
        #for f in self.faces:
        #    self.cc.faces[f].draw(opts)
        if LABEL in mode:
            self.draw_label()
        elif MARK in mode or SELECT in mode:

            set_select_colour(mode, SEGMENT)

            v = self.order_verts_for_overload()
            glBegin(GL_LINES)
            for i in range(4):
                glVertex3f(*self.cc.verts[v[i]].dpos)
                glVertex3f(*self.cc.verts[v[(i+1)%4]].dpos)
                glVertex3f(*self.cc.verts[v[i+4]].dpos)
                glVertex3f(*self.cc.verts[v[(i+1)%4+4]].dpos)
                glVertex3f(*self.cc.verts[v[i]].dpos)
                glVertex3f(*self.cc.verts[v[i+4]].dpos)
            glEnd()
            
    def contained_in_screen_rect(self, verts2d, x,y,x2, y2):
        for vid in self.verts():
            if not self.cc.verts[vid].contained_in_screen_rect(verts2d, x,y,x2, y2): return False
        return True
            
    def overlaps_screen_rect(self, verts2d, x,y, x2, y2):
        for fid in self.faces:
            if self.cc.faces[fid].overlaps_screen_rect(verts2d, x,y,x2, y2): return True
        return False
    
    def draw_label(self):
        glColor3f(1,1,0)
        ct = self.centroid()
        glPrint(ct,str(self.index))


    def order_verts_for_overload(self):
        verts = [None]*8
        for iface in [4,5]:
            f = self.cc.faces[self.faces[iface]]
            for v in f.verts:
                s = []
                for i,f2 in enumerate(self.faces):
                    if v in self.cc.faces[f2].verts:
                        s.append(i)

                ind = CORNERMAP[tuple(sorted(s))]
                verts[ind] = v
        return verts


    def centroid(self):
        """
        Get the body centroid as an xyz triplet
        """
        ct = [0.0,0.0,0.0]
        for face in self.faces:
            ct2 = self.cc.faces[face].centroid()
            for i in range(3):
                ct[i] += ct2[i]
        ct = [a/float(len(self.faces)) for a in ct]
        return ct


    def verts(self):
        return self.cc.faces[self.faces[4]].verts[::-1]+self.cc.faces[self.faces[5]].verts
    
    def guess_structure_directions(self):
        """
        Attempts to establish which faces are opposite others
        and assign structure directions to a cube.
        """
        self.struct = {}
        if len(self.faces) != 6:
            self.type = UNSTRUCT
        else:
            self.dirs = [0]*6
            d = 1
            for i in range(6):
                if self.dirs[i] != 0:
                    continue
                f = self.cc.faces[i]
                for j in range(6):
                    if j==i: continue
                    f2 = self.cc.faces[j]
                    if len(f.common_edge(f2)) == 0:
                        self.dirs[i] = -d
                        self.dirs[j] = d
                        self.struct[d] = (i,j)
                        d += 1


    def neighbouring_face(self, iface, iedge):
        """
        Returns the face on the other side of
        iedge, within this cube only.
        iface - global face ID
        iedge - global edge ID
        """
        face = self.cc.faces[iface]
        edge = self.cc.edges[iedge]
        for f in edge.faces:
            if f == iface: continue
            fac2 = self.cc.faces[f]
            if self.index in fac2.cubes:
                return fac2
        return None


    def opposite(self, face):
        """
        Looks up the opposite face to intface, and
        work out if it is oriented similarly to intface.
        intface - internal face ID
                  (ie in range 0 -> len(self.faces)-1)
        """
        intface = self.faces.index(face.index)
        d = self.dirs[intface]
        struct = self.struct[abs(d)]
        face = self.cc.faces[self.faces[intface]]
        if struct[0] == intface:
            other = self.cc.faces[self.faces[struct[1]]]
        else:
            other = self.cc.faces[self.faces[struct[0] ]]
        link = self.neighbouring_face(face.index, face.edges[0])
        ind = link.edges.index(face.edges[0])
        ori1 = face.oris[0] * link.oris[ind]

        ce = link.common_edge(other)[0]

        ind = link.edges.index(ce)
        ind2 = other.edges.index(ce)
        ori2 = link.oris[ind] * other.oris[ind2]

        return (other, ori1*ori2)

    def replace_face(self, i, inewface):
        ioldface = self.faces[i]
        self.faces[i] = inewface
        self.cc.faces[ioldface].cubes.remove(self.index)
        self.cc.faces[inewface].cubes.append(self.index)


    def split_preview(self, sp, edge, t, sns, lev):
        """
        Create a temp split face across this cube, starting
        from edge at position t.
        """        
        if self.index not in sp.cubes:

            faces = []
            #print lev*"  "+"SP cube %d"%self.index

            es = set()
            for ifn, f in enumerate(edge.faces):
                if f not in self.faces: continue
                i = self.faces.index(f)
                se = self.cc.faces[f].split_preview(sp, edge, t, sns, lev+1)
                es.add(se)

                pv = sp.pverts[se.isvs[0]]
                if pv[0] == edge:
                    pv = sp.pverts[se.isvs[1]]

                other = self.neighbouring_face(f, pv[0].index)
                es.add(other.split_preview(sp, pv[0], pv[1], pv[2], lev+1))
                
            #Check again in case a recursive call has already finished it
            if self.index not in sp.cubes:
                sf = Face(self.cc, [e.index for e in es])
                sp.cubes[self.index] = sf

        return sp.cubes[self.index]


    def __repr__(self):
        st = ""
        st+="Cube %d: f=%s,"%(self.index, self.faces)
        if hasattr(self, "dirs") and hasattr(self, "struct"):
            st += "dirs=%s, struct=%s\n"%(self.dirs, self.struct)
        else:
            st+="\n"
        return st
