uniform sampler2D colorMap;
varying float fogFactor;
void main ()
{   
  gl_FragColor = mix(gl_Fog.color, texture2D( colorMap, gl_TexCoord[0].st), fogFactor);
}
