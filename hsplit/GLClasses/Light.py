from OpenGL.GL import *
nlights = 0

class Light:
    
    def __init__(self,amb=(0.0,0.0,0.0,1.0),
                      dif=(1.0,1.0,1.0,1.0),
                      pos=(0.0,0.0,0.0,1.0)):
        global nlights
        self.amb = amb
        self.dif = dif
        self.pos = pos
        exec("self.glenum = GL_LIGHT%s"%nlights)
        nlights+=1
        
    def setup(self):
        glLightfv(self.glenum, GL_AMBIENT, self.amb)
        glLightfv(self.glenum, GL_DIFFUSE, self.dif)
        
    def position(self):
        glLightfv(self.glenum, GL_POSITION, self.pos)
        
    def enable(self):
        glEnable(self.glenum)
        
    def disable(self):
        glDisable(self.glenum)