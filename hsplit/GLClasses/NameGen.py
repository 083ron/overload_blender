class NameGen:
    
    def __init__(self):
        self.counter = 0
        
    def generatename(self):
        self.counter += 1
        return self.counter
    
    def reset(self):
        self.counter = 0