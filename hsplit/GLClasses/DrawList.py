from .Regenerable import Regenerable
from hsplit.vertex import Vertex
from hsplit.edge import Edge
from hsplit.face import Face
from hsplit.cube import Cube
from OpenGL.GL import *
from hsplit.Constants import *

class DrawList(Regenerable):
    
    def __init__(self, *args, **kwargs):
        Regenerable.__init__(self, *args, **kwargs)
        self.colors = []
        self.items = []

    def append(self, item):
        self.items.append(item)
        #self.mark_stale()

    def remove(self, item):
        self.items.remove(item)

    def __len__(self):
        return len(self.items)

    def __getitem__(self, i):
        return self.items[i]

    def __setitem__(self, i, item):
        self.items[i] = item
        #self.mark_stale()

    def empty(self):
        #self.mark_stale()
        self.items = []
        
    def draw_geometry(self, mode, opts, lst = None):
        if not lst:
            lst = self.items

        i = 0
        for item in lst:
            if item is None:
                continue
            if self.colors != []:
                i += 1
                print self.colors[i%len(self.colors)]
                glColor4fv(self.colors[i%len(self.colors)])
            item.draw(mode, opts)

    def __delitem__(self, item):
        del self.items[item]
        #self.mark_stale()

    def __repr__(self):
        return "DL"

class DrawCollection(object):

    def __init__(self):
        self.verts = DrawList()
        self.edges = DrawList()
        self.faces = DrawList()
        self.cubes = DrawList()
        self.items = { VERTEX :  self.verts,
                       EDGE :  self.edges,
                       SIDE :  self.faces,
                       SEGMENT :  self.cubes }

    def mark_stale(self, mode=None):
        self.verts.mark_stale()
        self.edges.mark_stale()
        self.faces.mark_stale()
        self.cubes.mark_stale()

    def empty(self):
        self.verts.empty()
        self.edges.empty()
        self.faces.empty()
        self.cubes.empty()
        
    def draw(self, mode, **kwargs):
        self.faces.draw(mode, **kwargs)
        self.cubes.draw(mode, **kwargs)
        self.edges.draw(mode, **kwargs)
        self.verts.draw(mode, **kwargs)

class Selection(DrawCollection):

    def __init__(self):
        DrawCollection.__init__(self)

    def indices(self):
        i = [0]*4
        i[0] = self.verts[0].index
        i[1] = self.edges[0].index
        i[2] = self.faces[0].index
        i[3] = self.cubes[0].index
        return tuple(i)

    def reindex(self, cc):
        v,e,f,b = self.indices()
        self.vert = cc.verts[v]
        self.edge = cc.edges[e]
        self.side = cc.faces[f]
        self.segment = cc.cubes[b]
                                
    def __getattribute__(self, name):
        if name == 'vert':
            return DrawCollection.__getattribute__(self, 'verts')[0]
        elif name == 'edge':
            return DrawCollection.__getattribute__(self, 'edges')[0]
        elif name == 'side':
            return DrawCollection.__getattribute__(self, 'faces')[0]
        elif name == 'segment':
            return DrawCollection.__getattribute__(self, 'cubes')[0]
        else:
            return DrawCollection.__getattribute__(self, name)

    def __setattr__(self, name, value):
        if name == 'vert':
            self.verts[0] = value
        elif name == 'edge':
            self.edges[0] = value
        elif name == 'side':
            self.faces[0] = value
        elif name == 'segment':
            self.cubes[0] = value
        else:
            object.__setattr__(self, name, value)
            
    #def draw_geometry(self, mode, opts):
    #    DrawList.draw_geometry(self, mode, opts,
    #                           lst=self.items+[self.vert,
    #                                           self.edge,
    #                                           self.side,
    #                                           self.segment]
    #                           )
