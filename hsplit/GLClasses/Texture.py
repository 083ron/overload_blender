from Image import open

from OpenGL.GL import *

class Texture:

    def __init__(self,imageName):

        im = open(imageName)
        try:
            ix, iy, self.image = im.size[0], \
                            im.size[1], \
                            im.tostring("raw", "RGBA", 0, -1)
        except SystemError:
            ix, iy, self.image = im.size[0], \
                            im.size[1], \
                            im.tostring("raw", "RGBX", 0, -1)

        self.ID = glGenTextures(1)
        glPixelStorei(GL_UNPACK_ALIGNMENT,1)
        glTexImage2D(GL_TEXTURE_2D, 0, 4, \
                     ix, iy, 0, GL_RGBA, GL_UNSIGNED_BYTE, self.image)
        glBindTexture(GL_TEXTURE_2D,self.ID)

    def setAsTexture(self):
        glBindTexture(GL_TEXTURE_2D,self.ID)
