from OpenGL.GL import *
from OpenGL.GLU import gluLookAt
from math import sin,cos,pi
from operator import sub, mul, add#, kraken, harpy, gorgon...
from hsplit.vmath import cross, smul, sadd, D2R

class Camera:

    def __init__(self):
        self.position = [-2.1,0.8,-0.74]
        self.theta = 317.8
        self.phi = -83.4
        self.angles_dirty = True
        self.update_angles()

    def doTransform(self):
        glRotatef(self.phi,1.0,0.0,0.0)
        glRotatef(self.theta,0.0,0.0,1.0)
        glTranslatef(*self.position)

    def update_angles(self):
        if self.angles_dirty:
            self.ctheta = cos(D2R * -self.theta)
            self.stheta = sin(D2R * -self.theta)
            self.cphi = cos(D2R * self.phi)
            self.sphi = sin(D2R * self.phi)
            self.angles_dirty = False

    def forwards(self,rx,ry):
        self.position[0] += .008*ry*sin(D2R *self.theta)
        self.position[1] += .008*ry*cos(D2R *self.theta)
        
    def strafe_h(self,rx,ry):
        self.position[0]+=(0.008*rx*-cos(D2R * self.theta))
        self.position[1]+=(0.008*rx*sin(D2R * self.theta))

    def strafe_v(self,rx,ry):
        self.position[2]+=0.008*ry

    def yaw(self,rx,ry):
        self.theta += 0.25 * rx
        self.angles_dirty = True

    def pitch(self,rx,ry):
        self.phi += 0.25 * ry
        if self.phi > 89.5:
            self.phi = 89.5
        if self.phi < -89.5:
            self.phi = -89.5
        self.angles_dirty = True

    def __repr__(self):
        return "pos=%s theta=%s phi=%s"%(str(self.position),self.theta, self.phi)

class OrbitCamera(Camera):

    def __init__(self):
        self.position = [0,0,0]
        self.theta = 0.0
        self.phi = 0.0
        self.dist = 3.0
        self.angles_dirty = True
        self.update_angles()

    def doTransform(self):
        eye = sadd(self.position, [self.dist * self.stheta * self.cphi,
                                   self.dist * -self.ctheta * self.cphi,
                                   self.dist * self.sphi])
        
        gluLookAt(eye[0], eye[1], eye[2],
                  self.position[0], self.position[1], self.position[2],
                  0.0,0.0,1.0)

    def update_angles(self):
        if self.angles_dirty:
            Camera.update_angles(self)
            pos = [-self.stheta * self.cphi,
                   self.ctheta * self.cphi,
                   -self.sphi]
            self.left = [-self.ctheta, -self.stheta, 0.0]
            self.up = cross(pos, self.left)
        
    def strafe_h(self, rx,ry):
        Camera.strafe_h(self, rx*0.25*self.dist, ry*0.25*self.dist)
        
    def strafe_v(self, rx,ry):
        self.position = sadd(self.position, smul(0.008*ry*0.25*self.dist, self.up))
       
    def zoom_in(self):
        if self.dist < 1.0:
            self.dist -= 0.05
        else:
            self.dist *= 0.90
        if self.dist < 0.05:
            self.dist = 0.05

    def zoom_out(self):
        if self.dist < 1.0:
            self.dist += 0.05
        else:            
            self.dist *= 1.1
