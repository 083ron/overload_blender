from OpenGL.GL import *

class DummyDrawInfo:

    def __enter__(self):
        pass

    def __exit__(self, exception_type, exception_value, traceback):
        pass

class DrawInfo:

    def __init__(self, shader=None,
                 linewidth = 1.0,
                 depth_mask=True,
                 enable=[],
                 disable=[],
                 opts = {},
                 uniforms = {}):
        self.opts = opts.copy()
        self.shader = shader
        self.enable = enable
        self.disable = disable
        self.linewidth = linewidth
        self.depth_mask = depth_mask
        self.uniforms = {}
        
    def __enter__(self):
        if self.shader is not None:
            glUseProgram(self.shader)
        for flag in self.enable:
            glEnable(flag)
        for flag in self.disable:
            glDisable(flag)
        if self.linewidth is not None:
            glLineWidth(self.linewidth)
        if not self.depth_mask:
            glDepthMask(GL_FALSE)

        for k,v in self.uniforms.iteritems():
            uloc = glGetUniformLocation(self.shader, k)
            # Assume scalar float for now, but could expand
            glUniform1f(uloc, v)
    
    def __exit__(self, exception_type, exception_value, traceback):
        for flag in self.disable:
            glEnable(flag)
        for flag in self.enable:
            glDisable(flag)
        if not self.depth_mask:
            glDepthMask(GL_TRUE)
