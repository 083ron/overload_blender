from OpenGL.GL import *
import traceback
import hsplit.shaders

class Regenerable:
    """
    Drawable object that manages its own displaylist
    """

    def __init__(self, name="regenerable"):
        self.displaylists = {}
        self.modes = {}
        self.trace = False
        self.name = name

    def __del__(self):
        try:
            for k, v in self.displaylists.items():
                glDeleteLists(v,1)
        except:
            traceback.print_exc()

    def find_closest_mode(self, mode):
        if mode in self.modes:
            return mode
        else:
            for md in self.modes:
                if len(md) < len(mode):
                    if [a for a in md if a not in mode] == []:
                        return md
        return None

    def register_draw_info(self, mode, info):
        self.modes[mode] = info

    def regenList(self,mode,opts,execute=False):

        md = self.find_closest_mode(mode)
       
        if md in self.displaylists:
            glDeleteLists(self.displaylists[md],1)

        self.displaylists[md] = glGenLists(1)
        if(execute):
            glNewList(self.displaylists[md],GL_COMPILE_AND_EXECUTE)
        else:
            glNewList(self.displaylists[md],GL_COMPILE)
        self.draw_geometry(mode, opts)
        glEndList()

    def mark_stale(self, mode=None):
        if mode is None:
            # Delete all lists
            for mode in self.displaylists:
                glDeleteLists(self.displaylists[mode],1)
            self.displaylists = {}
        elif mode in self.displaylists:
            glDeleteLists(self.displaylists[mode],1)
            del self.displaylists[mode]

    def draw(self, mode, **kwargs):
        if mode in self.modes:
            info = self.modes[mode]
        else:
            md = self.find_closest_mode(mode)
            if md is not None:
                info = self.modes[md]
            else:
                info = hsplit.shaders.draw_info_dummy
        if self.trace:
            print "----------------------"
            print self.name, "mode: ", mode, info.shader
            traceback.print_exc()
            print "----------------------"
       
        with info:
            if mode not in self.displaylists:
                #print self.name, mode, "regen list"
                dct = {}
                dct.update(info.opts)
                dct.update(kwargs)
                self.regenList(mode, dct, execute=True)
            else:
                #print self.name, mode, "call list"
                glCallList(self.displaylists[mode])
