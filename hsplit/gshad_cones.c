#version 150 compatibility
uniform vec2 vp = vec2(1500.0,900.0);
layout(points) in;
layout(triangle_strip, max_vertices = 8) out;
in vec4 vColor[];
out vec4 fColor;
	       
void main()
{
    fColor = vColor[0];
    vec2 diag = 150.0/vp;

    float n = 0.9;
    float f = 0.1;
    
    gl_Position =  gl_in[0].gl_Position;
    gl_Position /= gl_Position.w;

    gl_Position.xy += vec2(-diag[0], -diag[1]);
    gl_Position.z = n;
    EmitVertex();

    gl_Position.xy += vec2(diag[0]*2.0, 0.0);
    gl_Position.z = n;
    EmitVertex();

    gl_Position.xy += vec2(-diag[0], +diag[1]);
    gl_Position.z = f;
    EmitVertex();

    gl_Position.xy += vec2(diag[0], diag[1]);
    gl_Position.z = n;
    EmitVertex();

    EndPrimitive();

    EmitVertex();
    gl_Position.xy += vec2(-diag[0]*2.0, 0.0);
    gl_Position.z = n;

    EmitVertex();

    gl_Position.xy += vec2(diag[0], -diag[1]);
    gl_Position.z = f;
    EmitVertex();

    gl_Position.xy += vec2(-diag[0], -diag[1]);
    gl_Position.z = n;
    EmitVertex();
    

}
