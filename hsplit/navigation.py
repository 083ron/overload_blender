def cycle_side(cc, sel, inc):
    seg = sel.segment
    side = sel.side
    i = seg.faces.index(side.index)
    i = (i+inc)%6
    sel.side = cc.faces[seg.faces[i]]
   
def cycle_seg(cc, sel, inc):
    seg = sel.segment
    side = sel.side
    i = seg.faces.index(side.index)
    j = seg.index
    j = (j+inc)%len(cc.cubes)
    sel.segment = cc.cubes[j]
    sel.side = cc.faces[cc.cubes[j].faces[i]]

def other_side(cc, sel, e=None):
    seg = sel.segment
    side = sel.side
    opp, ori = seg.opposite(side)
    sel.side = opp

def prev_seg(cc, sel, e=None):
    seg = sel.segment
    side = sel.side
    other_side(cc, sel)
    if len(sel.side.cubes) < 2:
        other_side(cc, sel)
        return
    next_seg(cc, sel, flip=False)

def next_seg(cc, sel, e=None, flip=True):
    seg = sel.segment
    side = sel.side
    segs = side.cubes
    if len(segs) < 2:
        return
    if segs[0] == seg.index:
        seg = cc.cubes[segs[1]]
    else:
        seg = cc.cubes[segs[0]]

    sel.segment = seg
    if flip:
        other_side(cc, sel)
