import OpenGL.raw.WGL._types as t
from ctypes import _SimpleCData, _check_size

delattr(t.HANDLE, "final")

class HANDLE(_SimpleCData):
    _type_ = "P"
    
_check_size(HANDLE)

HANDLE.final = True

t.HANDLE = HANDLE
t.HGLRC = HANDLE
t.HDC = HANDLE

t.HPBUFFERARB = HANDLE
t.HPBUFFEREXT = HANDLE
