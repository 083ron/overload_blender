#version 150 compatibility
uniform vec2 vp = vec2(1500.0,900.0);
uniform float psize = 6.0;
layout(points) in;
layout(triangle_strip, max_vertices = 4) out;
in vec4 vColor[];
out vec4 fColor;
	       
void main()
{
    fColor = vColor[0];
    vec2 diag = psize/vp;

    gl_Position =  gl_in[0].gl_Position;
    gl_Position /= gl_Position.w;
    gl_Position.xy += vec2(-diag[0], -diag[1]);
    EmitVertex();

    gl_Position.xy += vec2(diag[0]*2.0, 0.0);
    EmitVertex();
    
    gl_Position.xy += vec2(-diag[0]*2.0, diag[1]*2.0);
    EmitVertex();

    gl_Position.xy += vec2(diag[0]*2.0, 0.0);
    EmitVertex();
    
    EndPrimitive();
}
