varying float fogFactor;
const float LOG2 = 1.442695;
void main()
{

  vec3 ecPosition = vec3(gl_ModelViewMatrix * gl_Vertex);
  gl_FogFragCoord = length(ecPosition);

  fogFactor = exp2( -gl_Fog.density *
         	    gl_Fog.density *
		    gl_FogFragCoord *
   		    gl_FogFragCoord *
   		    LOG2 );
  clamp(fogFactor, 0.0, 0.9);

  gl_TexCoord[0] = gl_MultiTexCoord0;
  gl_Position = ftransform();
	
}
