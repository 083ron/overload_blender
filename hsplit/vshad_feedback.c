#version 330
in vec3 vert;
out vec4 outValue;

uniform mat4 mvMatrix;
uniform mat4 pMatrix;

void main() {
  vec4 v = ( pMatrix * mvMatrix * vec4(vert, 1.0));
  outValue = vec4(v.xyzw);
  
}
