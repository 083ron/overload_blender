#version 330

in vec2 UV;
uniform sampler2D textureimg;
out vec3 colour;
      
void main() {
  colour = texture(textureimg, UV).rgb;
}
