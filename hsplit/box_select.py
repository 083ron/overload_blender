from Constants import *
import numpy as np

def begin_drag(win, shift, ctrl):
    mode = win.controls[CTL_SELECT_MODE]
    iall = win.cc.items[mode]
    items = win.marked.items[mode]
    win.marked_flags = np.array([0]*len(iall))

    for item in items:
        win.marked_flags[item.index] = 1

def normalise_rect(winw, winh, rx1, ry1, rx2, ry2):
    w = winw/2.0
    h = winh/2.0

    x = rx1/w - 1.0
    y = -(ry1/h - 1.0)
    x2 = rx2/w - 1.0
    y2 = -(ry2/h - 1.0)

    if y < y2:
        t = y
        y = y2
        y2 = t

    xflip = False
    if x2 < x:
        xflip = True
        t = x
        x = x2
        x2 = t
    return x,y,x2,y2,xflip

def drag(win, ev):
    ctrl = ev.state&4 > 0
    shift = ev.state&1 > 0
    cc = win.cc

    if win.drag_first:
        begin_drag(win, ctrl, shift)
        win.drag_first = False

    mx,my,mx2,my2,xflip = normalise_rect(win.screenx, win.screeny, win.downxy[0], win.downxy[1], ev.x, ev.y)

    mode = win.controls[CTL_SELECT_MODE]
    items = win.marked.items[mode]

    win.marked.items[mode].empty()
    win.marked.items[mode].mark_stale()      

    vp  = []

    if mode == VERTEX:
        hits = np.zeros(len(cc.verts), dtype=int)
        for v in cc.verts:
            if v.contained_in_screen_rect(cc.verts2d, mx,my,mx2,my2):
                hits[v.index] = 1
    else:
        hits = np.zeros(len(cc.edges), dtype=int)
        for e in cc.edges:
            hit = False
            if xflip:
                hit = e.overlaps_screen_rect(cc.verts2d, mx,my,mx2,my2)
            else:
                hit = e.contained_in_screen_rect(cc.verts2d, mx,my,mx2,my2)

            if hit:
                hits[e.index] = 1
        if mode == SIDE or mode == SEGMENT:
            hit_faces = np.zeros(len(cc.faces), dtype=int)
            for side in cc.faces:
                edges = [hits[eid] for eid in side.edges]
                if xflip and any(edges) or not xflip and all(edges):
                    hit_faces[side.index] = 1
            if mode == SIDE:
                hits = hit_faces
            else:
                hit_segs = np.zeros(len(cc.cubes), dtype=int)
                for seg in cc.cubes:
                    faces = [hit_faces[fid] for fid in seg.faces]
                    if xflip and any(faces) or not xflip and all(faces):
                        hit_segs[seg.index] = 1
                hits = hit_segs

    if ctrl:
        new_marks = win.marked_flags - hits
    else:
        new_marks = win.marked_flags | hits
    for i in range(len(new_marks)):
        if new_marks[i] > 0:
            win.marked.items[mode].append(cc.items[mode][i])   

    win.marquee.update(mx,my,mx2,my2)


    #self.tmp_marquee.update(vp)
    win.marquee.show = True
    #self.tick()
