import json
import sys
import argparse
from overload.overload_parser import *
import re
from itertools import chain
from operator import sub, mul, add#, kraken, harpy, gorgon...
import math
from vmath import *

from OpenGL.GL import *
import numpy as np

class EditError(Exception):
    def __init__(self, message):
        self.message = message


def rotation(origin, axis, degrees):
    glMatrixMode(GL_MODELVIEW)
    glPushMatrix()
    glLoadIdentity()
    glTranslated(*origin)
    glRotated(degrees, *axis)
    glTranslated(-origin[0],-origin[1],-origin[2])
    mat = glGetDouble(GL_MODELVIEW_MATRIX)
    return mat


def rotate(vg, *args):

    start = None
    end = None
    deg = None

    if len(args[0].dpos) == 1:
        if len(args) == 3:
            origin, axis, deg = args
        else:
            origin, axis, start, end = args
        axis = VertexGroup(origin.dpos[0], sadd(origin.dpos[0], displacement(axis).ddir))
    elif len(args[0].dpos) == 2:
        if len(args) == 2:
            axis, deg = args
        else:
            axis, start, end = args
        origin = axis
    
    if deg is not None:
        return rotate_absolute(vg, origin, axis, deg)
    else:
        oc = origin.dpos[0]
        ac = displacement(axis).ddir

        
        
        start_vec = normalise(displacement(start, start | axis).ddir)
        end_vec = normalise(displacement(end, end| axis).ddir)

        proj = np.dot(start_vec, end_vec)
        winding = cross(start_vec, end_vec)

        if np.dot(winding, ac) > 0:
            ang = math.acos(proj)
        else:
            ang = -math.acos(proj)
        ang *= (180.0/math.pi)

        return rotate_absolute(vg, origin, axis, ang)

    
def rotate_absolute(vg, origin, axis, degrees):
    oc = origin.dpos[0]
    ac = displacement(axis).ddir
    rmat = rotation(oc, ac, degrees)
    rmat = rmat.transpose()
    cvs = [np.array([list(dpos)+[1.0]]).transpose() for dpos in vg.dpos]
    res = [ np.dot(rmat, cv) for cv in cvs]
    dps = [v.transpose()[:2].tolist()[0] for v in res]
    return VertexGroup(*dps)
    
def displacement(*args,**kwargs):
    if len(args) == 2:
        d = Displacement(ddir = ssub(args[1].dpos[0],
                                        args[0].dpos[0]))
    elif len(args) == 1:
        a = args[0]
        if a.__class__ == Displacement:
            return a
        if len(a.dpos) >= 3:
            d = Displacement(ddir=plane_normal(a.dpos[:3]))
        elif len(a.dpos) == 2:
            d = Displacement(ddir=ssub(a.dpos[1], a.dpos[0]))

    if 'length' in kwargs:
        magn = math.sqrt(dot(d.dirn,d.dirn))
        fac = kwargs['length']/magn
        d.dirn = smul(fac, d.dirn)

    return d
        
def line(*args):
    """
    Builds a line out of 2 vertexgroups, or one vertexgroup with 2 positions.
    """
    if len(args) == 2:
        return VertexGroup(*[a.dpos[0] for a in args])
    if len(args) == 1:
        if len(args[0].dpos) == 2:
            dp=args[0].dpos
            return VertexGroup(*dp)
    raise EditError("Could not build a line from %s"%args)

def bisector_plane(p1, p2):
    line = p1^p2
    p3 = sdiv(2.0,sadd(normalise(plane_normal(p1.dpos)),
              normalise(plane_normal(p2.dpos))))
    #line.dpos.append(sadd(line.dpos[0], p3))
    return VertexGroup(line.dpos[0], line.dpos[1], sadd(line.dpos[0], p3))
                                 

def plane(*args):
    """
    Builds a plane out of 3 vertexgroups, or one vertexgroup with 3 positions.
    """
    if len(args) == 3:
        return VertexGroup(*[a.dpos[0] for a in args])
    if len(args) == 2:
        pnt = args[0].dpos[0]
        nrm = args[1]
        if nrm.__class__ == VertexGroup:
            if len(nrm.dpos) == 2:
                nrm = displacement(nrm)
        nrm = nrm.ddir
        
        ax2 = cross(nrm,(1.0,0.0,0.0))
        if magnitude(ax2) < 1e-6:
            ax2 = cross(nrm,(0.0,1.0,0.0))
        ax2 = normalise(ax2)
        ax3 = normalise(cross(nrm, ax2))
        #print pnt, sadd(pnt, ax2), sadd(pnt, ax3)
        return VertexGroup(pnt, sadd(pnt, ax2), sadd(pnt, ax3))
    if len(args) == 1:
        if len(args[0].dpos) >= 3:
            dp=args[0].dpos
            return VertexGroup(*dp[:3])
    raise EditError("Could not build a plane from %s"%args)


class Displacement:

    def __init__(self, ddir):
        self.ddir = ddir

    def __add__(self, vg):
        return VertexGroup(vg.dpos, sadd(vg.dpos, self.ddir))

    def __mul__(self, scalar):
        return Displacement([d * scalar for d in self.ddir])

    __rmul__ = __mul__

class VertexGroup:
    """
    Class to track original vertex IDs and intermediate results
    A group of size:
        1 - is usually a single vertex
        2 - is usually a line
        3 - is usually a plane
        4 - came from a segment side
    """
    
    def __init__(self, *args, **kwargs):
        """
        Build a vertex group from a list of positions
        and optionally a list of index numebrs.
        """
        self.dpos = args
        if 'vids' in kwargs:
            self.vids = kwargs['vids']
        else:
            self.vids = None

    def __or__(self, other):
        """Overload (pun intended) the | operator (pun intended) to mean projection"""
        dirn = None
        if type(other) == tuple:
            other, dirn = other
            if dirn.__class__ == VertexGroup:
                if len(dirn.dpos) == 2:
                    dirn = displacement(dirn)
            dirn = dirn.ddir
        len_s = len(self.dpos)
        len_o = len(other.dpos)

        if dirn:
            if len_o >= 3:
                result = VertexGroup(*[lineintersectplane([dp, sadd(dp, dirn)], other.dpos)[0] for dp in self.dpos])
            elif len_o == 2:
                print(self, "|", other, "directional projection can only target plane")
            else:
                print(self, "|", other, "results in invalid projection")
        else:
            if len_o >= 3:
                result = VertexGroup(*[pointonplane(dp, other.dpos) for dp in self.dpos])
            elif len_o == 2:
                result = VertexGroup(*[pointonline(dp, other.dpos) for dp in self.dpos])
            else:
                print(self, "|", other, "results in invalid projection")
        return result

    def __xor__(self, other):
        """
        Overload (pun intended) the ^ operator (pun intended) to mean intersection.
        Return a new Vertex group with the result.
        """
        len_s = len(self.dpos)
        len_o = len(other.dpos)
        if len_o >= 3:
            if len_s >= 3:
                result = VertexGroup(*planeintersectplane(self.dpos[:3], other.dpos[:3]))
            elif len_s == 2:
                result = VertexGroup(*lineintersectplane(self.dpos, other.dpos[:3]))
        elif len_o == 2:
            if len_s >= 3:
                result = VertexGroup(*lineintersectplane(other.dpos, self.dpos[:3]))
            elif len_s == 2:
                raise "line line intersection not implemented"
        return result

            
    def __sub__(self, other):
        len_s = len(self.dpos)
        len_o = len(other.dpos)
        if len_o == 1 and len_s == 1:
            return Displacement(ddir = ssub(other.dpos[0], self.dpos[0]))
            
    def __add__(self, v):
        if isinstance(v, Displacement):
            return VertexGroup(*[sadd(d, v.ddir) for d in self.dpos])

    def __div__(self, v):
        len_s = len(self.dpos)
        len_o = len(v.dpos)
        if len_o >= 3 and len_s >= 3:
            return bisector_plane(self,v)

    def __repr__(self):
        st = "VertexGroupx%s %s"%(len(self.dpos),str(self.vids))
        return st
        
def add_segverts(seg, vids):
    for side in seg['sides']:
        for v in side['verts']:
            vids.add(v)
    return vids

def entity(string):
    global marked_verts, selected_vert, vertex_xyzs
    global marked_sides, selected_side
    global marked_segs, selected_seg
    if "v" in string:
        if string == "vs":
            vids = [selected_vert]
        elif string == "vm":
            vids = marked_verts
        else:
            vids = [int(string[1:])]
            
    elif "s" in string:
        if string == "ss":
            side = ovl_json['segments'][str(selected_seg)]['sides'][selected_side]
            vids = side['verts']
        elif string == "sm":
            vidss = set()
            for sg,sd,side in marked_sides:
                for v in side['verts']:
                    vidss.add(v)
            vids = list(vidss)
        else:
            b = string[1:].split('s')
            segindex = b[0]
            sideindex = int(b[1])
            side = ovl_json['segments'][str(segindex)]['sides'][sideindex]
            vids = side['verts']


    elif "g" in string:
        vids = set()
        if string == "gs":
            seg = ovl_json['segments'][str(selected_seg)]
            add_segverts(seg, vids)
        elif string == "gm":
            for seg in marked_segs:
                add_segverts(ovl_json['segments'][str(seg)], vids)
        else:
            segindex = string[1:]
            add_segverts(ovl_json['segments'][str(segindex)], vids)
        vids = list(vids)
        
    return VertexGroup(*[vertex_xyzs[vid] for vid in vids], vids=vids)

def entity2(string, cc, selection, marked):
    #global marked_verts, selected_vert, vertex_xyzs
    #global marked_sides, selected_side
    #global marked_segs, selected_seg
    if "v" in string:
        if string == "vs":
            vids = [selection.vert.index]
        elif string == "vm":
            vids = [v.index for v in marked.verts]
        else:
            vids = [int(string[1:])]
            
    elif "e" in string:    
        if string == "es":
            vids = selection.edge.isvs
        elif string == "em":
            vids = list(chain(*[e.isvs for e in marked.edges]))
        else:
            vids = cc.edges[int(string[1:])].isvs
    elif "g" in string:
        vids = set()
        if string == "gs":
            seg = selection.segment
            vids |= set(seg.verts())
        elif string == "gm":
            for seg in marked.cubes:
                vids|= set(seg.verts())
        else:
            seg = cc.cubes[int(string[1:])]
            vids |= set(seg.verts())
        vids = list(vids)
    elif "s" in string:
        if string == "ss":
            #side = ovl_json['segments'][str(selected_seg)]['sides'][selected_side]
            vids = selection.side.verts
        elif string == "sm":
            vidss = set()
            for side in marked.faces:
                for v in side.verts:
                    vidss.add(v)
            vids = list(vidss)
        else:
            b = string[1:]#.split('s')
            sideindex = int(b)
            vids = cc.faces[sideindex].verts
    else:
        return None

    return VertexGroup(*[cc.verts[vid].dpos for vid in vids], vids=vids)


def build_replacements(cc, src, dest, axis="xyz"):
    replacements = {}
    for cchar in axis:
        cint = ['x','y','z'].index(cchar)
        if len(src.dpos) == 1:
            for d in dest.vids:
                if d in replacements:
                    ld = list(replacements[d])
                else:
                    ld = list(cc.verts[d].dpos)
                ld[cint] = src.dpos[0][cint]
                replacements[d] = tuple(ld)
        elif len(src.dpos) == len(dest.vids):
            for i in range(len(src.dpos)):
                if dest.vids[i] in replacements:
                    ld = list(replacements[dest.vids[i]])
                else:
                    ld = list(cc.verts[dest.vids[i]].dpos)

                ld[cint] = src.dpos[i][cint]
                replacements[dest.vids[i]] = tuple(ld)
        else:
            raise "incompatible lengths"
    return replacements

def eval_expr(expr, cc, selection, marked):
    s = re.split("([a-z0-9]+)", expr)
    ops = s[::2]
    ents = s[1::2]

    environment = {}
    for string in ents:
        val = entity2(string, cc, selection, marked)
        if val is not None:
            environment[string] = val

    environment["X"] = Displacement((1,0,0))
    environment["Y"] = Displacement((0,1,0))
    environment["Z"] = Displacement((0,0,1))
    environment["L"] = line
    environment["P"] = plane
    environment["R"] = rotate
    environment["D"] = displacement
    r =  eval(expr, environment)
    return r
