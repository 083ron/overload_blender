from .Constants import *
import hsplit.face as face
import hsplit.cube as cube
from .types import PVert
import ctypes
from OpenGL.GL import *
from .glfun import *
import hsplit.shaders as shaders
from itertools import chain
from .GLClasses.DrawInfo import DrawInfo
from numpy import float64, float32
import traceback
import numpy as np
import snap

L = 0
R = 1

SNAP, SPLIT = 0,1

class SplitError(Exception):
    pass

def populate_buffer(buf, array):
    length = len(array)
    
    if type(array[0]) in [float, float64]:
        btype = GL_ARRAY_BUFFER
        ctype = ctypes.c_float
    elif type(array[0]) == int:
        btype = GL_ELEMENT_ARRAY_BUFFER
        ctype = ctypes.c_uint
    else:
        print( type(array[0]))

    size = ctypes.sizeof(ctype)
    glBindBuffer(btype, buf)
    glBufferData(btype, size*length, (ctype*length)(*array), GL_STATIC_DRAW)

class SplitPreview():

    def __init__(self, cc, snapinfo, opts, snap_dist):
#        print "New split: ", line, t, opts, snap_dist
        self.cc = cc
        self.cubes = {}
        self.faces = {}
        self.edges = {}
        self.pverts = {}
        self.line = snapinfo.line
        self.snap_dist = snapinfo.snap_dist
        if opts[SNAP] == SNAP_ON:
            self.t = snapinfo.tsnap
        else:
            self.t = snapinfo.t
        self.split_opts = opts
        self.vao = None
        self.arrays = [None]*4
        self.prog = shaders.interpprog

    def __del__(self):
        try:
            glDeleteBuffers(len(self.arrays), self.arrays)
            self.arrays = [None]*4
            glDeleteVertexArrays(1, [self.vao])
            self.cc.check()
            self.tidy()
        except:
            traceback.print_exc()

    def extra_uniform_locations(self):
        pass

    def set_extra_uniforms(self):
        pass

    def create_vbo(self):
        glUseProgram(self.prog)
        
        self.vao = glGenVertexArrays(1)
        glBindVertexArray(self.vao)
        self.classify_sides()

        self.build_vertex_arrays()

        vertices = self.vertices
        vertices2 = self.vertices2
        
        self.arrays = glGenBuffers(len(self.arrays))

        self.attr1 = glGetAttribLocation(self.prog, 'a_b1')
        self.attr2 = glGetAttribLocation(self.prog, 'a_b2')
        self.k_loc = glGetUniformLocation(self.prog, "k")
        self.alpha_loc = glGetUniformLocation(self.prog, "alpha")
        self.extra_uniform_locations()
        
        populate_buffer(self.arrays[0], self.vertices)
        glVertexAttribPointer(self.attr1, 3, GL_FLOAT, GL_FALSE, 0, None)

        populate_buffer(self.arrays[1], self.vertices2)
        glVertexAttribPointer(self.attr2, 3, GL_FLOAT, GL_FALSE, 0, None)

        populate_buffer(self.arrays[2], self.indices)
        populate_buffer(self.arrays[3], self.lineindices)

        glEnableVertexAttribArray(self.attr1)
        glEnableVertexAttribArray(self.attr2)
        glBindVertexArray(0)
                     
    def build_vertex_arrays(self):

        self.indices = []
        self.lineindices = []

        for cube_id, splitface in self.cubes.items():
            for v in splitface.verts:
                self.indices.append(self.vmap[v])

        for face_id, edge in self.faces.items():
            self.lineindices.append(self.vmap[edge.isvs[0]])
            self.lineindices.append(self.vmap[edge.isvs[1]])

        self.vertices = list(chain(*[self.cc.verts[v].dpos for v in self.vset[L]]))
        self.vertices2 = list(chain(*[self.cc.verts[v].dpos for v in self.vset[R]]))

    def tidy(self):
        """
        Wipe any unconnected faces/edges/verts
        in the cubecomplex.
        """
        cc = self.cc
        for i,face in enumerate(cc.faces):
            if len(face.cubes) == 0:
                for e in face.edges:
                    cc.edges[e].faces.remove(i)
                cc.faces[i] = None
        for i,edge in enumerate(cc.edges):
            if len(edge.faces) == 0:
                for v in edge.isvs:
                    cc.verts[v].edges.remove(i)
                cc.edges[i] = None
        for i,vert in enumerate(cc.verts):
            if len(vert.edges) == 0:
                cc.verts[i] = None
        self.cc.pack()

    def interpolate_uvs(self, face, orig_face):
        for i, edge_id in enumerate(face.edges):
            if edge_id in orig_face.edges:
                edge_pos = orig_face.edges.index(edge_id)
                if orig_face.oris[edge_pos] != face.oris[i]:
                    face.flip()
                    break
        for i, vid in enumerate(face.verts):
            if vid in self.pverts:
                pvert = self.pverts[vid]
                i1 = orig_face.verts.index(pvert.edge.isvs[0])
                i2 = orig_face.verts.index(pvert.edge.isvs[1])
                u1, v1 = orig_face.uvs[i1]
                u2, v2 = orig_face.uvs[i2]
                u = (1.0-pvert.t)*u1 + pvert.t*u2
                v = (1.0-pvert.t)*v1 + pvert.t*v2
                face.uvs[i] = (u,v)
            else:
                face.uvs[i] = orig_face.uvs[orig_face.verts.index(vid)]
        face.tex = orig_face.tex
                
    def draw(self):

        if self.arrays[0] is None:
            self.create_vbo()

        glBindVertexArray(self.vao)

        glUseProgram(self.prog)
        with shaders.draw_info_preview:
            glProgramUniform1f(self.prog, self.k_loc, self.t)
            self.set_extra_uniforms()
            
            glProgramUniform1f(self.prog, self.alpha_loc, 0.5)
            glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, self.arrays[2])
            glDrawElements(GL_QUADS, len(self.indices), GL_UNSIGNED_INT, None)
            
            glProgramUniform1f(self.prog, self.alpha_loc, 1.0)
            glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, self.arrays[3])
            glDrawElements(GL_LINES, len(self.lineindices), GL_UNSIGNED_INT, None)

        glBindVertexArray(0)

    def get_oriented_edge(self, vertex_id):
        pvert = self.pverts[ vertex_id ]
        
        if pvert.sense == 1:
            l,r = 0,1
        else:
            l,r = 1,0
        edge = pvert.edge 
        return edge.isvs[l], edge.isvs[r]

    def classify_sides(self):
        self.vset = [[], []]

        self.vmap = [None]*len(self.cc.verts)

        # Classify existing vertices into left and right sets
        # Split existing edges and record left and right halves.
        for vertex_id, pvert in self.pverts.items():

            edge = pvert.edge 
            lv, rv = self.get_oriented_edge(vertex_id)
            
            self.vset[L].append(lv)
            self.vset[R].append(rv)
            self.vmap[vertex_id] = len(self.vset[L]) - 1

    def do_split(self):
        """
        Split the cubecomplex with the set of split faces
        in this preview object
        """
        print "do split"
        cc = self.cc
        
        faceparts = {}
        self.finalise()
        #self.classify_sides(self)
        orig_edge_split_info = {}
        for vertex_id, pvert in self.pverts.items():

            edge = pvert.edge 
            e1, e2 = edge.split(vertex_id)
            lv, rv = self.get_oriented_edge(vertex_id)
            if pvert.sense == 1:
                inf = [lv, rv, e1, e2]
            else:
                inf = [lv, rv, e2, e1]

            orig_edge_split_info[edge.index] = inf

        for face_id, splitedge in self.faces.items():
            f = [None]*2
            for side in [L, R]:
                orig_face = cc.faces[face_id]
                new_face = []
                for i, iedge in enumerate(orig_face.edges):
                    edge = cc.edges[iedge]
                    if iedge in orig_edge_split_info:
                        new_edge = orig_edge_split_info[iedge][side+2].index
                    elif edge.isvs[0] in self.vset[side]:
                        new_edge = iedge
                    else:
                        new_edge = splitedge.index
                    new_face.append(new_edge)
                        
                f[side] = face.Face(self.cc, new_face)
                f[side].orient()
                self.interpolate_uvs(f[side],orig_face)

            faceparts[face_id] = f
                   
        for cube_id, splitface in self.cubes.items():
            c1 = cc.cubes[cube_id]
            c2 = cube.Cube(self.cc, c1.faces)

            if c1.orig_cube is not None:
                origc = c1.orig_cube
            else:
                origc = c1.index
            c1.orig_cube = origc
            c2.orig_cube = origc

            for i, iface in enumerate(c1.faces):
                orig_face = cc.faces[iface]
                if iface in faceparts:
                    fp = faceparts[iface]
                    c1.replace_face(i, fp[L].index)
                    c2.replace_face(i, fp[R].index)
                elif orig_face.verts[0] in self.vset[L]:
                    c2.replace_face(i, splitface.index)
                elif orig_face.verts[0] in self.vset[R]:
                    c1.replace_face(i, splitface.index)
                else:
                    raise SplitError("Shouldn't happen")

        self.cc.check()
        self.pverts = {}
        self.tidy()
    
    def reparam(self, snapinfo):
        self.t = snapinfo.t

    def finalise(self):
        for vid, v in self.pverts.items():
            sns = v.sense
            if sns == 1:
                t2 = self.t
            else:
                t2 = (1.0-self.t)
            self.pverts[vid] = PVert(v.edge, t2, sns)
            self.cc.verts[vid].dpos = v.edge.eval(t2)
            
    def __repr__(self):
        st = ""
        st += str(self.cubes) + "\n"
        st += str(self.faces) + "\n"
        st += str(self.edges) + "\n"
        st += str(self.pverts) + "\n"
        return "st"

class SnapSplitPreview(SplitPreview):

    def __init__(self, cc, snapinfo, opts, snap_dist):
        SplitPreview.__init__(self, cc, snapinfo, opts, snap_dist)
        self.prog = shaders.interp_snap_prog
        
    def extra_uniform_locations(self):
        self.snap_dist_loc = glGetUniformLocation(self.prog, "snap_dist")

    def set_extra_uniforms(self):
        glProgramUniform1f(self.prog, self.snap_dist_loc, self.snap_dist)

    def reparam(self, snapinfo):
        self.t = snapinfo.tsnap
        self.snap_dist = snapinfo.snap_dist

    def finalise(self):
        for vid, v in self.pverts.items():
            sns = v.sense
            if sns == 1:
                t2 = self.t
            else:
                t2 = (1.0-self.t)

            v1 = np.array(self.cc.verts[v.edge.isvs[0]].dpos)
            v2 = np.array(self.cc.verts[v.edge.isvs[1]].dpos)

            snapinfo = snap.snap(v.edge, v1, v2, t2, self.snap_dist)
            if snapinfo.tsnap < 1e-4 or snapinfo.tsnap+1e-4 > 1.0:
                raise SplitError("t out of range")

            self.cc.verts[vid].dpos = snapinfo.xyzsnap
            self.pverts[vid] = PVert(v.edge, snapinfo.tsnap, sns)
            
class OffsetSplitPreview(SplitPreview):

    def __init__(self, cc, snapinfo, opts, snap_dist):
        SplitPreview.__init__(self, cc, snapinfo, opts, snap_dist)
        self.prog = shaders.interp_offset_prog

    def reparam(self, snapinfo):
        offs = snapinfo.v2 - snapinfo.v1
        self.t = snapinfo.t * np.sqrt(np.dot(offs, offs))

    def finalise(self):
        for vid, v in self.pverts.items():
            sns = v.sense

            if sns == 1:
                l, r = 0,1
            else:
                l, r = 1, 0

            print v.edge.isvs
            v1 = np.array(self.cc.verts[v.edge.isvs[0]].dpos)
            v2 = np.array(self.cc.verts[v.edge.isvs[1]].dpos)
            offs = v2 - v1
            
            t2 = self.t / np.sqrt(np.dot(offs, offs))
            print t2
            if sns == 1:
                pass
            else:
                t2 = (1.0-t2)

            if t2 < 1e-4 or t2+1e-4 > 1.0:
                raise SplitError("t out of range")
            print t2
            #snapinfo = snap.snap(v.edge, v1, v2, t2, self.snap_dist)
            if t2 < 1e-4 or t2+1e-4 > 1.0:
                raise SplitError("t out of range")
            self.cc.verts[vid].dpos = v.edge.eval(t2)
            self.pverts[vid] = PVert(v.edge, t2, sns)



class OffsetSnapSplitPreview(SplitPreview):

    def __init__(self, cc, snapinfo, opts, snap_dist):
        SplitPreview.__init__(self, cc, snapinfo, opts, snap_dist)
        self.prog = shaders.interp_offset_snap_prog
        
    def extra_uniform_locations(self):
        self.snap_dist_loc = glGetUniformLocation(self.prog, "snap_dist")

    def set_extra_uniforms(self):
        glProgramUniform1f(self.prog, self.snap_dist_loc, self.snap_dist)

    def reparam(self, snapinfo):
        offs = snapinfo.v2 - snapinfo.v1
        self.t = snapinfo.tsnap * np.sqrt(np.dot(offs, offs))
        self.snap_dist = snapinfo.snap_dist

    def finalise(self):
        for vid, v in self.pverts.items():
            sns = v.sense

            if sns == 1:
                l, r = 0,1
            else:
                l, r = 1, 0
            v1 = np.array(self.cc.verts[v.edge.isvs[0]].dpos)
            v2 = np.array(self.cc.verts[v.edge.isvs[1]].dpos)
            offs = v2 - v1
            
            t2 = self.t / np.sqrt(np.dot(offs, offs))

            if sns == 1:
                pass
            else:
                t2 = (1.0-t2)

            snapinfo = snap.snap(v.edge, v1, v2, t2, self.snap_dist)
            if snapinfo.tsnap < 1e-4 or snapinfo.tsnap+1e-4 > 1.0:
                raise SplitError("t out of range")
            self.cc.verts[vid].dpos = snapinfo.xyzsnap
            self.pverts[vid] = PVert(v.edge, snapinfo.tsnap, sns)
            #self.cc.verts[vid].dpos = v.edge.eval(t2)
            #self.pverts[vid] = PVert(v.edge, t2, sns)
