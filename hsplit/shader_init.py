from OpenGL.GL import glCreateShader
from OpenGL.GL import *
from .GLClasses.DrawInfo import DrawInfo, DummyDrawInfo
import hsplit.shaders as shaders
import sys
from ctypes import POINTER, c_char
import ctypes

def include(fn):
    fp = open(fn, "r")
    st = fp.readlines()
    fp.close()
    return st


def create_and_compile_shader(typ, str):
    shdr = glCreateShader(typ)
    glShaderSource(shdr, str)
    glCompileShader(shdr)
    return shdr


def build_shader(v, f, g=None):
    print v
    vertex_shader=create_and_compile_shader(GL_VERTEX_SHADER, include(v))
    print f
    fragment_shader=create_and_compile_shader(GL_FRAGMENT_SHADER, include(f))
    if g:
        print g
        geom_shader=create_and_compile_shader(GL_GEOMETRY_SHADER, include(g))

    prog = glCreateProgram()
    glAttachShader(prog, vertex_shader)
    if g:
        glAttachShader(prog, geom_shader)
    glAttachShader(prog, fragment_shader)
    
    glLinkProgram(prog)

    try:
        glUseProgram(prog)
    except OpenGL.error.GLError:
        print(glGetProgramInfoLog(prog))
        sys.exit(1)

    return prog

def build_feedback_shader(v):
    vs =  create_and_compile_shader(GL_VERTEX_SHADER, include(v))
    prog = glCreateProgram()
    glAttachShader(prog, vs)
    varyings = ctypes.create_string_buffer('outValue')
    varyings_pp = POINTER(POINTER(c_char))(ctypes.cast(varyings, POINTER(c_char)))
    glTransformFeedbackVaryings(prog, 1, varyings_pp, GL_INTERLEAVED_ATTRIBS)
    glLinkProgram(prog)
    try:
        glUseProgram(prog)
    except OpenGL.error.GLError:
        print(glGetProgramInfoLog(prog))
        sys.exit(1)
    return prog
    
def init_shaders():

    shaders.renderprog =       build_shader("hsplit/vshad.c",       "hsplit/fshad.c")
    shaders.interp_snap_prog = build_shader("hsplit/interp_snap.c", "hsplit/fshad_color2.c")
    shaders.interpprog =       build_shader("hsplit/interp.c",      "hsplit/fshad_color2.c")
    shaders.interp_offset_prog = build_shader("hsplit/interp_offset.c", "hsplit/fshad_color2.c")
    shaders.interp_offset_snap_prog = build_shader("hsplit/interp_offset_snap.c", "hsplit/fshad_color2.c")
    shaders.selectprog =       build_shader("hsplit/vshad_color.c", "hsplit/fshad_color.c")
    shaders.textureprognofog = build_shader("hsplit/vtex.c",        "hsplit/ftex.c")
    shaders.vbillboardprog =   build_shader("hsplit/vshad_billboard.c", "hsplit/fshad_billboard.c", g="hsplit/gshad_billboard.c")
    shaders.coneprog =         build_shader("hsplit/vshad_billboard.c", "hsplit/fshad_billboard.c", g="hsplit/gshad_cones.c")
    shaders.textureprog =      build_shader("hsplit/vtexfog.c",     "hsplit/ftexfog.c")
    shaders.dmeshprog =        build_shader("hsplit/vtexattr.c",    "hsplit/ftexattr.c")
    shaders.feedbackprog =     build_feedback_shader("hsplit/vshad_feedback.c")
    shaders.marqueeprog =      build_shader("hsplit/vshad_marquee.c", "hsplit/fshad_marquee.c",)

    print "shaders.renderprog",       shaders.renderprog
    print "shaders.interpprog",       shaders.interpprog
    print "shaders.selectprog",       shaders.selectprog
    print "shaders.textureprognofog", shaders.textureprognofog
    print "shaders.coneprog",         shaders.coneprog
    print "shaders.vbillboardprog",   shaders.vbillboardprog
    print "shaders.textureprog",      shaders.textureprog
    print "shaders.dmeshprog",        shaders.dmeshprog
    print "shaders.feedbackprog",     shaders.feedbackprog
    print "shaders.marqueeprog",      shaders.marqueeprog
    
    shaders.draw_info_dummy     = DummyDrawInfo()
    shaders.draw_info_dummy.opts = {}
    
    shaders.draw_info_polys     = DrawInfo(shader = shaders.textureprog,
                                           enable = [GL_POLYGON_OFFSET_FILL])

    shaders.draw_info_select    = DrawInfo(shader    = shaders.selectprog,
                                           linewidth = 40)
    
    shaders.draw_info_highlight = DrawInfo(shader     = shaders.selectprog,
                                           linewidth  = 3,
                                           disable    = [GL_DEPTH_TEST],
                                           depth_mask = False)

    shaders.draw_info_marked    = DrawInfo(shader     = shaders.selectprog,
                                           linewidth  = 2,
                                           disable    = [GL_DEPTH_TEST],
                                           depth_mask = False)
    
    shaders.draw_info_labels    = DrawInfo(shader = shaders.renderprog)

    shaders.draw_info_cones    = DrawInfo(shader = shaders.coneprog)
    
    shaders.draw_info_vbill = DrawInfo(shader = shaders.vbillboardprog,
                                       disable    = [GL_DEPTH_TEST],
                                       depth_mask = False,
                                             uniforms={'psize':3.0})
    
    shaders.draw_info_vbill_large = DrawInfo(shader = shaders.vbillboardprog,
                                       disable    = [GL_DEPTH_TEST],
                                             depth_mask = False,
                                             uniforms={'psize':6.0})
    
    shaders.draw_info_lines     = DrawInfo(shader    = shaders.renderprog,
                                           linewidth = 1.0,
                                           enable    = [GL_BLEND, GL_POLYGON_OFFSET_FILL])

    shaders.draw_info_preview   = DrawInfo(linewidth = 2.0,
                                           enable    = [GL_BLEND, GL_POLYGON_OFFSET_FILL])

    shaders.draw_info_dmesh     = DrawInfo(shader = shaders.dmeshprog,
                                           linewidth = 1.0,
                                           enable    = [GL_POLYGON_OFFSET_FILL])

    shaders.draw_info_feedback  = DrawInfo(shader = shaders.feedbackprog,
                                           enable    = [GL_RASTERIZER_DISCARD])

    shaders.draw_info_marquee   = DrawInfo(disable    = [GL_DEPTH_TEST],
                                           enable    = [GL_BLEND],
                                           linewidth = 1.2,
                                           depth_mask = False)
