import navigation as nav
from ctypes.wintypes import SHORT
from ctypes import WinDLL, c_int
import box_select

pk = WinDLL('user32').GetAsyncKeyState
pk.restype = SHORT
pk.argtypes = [c_int]

def poll(e=None):
    #print  pk(0x12) < -32000
    return pk(0x12) < -32000

def bind_relative(win, keybind, cmds, poll_alt=False):
    """
    Binds the functions in cmds to this widget, supplying them
    each with a mouse movement delta.
    """

    win.bind(keybind, lambda e: calc_relative_coord(win, e), "+")
    if not poll_alt:
        for cmd in cmds:
            win.bind(keybind, lambda e, cmd=cmd: cmd(win.rel[0],win.rel[1]), "+")
    else:
        for cmd in cmds:
            win.bind(keybind, lambda e, cmd=cmd:
                      cmd(win.rel[0],win.rel[1]) if poll() else None, "+")

    win.bind(keybind, lambda e: win.camera.update_angles(), "+")
    win.bind(keybind, lambda e: set_coord_and_redraw(win, e), "+")

def bind_nopoll(win, keybind, cmd2):
    win.bind(keybind, lambda e, cmd=cmd2: cmd(e) if not poll() else None, "+")

def calc_relative_coord(win, event):
    """
    Returns the difference between the current mouse
    coords and the next most recent mouse coord.
    """
    if win.lastxy is None:
        win.rel = (0,0)
        win.lastxy = (event.x, event.y)
    else:
        win.rel = (event.x-win.lastxy[0],
                    event.y-win.lastxy[1])

def set_coord_and_redraw(win, event):
    """
    Remember the mouse coords of the last event, for
    calculating relative movement. Redraw the display
    as this is called with each movement in a mouse drag.
    """
    win.lastxy = (event.x, event.y)
    win.tick()

def run_nav(win, cmd, e):
    cmd(e)
    win.selection.mark_stale()
    win.update_selection_label()
    win.tick()

def bind_nav(win, key, cmd):
    win.bind(key, lambda e: run_nav(win, cmd, e))

def camera_bindings_unreal(win):
    c = win.camera
    bind_relative(win, "<B1-B3-Motion>", [win.clear_mb, c.strafe_h, c.strafe_v])
    bind_relative(win, "<B1-Motion>",    [win.clear_mb, c.forwards, c.yaw])
    bind_relative(win, "<B3-Motion>",    [win.clear_mb, c.pitch, c.yaw])
    
def camera_bindings_ole(win):
    bind_nopoll(win, "<B1-Motion>", lambda e: box_select.drag(win, e))
    c = win.camera
    bind_relative(win, "<B1-Motion>", [c.pitch, c.yaw], poll_alt=True)
    win.bind(          "<B1-Motion>", win.clear_mb, "+")
    bind_relative(win, "<B2-Motion>", [c.strafe_h, c.strafe_v])
    win.bind(          "<B2-Motion>", win.clear_mb, "+")
    win.parent.bind("<MouseWheel>", win.mouse_wheel)
    
def main_window_bindings(win):
    bind_nav(win, "<Key-c>", lambda e: nav.cycle_seg(win.cc, win.selection, 1))
    bind_nav(win, "<Key-C>", lambda e: nav.cycle_seg(win.cc, win.selection, -1))

    bind_nav(win, "<Key-x>", lambda e: nav.cycle_side(win.cc, win.selection, 1))
    bind_nav(win, "<Key-X>", lambda e: nav.cycle_side(win.cc, win.selection, -1))

    bind_nav(win, "<Key-n>", lambda e: nav.next_seg(win.cc, win.selection))
    bind_nav(win, "<Key-N>", lambda e: nav.prev_seg(win.cc, win.selection))

    win.bind("<ButtonPress>",    win.mouse_down)
    win.bind("<KeyPress-space>", win.mark)
    win.bind("<Shift-space>",    win.mark_unmark_all)
    win.bind("<ButtonRelease>",  win.mouse_up)
    win.bind("<Motion>",         win.mouse_move)

    win.bind("<Key-q>", lambda e: win.mark_same_texture())
    win.bind("<Key-Q>", lambda e: win.mark_coplanar())
