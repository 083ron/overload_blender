import numpy as np
from collections import namedtuple

SnapInfo = namedtuple('SnapInfo', ['line','t','v1','v2','xyz','tsnap','xyzsnap','snap_dist'])

def snap(line, xyz1, xyz2, t, snap_dist):
    diff = xyz2-xyz1
    delta = np.absolute(diff)
    xpos = (xyz1*(1.0-t)) + (xyz2*t)
    imax = np.argmax(delta)
    tsnap = ((np.around(xpos[imax]/snap_dist)*snap_dist)-xyz1[imax]) / diff[imax]
    dc_dt = snap_dist / delta[imax]
    if tsnap <= 1e-4:
        tsnap = dc_dt
    if tsnap + 1e-4 >= 1.0:
        tsnap = 1.0-dc_dt
    xsnap = (xyz1*(1.0-tsnap)) + (xyz2*tsnap)
        
    return SnapInfo(line=line,
                    t=t,
                    v1=xyz1,
                    v2=xyz2,
                    xyz=xpos,
                    tsnap=tsnap,
                    xyzsnap=xsnap,
                    snap_dist=snap_dist)
