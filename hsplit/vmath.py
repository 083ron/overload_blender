from operator import add, sub, mul
import math
D2R = math.pi/180.0

def cross(v1, v2):
    "3D Vector cross product"
    return [v1[1]*v2[2] - v1[2]*v2[1],
            v1[2]*v2[0] - v1[0]*v2[2],
            v1[0]*v2[1] - v1[1]*v2[0]]

def dot(v1, v2):
    "N-dimensional Dot product"
    return sum(map(mul,v1,v2))


def smul(s, v):
    "Elementwise scalar multiply"
    return [s*vn for vn in v]


def sdiv(s, v):
    "Elementwise scalar divide"
    return [vn/s for vn in v]


def sadd(v, w):
    "Elementwise addition"
    return list(map(add, v,w))


def ssub(v, w):
    "Elementwise addition"
    return list(map(sub, v,w))


def plane_normal(plane_points):
    """
    Normal vector to plane defined by three points
    Note, no check for colinearity or degeneracy
    """
    return cross(ssub(plane_points[1], plane_points[0]),
                 ssub(plane_points[2], plane_points[0]))

def magnitude(v):
    mag = math.sqrt(dot(v,v))
    return mag

def normalise(v):
    """
    Scale any vector to unit length
    """
    return sdiv(magnitude(v), v)

def pointonplane(point, plane_points):
    """
    Project point onto infinte plane defined by three points
    """
    unit_normal = normalise(plane_normal(plane_points))
    fac = dot( ssub(point, plane_points[0]), unit_normal)
    return ssub(point, smul(fac, unit_normal))


def pointonline(point, line_points):
    """
    Project a point onto infinite line defined by two points.
    """
    p1 = line_points[0]
    p2 = line_points[1]
    v = ssub(p2,p1)
    vmag = math.sqrt(dot(v,v))
    vn = sdiv(vmag, v)
    fac = dot(ssub(point, p1), vn)
    return sadd(p1, smul(fac, vn))


def planeintersectplane(p_points, q_points):
    """
    Returns two points that lie on the line of intersection
    between two planes.
    """
    pnorm = normalise(plane_normal(p_points))
    qnorm = normalise(plane_normal(q_points))
    line_dir = cross(pnorm, qnorm)
    along_qq = cross(line_dir, qnorm)
    p1 = lineintersectplane( [q_points[0], sadd(q_points[0], along_qq)],
                             p_points)[0]
    return [p1,
            sadd(p1, line_dir)]


def lineintersectplane(line_points, plane_points):
    "Returns intersection point between a line and a plane"
    norm = plane_normal(plane_points)
    lv = ssub(line_points[1], line_points[0])
    
    fac = (dot(norm, ssub(plane_points[0], line_points[0])) /
           dot(norm, lv))
    return [sadd(line_points[0],smul(fac, lv))]



def box_code(x,y, bx1, by1, bx2, by2):
    code = 0
    if x < bx1:
        code |= 8
    elif x > bx2:
        code |= 4
    if y < by1:
        code |= 2
    elif y > by2:
        code |=1
    return code

def lb(x1,y1,x2,y2, bx1, by1, bx2, by2):
    bc =  box_code(x1, y1, bx1, by1, bx2, by2)
    bc2 = box_code(x2, y2, bx1, by1, bx2, by2)

    if bc == 0 and bc2 == 0:
        return True
    if bc&bc2 == 0:
        return rect_intersect(x1,y1,x2,y2, bx1, by1, bx2, by2)
    return False

def rect_intersect(x1,y1,x2,y2, bx1, by1, bx2, by2):
    if xin(x1,y1,x2,y2, bx1, by1, by2):
        return True
    if xin(x1,y1,x2,y2, bx2, by1, by2):
        return True
    if yin(x1,y1,x2,y2, by1, bx1, bx2):
        return True
    if yin(x1,y1,x2,y2, by2, bx1, bx2):
        return True
    return False
    
def xin(x1,y1,x2,y2, x0, by1, by2):
    fac = (x0-x1)/(x2-x1)
    y0 = y1 + fac*(y2-y1)
    return y0<by2 and y0>by1

def yin(x1,y1,x2,y2, y0, bx1, bx2):
    fac = (y0-y1)/(y2-y1)
    x0 = x1 + fac*(x2-x1)
    return x0<bx2 and x0>bx1
