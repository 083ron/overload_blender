from GLClasses.Regenerable import Regenerable
from OpenGL.GL import *
from hsplit.edit import build_replacements
import shaders
from Constants import *
import edit

class EditPreview(Regenerable):

    def __init__(self, cc, src, dest, axes):
        Regenerable.__init__(self)
        self.cc = cc
        self.dest = dest
        self.src = src
        self.axes = axes
        self.replacements = None
        self.register_draw_info((RENDER,), shaders.draw_info_highlight)
        
    def get_replacements(self):
        if self.replacements is None:
            self.replacements = edit.build_replacements(self.cc,self.src,self.dest, axis=self.axes)
        return self.replacements
    
    def draw_geometry(self, mode, opts):
        if self.dest.vids is None:
            return
        repl = self.get_replacements()
        glBegin(GL_LINES)
        for d in self.dest.vids:
            glVertex3fv(self.cc.verts[d].dpos)
            glVertex3fv(repl[d])
        glEnd()
