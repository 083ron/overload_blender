import numpy as np
from numpy.linalg import inv
from vmath import *
def face_basis(cc, face_id, edge_id):
    face = cc.faces[face_id]
    edge = cc.edges[edge_id]
    vid1, vid2 = edge.isvs
    fl1 = face.verts.index(vid1)
    fl2 = face.verts.index(vid2)

    if fl1 > fl2 or (fl1==0 and fl2==3):
        vid1, vid2 = vid2, vid1
        fl1, fl2 = fl2, fl1
    fl3 = (fl2+2)%4
    ev1 = ssub(cc.verts[vid2].dpos,cc.verts[vid1].dpos)
    ev2 = ssub(cc.verts[face.verts[fl3]].dpos,cc.verts[vid1].dpos)

    nrm = normalise(cross(ev1,ev2))
    u = normalise(ev1)
    v = -np.array(cross(u,nrm))
    basis = np.transpose(np.array([u,v,nrm]))

    inv_basis = inv(basis)

    for i,vert in enumerate(face.verts):
        uv = np.dot(inv_basis, cc.verts[vert].dpos-np.array(cc.verts[vid1].dpos))/0.4
        face.uvs[i] = tuple(uv[:2])

