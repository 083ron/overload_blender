from Constants import *
from OpenGL.GL import *
import numpy
import shaders
from glfun import loadOverloadTexture
import json
import os
import config

class Dmesh:

    def __init__(self):
        self.modes = {}
        self.tex_ids = []
        
    def register_draw_info(self, mode, info):
        self.modes[mode] = info

    def load_from_dmesh_file(self, dmeshfile):
        fp = open(dmeshfile, "r")
        j = json.load(fp)
        fp.close()

        vdata = []
        indices = []
        uvs = []

        self.tex_vert_offsets = [0]
        self.nlins = 0
        ntex = len(j['tex_names'])
        vperm = {}
        tp = {}
        flat = 0

        ntris = 0

        for itex in range(ntex):
            for ti, t in j['triangles'].iteritems():
                if t['tex_index'] != itex:
                    continue
                ntris += 1
                #indices += t['verts']
                for i in range(3):
                    vi = t['verts'][i]
                    v = j['verts'][vi]
                    vdata.append(v['x']/10.0)
                    vdata.append(v['y']/10.0)
                    vdata.append(v['z']/10.0)
                    if vi not in vperm:
                        vperm[vi] = flat
                    flat += 1
                for uv in t['uvs']:
                    uvs.append(uv['u'])
                    uvs.append(-uv['v'])

                for i in range(3):
                    v = vperm[t['verts'][i%3]]
                    v2 = vperm[t['verts'][(i+1)%3]]
                    if (v,v2) not in tp and (v2,v) not in tp:
                        indices.append(v)
                        indices.append(v2)
                        tp[(v,v2)] = 1
                        self.nlins += 1

            self.tex_vert_offsets.append(ntris*3)

        for tname in j['tex_names']:
            self.tex_ids.append(loadOverloadTexture(tname))

        self.load_verts(vdata, uvs, indices)
        
    def load_verts(self,vertdata, uvdata, indices):
        info = self.modes[(RENDER,)]

        glUseProgram(info.shader)
        self.nverts = len(vertdata) / 3
        
        vert_loc = glGetAttribLocation(info.shader, "vert")
        uv_loc = glGetAttribLocation(info.shader, "uV")
        self.proj_loc = glGetUniformLocation(info.shader, 'pMatrix')
        self.mv_loc = glGetUniformLocation(info.shader, "mvMatrix")
        self.tex_loc = glGetUniformLocation(info.shader, "textureimg")
        
        # create and bind VAO       
        self.vao = glGenVertexArrays(1)
        glBindVertexArray(self.vao)
 
        # generate vertex buffer
        vertexBuffer = glGenBuffers(1)
        glBindBuffer(GL_ARRAY_BUFFER, vertexBuffer)
        glEnableVertexAttribArray(vert_loc)
        glVertexAttribPointer(vert_loc, 3, GL_FLOAT, GL_FALSE, 0, None)
        vertexData = numpy.array(vertdata, numpy.float32)
        glBufferData(GL_ARRAY_BUFFER, 4 * len(vertexData), vertexData, GL_STATIC_DRAW)

        # Generate index buffer
        iBuffer = glGenBuffers(1)
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, iBuffer)
        indexData = numpy.array(indices, numpy.int32)
        glBufferData(GL_ELEMENT_ARRAY_BUFFER, 4 * len(indexData), indexData, GL_STATIC_DRAW)

        # generate UV buffer
        uvBuffer = glGenBuffers(1)
        glBindBuffer(GL_ARRAY_BUFFER, uvBuffer)
        glEnableVertexAttribArray(uv_loc)
        glVertexAttribPointer(uv_loc, 2, GL_FLOAT, GL_FALSE, 0, None)
        uvData = numpy.array(uvdata, numpy.float32)
        glBufferData(GL_ARRAY_BUFFER, 4 * len(uvData), uvData, GL_STATIC_DRAW)

        # unbind and disable
        glBindVertexArray(0)
        glDisableVertexAttribArray(vert_loc)
        glDisableVertexAttribArray(uv_loc)
        glBindBuffer(GL_ARRAY_BUFFER, 0)
    
 
    def draw(self, mode, mvmat, pmat, **kwargs):

        if mode not in self.modes:
            return
        info = self.modes[mode]
        with info:
            glUseProgram(info.shader)

            # set uniforms
            glUniformMatrix4fv(self.mv_loc, 1, GL_FALSE, mvmat)
            glUniformMatrix4fv(self.proj_loc, 1, GL_FALSE, pmat)
            glUniform1i(self.tex_loc, 0)

            # bind VAO
            glBindVertexArray(self.vao)

            # bind background texture

            # draw
            for i in range(len(self.tex_ids)):
                glBindTexture(GL_TEXTURE_2D, self.tex_ids[i])
                start = self.tex_vert_offsets[i]
                end = self.tex_vert_offsets[i+1]
                glDrawArrays(GL_TRIANGLES, start, end-start)

            glUseProgram(shaders.renderprog)
            glColor4f(0.8,0.8,0.8,0.0)
            glDrawElements(GL_LINES, self.nlins*2, GL_UNSIGNED_INT, None)

            # unbind
        glBindVertexArray(0)
        glUseProgram(0)
        
