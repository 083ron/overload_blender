#version 130
in vec3 a_b1;
in vec3 a_b2;
//out vec3 pos2;
uniform float k;
uniform float alpha;
uniform float snap_dist;

void main() {

  vec3 diff = abs(a_b2 - a_b1);
  float t2;

  t2 = k / length(diff);

  vec3 pos = a_b1*(1.0-t2) + a_b2*t2;
  
  vec3 rpos = ((round(pos/snap_dist)*snap_dist) - a_b1) / (a_b2 - a_b1);
  
  float k2;
  float dc_dt;
  if (diff[0] > diff[1] && diff[0] > diff[2]) {
    k2 = rpos[0];
    dc_dt = snap_dist / diff[0];
  } else {
    if (diff[1] > diff[2]) {
      k2 = rpos[1];
      dc_dt = snap_dist / diff[1];
    } else {
      k2 = rpos[2];
      dc_dt = snap_dist / diff[2];
    }
  }
  gl_FrontColor = vec4(0.0,1.0,0.0,alpha); 
  if (k2 <= 1e-4) {
    k2 = dc_dt;
    gl_FrontColor = vec4(1.0, 0.5, 0.0, alpha);
  }
  if ( k2 +1e-4 >= 1.0 ) {
    k2 = 1.0-dc_dt;
    gl_FrontColor = vec4(1.0, 0.5, 0.0, alpha);
  }
  
  vec3 pos3 = a_b1*(1.0-k2) + a_b2*k2;
  
  gl_Position = gl_ModelViewProjectionMatrix * vec4(pos3, 1.0);

  if (k2 >= 1.0 || k2 <= 0.0) {
    gl_FrontColor = vec4(1.0,0.0,0.0,alpha);
  }
}
