varying float fogFactor;

void main()
{
    vec3 color;

    gl_FragColor = mix(gl_Fog.color, gl_Color , fogFactor);

}
