from Tkinter import *
from ctypes import windll, c_int
import re
from ctypes.wintypes import LONG, HWND, BOOL, UINT
import re
user32 = windll["user32"]

SetWindowLong = user32.SetWindowLongA
SetWindowLong.restype = LONG
SetWindowLong.argtypes = [HWND, c_int, LONG]

SetWindowPos = user32.SetWindowPos
SetWindowPos.restype = BOOL
SetWindowPos.argtypes = [HWND, HWND, c_int, c_int, c_int, c_int, UINT]

def borderless(toplevel):
    hwp = windll.user32.GetParent(toplevel.winfo_id())
    prev = SetWindowLong(hwp, -16, 0)
    prev = SetWindowLong(hwp, -16, prev & ~(0x800000|0xc00000|0x40000))
    prev = SetWindowLong(hwp, -20, 0)
    prev = SetWindowLong(hwp, -20, prev & ~(0x100|0x200|0x20000|0x1))
    print re.split("[+x-]",toplevel.wm_geometry())
    w,h,x,y = [int(a) for a in re.split("[+x-]",toplevel.wm_geometry())]
    #toplevel.wm_geometry("%sx%s"%(w-24,h))
    toplevel.wm_attributes("-alpha",0.7)


