attribute vec3 a_b1;
attribute vec3 a_b2;

uniform float k;
uniform float alpha;


void main() {
    vec3 pos = a_b1*(1.0-k) + a_b2*k;
    gl_Position = gl_ModelViewProjectionMatrix * vec4(pos, 1.0);
    gl_FrontColor = vec4(0.0,1.0,0.0,alpha);
}
