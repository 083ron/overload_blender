import json
import os

class ConfigObj:

    def __init__(self, name):

        configdir = os.getenv('LOCALAPPDATA')
        if configdir == "":
            configdir = os.getenv('HOME')

        configdir = os.path.join(configdir, name)

        if not os.path.exists(configdir):
            os.mkdir(configdir)

        configfile = os.path.join(configdir, "config.json")
        if not os.path.exists(configfile):
            fp = open(configfile, "w")
            fp.write('{\n"LEVEL_TEXTURES" : "%LEVEL_TEXTURES%"\n}\n')
            fp.close()
        self.filename = configfile
        self.read()
        
    def read(self):
        fp = open(self.filename, "r")
        self.json = json.load(fp)
        fp.close()
        
    def write(self):
        fp = open(self.filename, "w")
        fp.write(json.dumps(self.json, indent=2, separators=(',', ': ')))
        fp.close()

    def __getitem__(self, index):
        if index not in self.json:
            self.json[index] = None
            return None
        return self.json[index]

    def __setitem__(self, index, value):
        self.json[index] = value 
