from math import *
import sys
from OpenGL.GL import *
from OpenGL.GLU import gluProject
#from OpenGL.GLUT import glutBitmapCharacter, GLUT_BITMAP_9_BY_15
from hsplit.Constants import *
from PIL import Image
import hsplit.shaders as shaders
import hsplit.glsetup as glsetup

def glp(x):
    glVertex3f(x[0],x[1],x[2])

def set_select_colour(mode, typ):
    if SELECT in mode:
        if typ in mode:
            glColor3fv(SELECT_COLOUR)
        else:
            glColor3fv(WHITE)
    
def cross(v1,v2):
    return ((v1[1]*v2[2])-(v2[1]*v1[2]), \
            (v2[0]*v1[2])-(v1[0]*v2[2]), \
            (v1[0]*v2[1])-(v2[0]*v1[1]))

def dot(v1,v2):
    return v1[0]*v2[0] + v1[1]*v2[1] + v1[2]*v2[2]

def unit(v):
    mod = sqrt(v[0]**2+v[1]**2+v[2]**2)
    if mod==0.0:
        return (0.0,0.0,0.0)
    return (v[0]/mod,v[1]/mod,v[2]/mod)

def xyzToPolar(t):
    x = t[0][0]
    y = t[1][0]
    z = t[2][0]
    return (sqrt(x**2+y**2),atan2(x,y),z,1.0)

def polarToXyz(t):
    r = t[0][0]
    th = t[1][0]
    z = t[2][0]
    return (r*sin(th),r*cos(th),z,1.0)

def trinorm(p1,p2,p3):
    v1 = [0,0,0]
    v2 = [0,0,0]
    for i in range(3):
        v1[i] = p1[i]-p2[i]
        v2[i] = p2[i]-p3[i]
    norm = cross(v1,v2)
    return unit(norm)
    if u == (0.0,0.0,0.0):
        return 

def point_line_distsq(pt,ee1,ee2):
    v1 = ee2-ee1
    v2 = pt-ee1
    denom = dot(v1,v1)
    if denom == 0.0:
        t= 0.0
    else:
        t = dot(v1,v2)/denom

    if t < 0.0: t = 0.0
    if t > 1.0: t = 1.0
    pt2 = t*ee2 + (1.0-t)*ee1
    distv = pt2-pt
    distsq = dot(distv,distv)
    return distsq

