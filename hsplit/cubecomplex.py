from .cube import *
from .face import *
from .edge import *
import os
import hsplit.shaders as shaders
from .glfun import loadImage
from .config import config
import json
from itertools import chain
from overload.json_fragments import DEFAULT_DECALS
from collections import OrderedDict, deque
import overload.overload_parser as op
import copy
from .GLClasses.DrawList import DrawList
from .Constants import *
import numpy

VERTEXMAP = [
    [7,6,2,3],
    [0,4,7,3],
    [0,1,5,4],
    [2,6,5,1],
    [4,5,6,7],
    [3,2,1,0]
]

def packarray(arr, perm = None):
    """
    In place array pack. Removes all occurences of None
    from arr, permutes indices according to perm if
    provided, and returns a permutation dict for arr.
    """
    vperm = {}

    # Packing works by sweeping two pointers over the arary,
    # any entries at the top of the array get moved to any
    # blank spaces at the bottom.
    ibottom = 0
    while True:
        iv = len(arr)-1
        while arr[iv] is None:
            # Delete blank entries at the top
            del arr[iv]
            vperm[iv] = "blank"
            iv -= 1
            if iv == 0: break

        if iv == 0: break

        while arr[ibottom] is not None:
            # Search for the blank space nearest the start
            if ibottom >= len(arr)-1: break
            ibottom += 1
        if ibottom >= len(arr)-1: break

        # Move highest entry into lowest blank space
        arr[ibottom] = arr[iv]
        arr[ibottom].index = ibottom
        del arr[iv]
        vperm[iv] = ibottom
        vperm[ibottom] = "blank"
        iv -= 1
        if len(arr) == 0: break

    return vperm

class CubeComplex:
    """
    Class to store a collection of connected cubes.
    """

    def __init__(self):
        self.json = None

        self.verts = DrawList()
        self.verts.register_draw_info((PICK, VERTEX), shaders.draw_info_cones)
        self.verts.name="verts"
        self.verts2d = None
        self.edges = DrawList()
        self.edges.name="edges"
        self.edges.register_draw_info((PICK, EDGE), shaders.draw_info_select)
        self.edges.register_draw_info((PICK, EDGE_SPLIT), shaders.draw_info_select)
        self.edges_internal = DrawList()
        self.edges_external = DrawList()
        self.edges_internal.register_draw_info((RENDER,), shaders.draw_info_lines)
        self.edges_external.register_draw_info((RENDER,), shaders.draw_info_lines)
        self.edges_internal.name="edges_internal"
        self.edges_external.name="edges_external"
        
        self.faces = DrawList()
        self.faces.name="faces"
        self.faces.register_draw_info((RENDER,), shaders.draw_info_polys)
        self.faces.register_draw_info((PICK, SIDE), shaders.draw_info_select)
        self.faces.register_draw_info((PICK, SEGMENT), shaders.draw_info_select)
        self.faces.modes[(RENDER,)].opts['connectivity'] = 1
        self.faces.modes[(PICK, SIDE)].opts['connectivity'] = 1
        self.faces.modes[(PICK, SEGMENT)].opts['connectivity'] = 1
        self.cubes = DrawList()
        self.cubes.name="cubes"
        self.etups = {}
        self.ftups_v = {}
        self.texids = {}
        self.vao = glGenVertexArrays(1)
        glBindVertexArray(self.vao)

        self.vertex_buffer = glGenBuffers(1)
        glBindBuffer(GL_ARRAY_BUFFER, self.vertex_buffer)
        vert_loc = glGetAttribLocation(shaders.feedbackprog, "vert")
        self.proj_loc = glGetUniformLocation(shaders.feedbackprog, 'pMatrix')
        self.mv_loc = glGetUniformLocation(shaders.feedbackprog, "mvMatrix")
        
        glEnableVertexAttribArray(vert_loc)
        glVertexAttribPointer(vert_loc, 3, GL_FLOAT, GL_FALSE, 0, None)

        self.vertex_2d_buffer = glGenBuffers(1)
        glBindBuffer(GL_ARRAY_BUFFER, self.vertex_2d_buffer)
        self.items = {VERTEX:self.verts,
                      EDGE:self.edges,
                      SIDE:self.faces,
                      SEGMENT:self.cubes}
        
    def __del__(self):
        glDeleteVertexArrays(1, self.vao)
        glDeleteBuffers(1, self.vertex_buffer)
        
    def fill_vertex_array(self):
        vertexData = numpy.array(list(chain(*[v.dpos for v in self.verts])), numpy.float32)
        glBindBuffer(GL_ARRAY_BUFFER, self.vertex_buffer)
        glBufferData(GL_ARRAY_BUFFER, 4 * len(vertexData), vertexData, GL_STATIC_DRAW)
        glBindBuffer(GL_ARRAY_BUFFER, self.vertex_2d_buffer)
        glBufferData(GL_ARRAY_BUFFER, 4 * 4 * len(self.verts), None, GL_STATIC_READ)

    def add_edge(self, v1, v2):
        """
        Adds an edge to this cubecomplex, eliminating
        duplicates.
        """
        etup = tuple(sorted([v1,v2]))
        if etup in self.etups:
            edge = self.etups[etup]
        else:
            edge = Edge(self, [v1,v2])
            self.etups[etup] = edge
        return edge.index

    
    def add_face(self, side):
        """
        Adds a face to this cubecomplex, eliminating duplicates.
        """
        fverts = side['verts']
        ftup = tuple(sorted(fverts))

        if ftup in self.ftups_v:
            face = self.ftups_v[ftup]
        else:
            edgs = []
            for i in range(4):
                edgs.append(self.add_edge(fverts[i], fverts[(i+1)%4]))
            tex_name = side['tex_name']
            if tex_name not in self.texids:
                try:
                    self.texids[tex_name] = loadImage(os.path.join(config['LEVEL_TEXTURES'],tex_name+".png"))
                except:
                    self.texids[tex_name] = loadImage("hsplit/placeholder.png")

            texid = self.texids[tex_name]

            # Cast to float to catch the string "NaN"
            uvs = [(float(uv['u']),-float(uv['v'])) for uv in side['uvs']]
            face = Face(self, edgs, verts = side['verts'],
                        uvs=uvs, tex=texid)
            face.marked_on_load = side['marked']
            self.ftups_v[ftup] = face
        return face
            

    def classify_edges(self):
        """
        Sort edges into two drawlists, one for
        all those on the outer skin of the cubecomplex,
        another for all those in the interior.
        """
        self.edges_internal.items = []
        self.edges_external.items = []
        for e in self.edges:
            n = 0
            for face_id in e.faces:
                if len(self.faces[face_id].cubes) == 1:
                    n += 1
            if n==2:
                self.edges_external.append(e)
            else:
                self.edges_internal.append(e)

    def walk_faces(self, initial_face, accept_face_pair):
        faces = set()

        faces_todo = deque()
        faces_todo.append(initial_face)

        while len(faces_todo) > 0:
            f = faces_todo.pop()

            for edge_id in f.edges:
                edge = self.edges[edge_id]
                faces2 = [self.faces[fid] for fid in edge.faces if fid != f.index]
                
                for f2 in faces2:
                    if f2 not in faces:
                        if len(f.cubes) == 1 and \
                           len(f2.cubes) == 1 and \
                           accept_face_pair(self, edge, f, f2):
                            faces.add(f2)
                            faces_todo.append(f2)
        return faces

    def exterior_edge_parents(self, edge):
        exf = []
        for face_id in edge.faces:
            face = self.faces[face_id]
            if len(face.cubes) == 1:
                exf.append(face)
        return exf

    def reload_overload(self):
        self.etups = {}
        self.ftups_v = {}
        self.texids = {}
        self.json = None
        self.load_overload(self.orig_filename)

    def load_overload(self, filename):
        self.verts.empty()
        self.edges.empty()
        self.faces.empty()
        self.cubes.empty()
        fp = open(filename, "r")
        self.orig_filename = filename
        ovl_json = json.load(fp, object_pairs_hook=OrderedDict)
        self.json = ovl_json
        fp.close()

        vertex_xyzs = op.get_vertex_xyzs(ovl_json, blender_flip=True, scale = 0.1)
        mvs = op.get_marked_verts(ovl_json)
        vtxs = []
        for tp in vertex_xyzs:
            vtxs.append(Vertex(self, tp))
            if len(vtxs)-1 in mvs:
                vtxs[-1].marked_on_load = True
           
        for segid, segment in ovl_json['segments'].items():
            fcs = []

            for side in segment['sides']:
                fcs.append(self.add_face(side).index)
            cube = Cube(self, fcs)
            cube.marked_on_load = segment['marked']
            cube.orig_id = int(segid)

        for i, v in enumerate(self.verts):
            if len(v.edges) == 0 or v.dpos is None:
                # Vertex is orphaned or missing
                self.verts[i] = None

        self.pack()
        self.verts.mark_stale()
        self.edges.mark_stale()
        self.faces.mark_stale()
        self.cubes.mark_stale()
        self.fill_vertex_array()
        print "Loaded: ", len(self.cubes)
    
    def export_overload(self, filename=None):
        self.pack()
        self.json["properties"]["num_marked_sides"] = 0
        self.json["properties"]["num_marked_vertices"] = 0
        self.json["properties"]["num_marked_entities"] = 0
        self.json["properties"]["num_marked_segments"] = 0
        self.json["properties"]["selected_segment"] = 0
        self.json["properties"]["selected_vertex"] = 0
        self.json["properties"]["selected_entity"] = -1
        self.json["properties"]["next_entity"] = 0
        
        self.json["properties"]["num_segments"] = len(self.cubes)
        self.json["properties"]["next_segment"] = len(self.cubes)
        self.json["properties"]["num_vertices"] = len(self.verts)
        self.json["properties"]["next_vertex"] = len(self.verts)

        self.json["verts"] = OrderedDict()
        
        for v in self.verts:
            k = str(v.index)
            self.json['verts'][k] = OrderedDict()
            self.json['verts'][k]['marked'] = False
            self.json['verts'][k]['x'] = v.dpos[0]*10.0
            self.json['verts'][k]['y'] = v.dpos[2]*10.0
            self.json['verts'][k]['z'] = v.dpos[1]*10.0
        
        oldsegs = self.json["segments"]
        newsegs = OrderedDict()
        
        for cube in self.cubes:
            key = str(cube.index)
            verts = cube.order_verts_for_overload()

            if cube.orig_cube is not None:
                origkey = str(cube.orig_cube)
                newseg = copy.deepcopy(oldsegs[origkey])
            else:
                newseg = copy.deepcopy(oldsegs[key])

            newseg['verts'] = verts[:]
            for i, side in enumerate(newseg["sides"]):
                face = self.faces[cube.faces[i]]
                nv = [verts[j] for j in VERTEXMAP[i]]
                side['verts'] = nv
                side['marked'] = False
                origv = [face.verts.index(j) for j in side['verts']]
                side['uvs'] = [None]*4
                #if cube.orig_cube is not None:
                for k in range(4):
                    side['uvs'][k] = {"u":face.uvs[origv[k]][0],
                                      "v":-face.uvs[origv[k]][1]}

                ngh = face.other_cube(cube.index)
                if ngh is not None:
                    newseg['neighbors'][i] = ngh
                else:
                    newseg['neighbors'][i] = -1
            newseg['marked'] = False
            newsegs[key] = newseg

        self.json["segments"] = newsegs

        if filename is not None:
            self.orig_filename = filename

        op.write_backup(self.orig_filename)
        fp = open(self.orig_filename, "w")
        fp.write(json.dumps(self.json, indent=2, separators=(',', ': ')))
        fp.close()

    def mark_stale(self, mode=None):
        self.fill_vertex_array()
        self.verts.mark_stale()
        self.edges.mark_stale()
        self.edges_internal.empty()
        self.edges_external.empty()
        self.edges_internal.mark_stale()
        self.edges_external.mark_stale()
        self.faces.mark_stale()
        self.cubes.mark_stale()
        

    def __repr__(self):
        st = ""
        for elist in [self.verts, self.edges, self.faces, self.cubes]:
            for e in elist:
                st+=str(e)
                if e is None:
                    st+="\n"
        return st
    
    def check(self):
        for f in self.faces:
            for ei in f.edges:
                if f.index not in self.edges[ei].faces:
                    print("edge %d missing back pointer to face %d"%(ei,f.index))


    def draw(self, mode, opts):
        if len(self.edges_external) == 0:
            self.classify_edges()
    
        if RENDER in mode:
            self.faces.draw(mode)
            glColor4f(0.5,0.5,0.55,1.0)
            self.edges_external.draw(mode)
            glColor4f(0.5,0.5,0.5,0.2)
            self.edges_internal.draw(mode)

        elif PICK in mode:
            if EDGE in mode or EDGE_SPLIT in mode:
                self.edges.draw(mode)
            elif SIDE in mode or SEGMENT in mode:
                self.faces.draw(mode)
            elif VERTEX in mode:
                # Redraw verts every frame, inefficient but good enough
                # for now.
                self.verts.mark_stale(mode)
                self.verts.draw(mode)

        elif LABEL in mode:
            pass

    def calc_clipped_edges_2d(self):
        self.fill_vertex_array()
        glUseProgram(shaders.feedbackprog)
        glBindVertexArray(self.vao)

        mvmat = glGetDoublev(GL_MODELVIEW_MATRIX)
        pmat = glGetDoublev(GL_PROJECTION_MATRIX)

        glUniformMatrix4fv(self.mv_loc, 1, GL_FALSE, mvmat)
        glUniformMatrix4fv(self.proj_loc, 1, GL_FALSE, pmat)

        glBindBuffer(GL_ARRAY_BUFFER, self.vertex_buffer)
        glBindBufferBase(GL_TRANSFORM_FEEDBACK_BUFFER, 0, self.vertex_2d_buffer)
        glBeginTransformFeedback(GL_POINTS)
        glDrawArrays(GL_POINTS, 0, len(self.verts))
        glEndTransformFeedback()
        glFlush()

        r_data = glGetBufferSubData(GL_TRANSFORM_FEEDBACK_BUFFER, 0, 4*4*len(self.verts))
        self.verts2d = numpy.reshape(r_data.view('<f4'), [len(self.verts),4])

        glBindVertexArray(0)
        glUseProgram(0)

        for edge in self.edges:
            id1 = edge.isvs[0]
            id2 = edge.isvs[1]
            v1 = self.verts2d[id1]
            v2 = self.verts2d[id2]

            x1,y1,z1,w1 = v1
            x2,y2,z2,w2 = v2
                
            if w1 < 0.0 and w2 < 0.0:
                # Segment is entirely behind the camera
                edge.clipped_coords = [[-2,-2,0,1], [-2,-2,0,1]]

            if w1 < 0.0 and w2 > 0.0 or w1 > 0.0 and w2 < 0.0:
                # Segment cuts the near clip plane
                t = (w1 - 0.1) / (w1 - w2)
                xc = (t * x1) + ((1-t) * x2)
                yc = (t * y1) + ((1-t) * y2)
                zc = (t * z1) + ((1-t) * z2)
                vc = [xc, yc, zc, 0.1]

                if w1 < 0.0:
                    edge.clipped_coords = [[xc/0.1,yc/.1,zc/.1,1],
                                           [x2/w2,y2/w2,z2/w2,1]]
                else:
                    edge.clipped_coords = [[x1/w1,y1/w1,z1/w1,1],
                                           [xc/0.1,yc/.1,zc/.1,1]]

            if w1 > 0.0 and w2 > 0.0:
                # Segment is wholly in front of the camera
                edge.clipped_coords = [[x1/w1,y1/w1,z1/w1,1],
                                       [x2/w2,y2/w2,z2/w2,1]]

        
    def pack(self):
        vperm = packarray(self.verts)
        eperm = packarray(self.edges)
        fperm = packarray(self.faces)
        bperm = packarray(self.cubes)

        noblank = lambda x:x!='blank'
        for v in self.verts:
            e2 = set()
            for e in v.edges:
                if e in eperm:
                    e2.add(eperm[e])
                else:
                    e2.add(e)

            v.edges = set(filter(noblank, e2))
        for e in self.edges:
            isv = []
            for v in e.isvs:
                if v in vperm:
                    isv.append(vperm[v])
                else:
                    isv.append(v)


            e.isvs = list(filter(noblank, isv))
            facs = []
            for f in e.faces:
                if f in fperm:
                    facs.append(fperm[f])
                else:
                    facs.append(f)

            e.faces = list(filter(noblank, facs))

        for f in self.faces:
            edgs = []
            for e in f.edges:
                if e in eperm:
                    edgs.append(eperm[e])
                else:
                    edgs.append(e)
            for i, v in enumerate(f.verts):
                if v in vperm:
                    f.verts[i] = vperm[v]
            f.edges = list(filter(noblank, edgs))

            blks = []
            for b in f.cubes:
                if b in bperm:
                    blks.append(bperm[b])
                else:
                    blks.append(b)
            f.cubes = list(filter(noblank, blks))

        for b in self.cubes:
            facs = []
            for f in b.faces:
                if f in fperm:
                    facs.append(fperm[f])
                else:
                    facs.append(f)
            b.faces = list(filter(noblank, facs))

