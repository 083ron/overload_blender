#version 330
in vec2 vert;

void main() {
  gl_Position = vec4(vert.x, vert.y, 0.9, 1.0);
}
