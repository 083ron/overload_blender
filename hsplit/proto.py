import hsplit.glsetup as glsetup
from .cubecomplex import CubeComplex

import os
from .GLClasses.Camera import Camera, OrbitCamera
from .GLClasses.Regenerable import Regenerable
from .GLClasses.DrawInfo import DrawInfo
from .GLClasses.DrawList import DrawList, DrawCollection, Selection
import time
from math import sin, cos
from .glsetup import build_viewport
from .glfun import loadImage, orthoquad, screenCoords
from PIL import Image
import traceback
from .splitpreview import *
from .editpreview import *
from tko.tk_win import TkOglWin
import geometry

from vmath import *
import edit
import uv_alignment
from gldmesh import Dmesh
from marquee import Marquee
from plot2d import Plot2D
import numpy as np
import snap
import bindings
import time

written = False


class CubeProto(TkOglWin):
    rot = 0
    def __init__(self, parent, args, *args2, **kwargs):
        kwargs['bg']="black"
        TkOglWin.__init__(self, parent, *args2, **kwargs)
        #Regenerable.__init__(self)      
        self.sbo_pixels = None
        self.fbo_depth = None
        self.fbo = None
        self.args = args
        self.freeze = False
        self.mb = [0]*5
        self.lastxy = None
        self.downxy = None
        self.drag_first = True
        self.marked_flags = []
        self.rel = (.1,.1)
        self.mpos = (0,0)
        self.mods = [0,0]
        self.cursor = [0,0,0]
        self.spreview = None
        self.edit_preview = None
        self.click = False
        self.cc = None
        self.selection = Selection()
        self.marquee = Marquee()
        self.mwtime = time.time()
        #self.tmp_marquee = Plot2D()
        #self.tmp_marquee.show = True
        #self.selection.faces.trace = True
        self.selection.faces.name = "selection"
        self.marked = DrawCollection()
        self.marked.name = "marked"
        self.controls = {}
        self.show_preview = False
        
        self.drawopts = {'oldsel': -2,
                         'sel': -1,
                         'connectivity': 1}

        #self.dmeshtest = Dmesh()
        #self.dmeshtest2 = Dmesh()        
        
        if self.args.unreal:
            # UnrealEd style
            self.camera = Camera()
            bindings.camera_bindings_unreal(self)
            
        else:
            # Overload LevelEditor style
            self.camera = OrbitCamera()
            bindings.camera_bindings_ole(self)

        bindings.main_window_bindings(self)

        import hsplit.shaders as shaders
    
    def notify(self, key, value):
        self.controls[key] = value
        if key == CTL_SELECT_MODE:
            self.draw_backbuffer()
            self.tick()

    def clear_mb(self,e):
        self.mb = [0]*5       
    
        
    def on_resize(self, event, arg=None):
        if not self.glready:
            return

        if event:
            w = event.width
            h = event.height
        elif arg:
            w = arg['w']
            h = arg['h']
        else:
            raise Exception

        self.screenx=w
        self.screeny=h

        glUseProgram(shaders.vbillboardprog)
        glUniform2f(self.vp_loc_billboard, w, h)

        glUseProgram(shaders.coneprog)
        glUniform2f(self.vp_loc_cone, w, h)
        
        
        glsetup.build_viewport(self.screenx, self.screeny)

        self.build_buffers()
        self.draw_backbuffer()
        self.tick()
   
    def mouse_down(self, ev):
        self.mb[ev.num] = 1
        self.lastxy = (ev.x,ev.y)
        self.downxy = (ev.x,ev.y)

    def mouse_up(self, ev):
        self.marquee.show = False
        self.draw_backbuffer()
        self.tick()
        if self.mb[1]>0:
            self.mb[1] = 0
            self.left_click(ev)
        self.drag_first = True
            
    def mouse_wheel(self, ev):
        self.mwtime = time.time()
        if ev.delta > 0:
            self.camera.zoom_in()
        elif ev.delta < 0:
            self.camera.zoom_out()
        self.after(100, self.mwtimeout)
        self.tick()

    def mwtimeout(self):
        if time.time() - self.mwtime > 0.1:
            self.mwtime = time.time()
            self.draw_backbuffer()
    
    def mouse_move(self, ev):
        self.mb = [0]*5
        self.focus_set()
        self.lastxy = None
        item_found = self.handle_pick(ev.x, ev.y)
        self.show_preview = item_found is not None

        #if self.spreview and not self.freeze:
        #    self.spreview.hide()

        if item_found:
            self.tick()

    def mark(self, e=None):
        mode = self.controls[CTL_SELECT_MODE]
        selected = self.selection.items[mode][0]

        if selected in self.marked.items[mode]:
            self.marked.items[mode].remove(selected)
        else:
            self.marked.items[mode].append(selected)

        self.update_marked()

    def mark_unmark_all(self,e):
        if len(self.marked.items[self.controls[CTL_SELECT_MODE]]) > 0:
            self.unmark_all()
        else:
            self.mark_all()

    def mark_all(self):
        mode = self.controls[CTL_SELECT_MODE]
        if mode == SIDE:
            self.marked.faces.items = self.cc.faces.items
        elif mode == SEGMENT:
            self.marked.cubes.items = self.cc.cubes.items
        elif mode == VERTEX: 
            self.marked.verts.items = self.cc.verts.items
        elif mode == EDGE:
            self.marked.edges.items = self.cc.edges.items
        self.update_marked()

    def unmark_all(self,e=None):
        self.marked.empty()
        self.update_marked()

    def update_marked(self):
        self.marked.mark_stale((MARK,))
        self.marked.draw((MARK,))
        self.tick()

    def mark_same_texture(self):
        faces = self.cc.walk_faces(self.selection.side,
                                   lambda cc,e,f1,f2: f1.tex == f2.tex)
        self.toggle_face_list(faces)

    def face_angle(self, f1, f2):
        v1 = [self.cc.verts[v].dpos for v in f1.verts[:3]]
        v2 = [self.cc.verts[v].dpos for v in f2.verts[:3]]
        n1 = plane_normal(v1)
        n2 = plane_normal(v2)
        return dot(normalise(n1), normalise(n2))
        
    def mark_coplanar(self):
        faces = self.cc.walk_faces(self.selection.side,
                                   lambda cc,e,f1,f2: self.face_angle(f1,f2) > math.cos(D2R*15.0))
        self.toggle_face_list(faces)
        
    def toggle_face_list(self, lst):
        if self.selection.side in self.marked.faces:
            for item in lst:
                if item in self.marked.faces:
                    self.marked.faces.remove(item)
        else:
            new_faces = [f for f in lst if f not in self.marked.faces]
            self.marked.faces.items += new_faces
        self.marked.mark_stale()
        self.tick()

#   Edit menu functions:
        
    def square_marked_uvs(self, e=None):
        for side in self.marked.faces:
            side.square_uvs()
        self.cc.faces.mark_stale((RENDER,))
        self.tick()

    def flip_marked_uvs(self, e=None):
        for side in self.marked.faces:
            side.flip_uvs()
        self.cc.faces.mark_stale((RENDER,))
        self.tick()

    def align_marked_uvs_to_edges(self, e=None):
        marked_edge_ids = [e.index for e in self.marked.edges]
        for face in self.marked.faces:
            for edge_id in face.edges:
                if edge_id in marked_edge_ids:
                    uv_alignment.face_basis(self.cc, face.index, edge_id)
        self.cc.faces.mark_stale((RENDER,))
        self.tick()


    def bisectors_from_marked_edges(self, e=None):
        geometry.bisectors_from_marked_edges(self.cc, self.marked)
        self.cc.verts.mark_stale((RENDER,))
        self.cc.edge.mark_stale((RENDER,))
        self.cc.faces.mark_stale((RENDER,))
        self.tick()
        
#   Split tool:
    
    def begin_edge_split(self):
        self.controls[CTL_SELECT_MODE] = EDGE_SPLIT
        self.draw_backbuffer()
        self.tick()

    def handle_pick(self, x,y):
        """
        Take action when the corsor hovers over an object on the screen.
        Currently this is restricted to the splitter
        """
        
        line, t = self.item_under_cursor(x,y)

        # Reject if entity not in range
        item_found = line >= 0 and line <= len(self.cc.edges)
      
        if item_found and self.controls[CTL_SELECT_MODE] == EDGE_SPLIT:
            self.show_preview = True
            split_opts = (self.controls[CTL_SNAP],
                          self.controls[CTL_SPLIT_TYPE])

            if self.spreview:
                # If the old preview is not for the same line,
                # or has different options, delete it and make a new one.
                if self.spreview.line != line or \
                   self.spreview.split_opts != split_opts:
                    del self.spreview
                    self.spreview = None

            self.cursor = self.cc.edges[line].eval(t)

            dist = self.controls[CTL_SNAP_DIST]
            edge = self.cc.edges[line]
            v1 = np.array(self.cc.verts[edge.isvs[0]].dpos)
            v2 = np.array(self.cc.verts[edge.isvs[1]].dpos)
            snapinfo = snap.snap(line, v1, v2, t, dist)
                
            if self.spreview:
                self.spreview.reparam(snapinfo)
            else:
                try:
                    self.spreview = self.start_new_split(snapinfo)
                    self.cc.edges[line].start_split(self.spreview, t, 1, 0)
                except:
                    traceback.print_exc()
        else:
            self.show_preview = False
        return item_found

    def start_new_split(self, snapinfo):
        snap, split = (self.controls[CTL_SNAP],
                       self.controls[CTL_SPLIT_TYPE])

        if snap == SNAP_ON:
            if split == SPLIT_PROPORTIONAL:
                klass = SnapSplitPreview
            else:
                klass = OffsetSnapSplitPreview
        else:
            if self.controls[CTL_SPLIT_TYPE] == SPLIT_PROPORTIONAL:
                klass = SplitPreview
            elif split == SPLIT_CONSTANT:
                klass = OffsetSplitPreview

        return klass(self.cc, snapinfo,
                     (snap, split), self.controls[CTL_SNAP_DIST])
    
    def preview_edit(self, src_str, dest_str, axes):
        selected_axes = ""
        for i in range(3):
            if axes[i].get() == 1:
                selected_axes += "xyz"[i]
        src = edit.eval_expr(src_str, self.cc, self.selection, self.marked)
        dest = edit.eval_expr(dest_str, self.cc, self.selection, self.marked)
        self.edit_preview = EditPreview(self.cc, src, dest, selected_axes)
        self.tick()

    def apply_edit(self, axes):
        if self.edit_preview is not None:
            repl = self.edit_preview.replacements
            for d in repl:
                self.cc.verts[d].dpos = tuple(repl[d])
        del self.edit_preview
        self.edit_preview = None
        self.cc.mark_stale()
        self.marked.mark_stale()
        self.selection.mark_stale()
        self.tick()

            
    def dump_selection_backbuffer(self):
        if not self.glready:
            return
        glBindFramebuffer(GL_FRAMEBUFFER, self.fbo)
        data_all = glReadPixels(0, 0, self.screenx, self.screeny, GL_RGB, GL_BYTE)
        img = Image.frombytes("RGB", (self.screenx, self.screeny),data_all)
        img.save('db.png')
        glBindFramebuffer(GL_FRAMEBUFFER, 0)

    def item_under_cursor(self,x,y):
        #if self.freeze:
        #     return (-1, -1)
        if not self.glready:
            return (-1,-1)

        glBindFramebuffer(GL_FRAMEBUFFER, self.fbo)
        data = glReadPixelsf(x, self.screeny-y, 1, 1, GL_RGB)

        glBindFramebuffer(GL_FRAMEBUFFER, 0)
        for d in data:
            rgb = d[0]
            index = int(round(rgb[1]*ID_MAP))-1
            tval = rgb[0]
            if index >= 0 and index < len(self.cc.edges):
                return (index, tval)

        return (-1,-1)

    def left_click(self, e):
        #if button == 3:
        #    self.freeze = not self.freeze

        s = self.selection
        mode = self.controls[CTL_SELECT_MODE]
        item, t = self.item_under_cursor(e.x,e.y)
        if mode == EDGE_SPLIT and self.spreview:

            si = s.segment.faces.index(s.side.index)
            self.spreview.do_split()

            self.selection.reindex(self.cc)
            self.selection.side = self.cc.faces[s.segment.faces[si]]
            
            self.selection.mark_stale()
            self.marked.empty()            
            del self.spreview

            self.spreview = None
            self.cc.mark_stale()
            self.draw_backbuffer()

            self.tick()
            return
            
        elif mode in[SIDE, SEGMENT]:
            s.side = self.cc.faces[item]

        elif mode == EDGE:
            s.edge = self.cc.edges[item]

        elif mode == VERTEX:
            s.vert = self.cc.verts[item]

        s.segment = self.cc.cubes[s.side.cubes[0]]

        s.mark_stale()
        self.update_selection_label()
        self.tick()


    def update_selection_label(self):
        self.controls['LABEL'].configure(text="Selected: %d/%d/%d/%d"%self.selection.indices())
        
    def setup_opengl(self):
        self.screenx=1500
        self.screeny=900

        glsetup.setup_common(self)
        glPolygonOffset(1.0,1.0)
        self.fbo = None
        self.sbo_pixels = None

        self.build_buffers()
        self.ringtex = loadImage("hsplit/ring.png")
        self.savetex = loadImage("hsplit/save.png")
#        self.indexLoc = glGetUniformLocation(shaders.selectprog, "id")
        self.shrinkLoc = glGetUniformLocation(shaders.renderprog, "shrink")
        self.vp_loc_cone = glGetUniformLocation(shaders.coneprog, "vp")
        self.vp_loc_billboard = glGetUniformLocation(shaders.vbillboardprog, "vp")
        self.loc_MVP = glGetUniformLocation(shaders.feedbackprog, "MVP")

        self.drawopts['sloc'] = self.shrinkLoc

        self.selection.verts.register_draw_info((SELECT,), shaders.draw_info_vbill_large)
        self.selection.edges.register_draw_info((SELECT,), shaders.draw_info_highlight)
        self.selection.faces.register_draw_info((SELECT,), shaders.draw_info_highlight)
        self.selection.cubes.register_draw_info((SELECT,), shaders.draw_info_highlight)
        
        self.marked.verts.register_draw_info((MARK,), shaders.draw_info_vbill)
        self.marked.edges.register_draw_info((MARK,), shaders.draw_info_marked)
        self.marked.faces.register_draw_info((MARK,), shaders.draw_info_marked)
        self.marked.cubes.register_draw_info((MARK,), shaders.draw_info_marked)

        #self.dmeshtest.register_draw_info((RENDER,), shaders.draw_info_dmesh)
        #self.dmeshtest.load_from_dmesh_file('dmeshes/stalagmite2_obj.dmesh')

        #self.dmeshtest2.register_draw_info((RENDER,), shaders.draw_info_dmesh)
        #self.dmeshtest2.load_from_dmesh_file('dmeshes/om_full_pent_shrunk.dmesh')

        
    def build_buffers(self):
        if self.sbo_pixels:
            glDeleteRenderbuffers(1, [self.sbo_pixels])
        if self.fbo:
            glDeleteRenderbuffers(1, [self.fbo])
        if self.fbo_depth:
            glDeleteRenderbuffers(1, [self.fbo_depth])
            
        self.sbo_pixels, self.fbo, self.fbo_depth = glsetup.build_buffers(
            self.screenx, self.screeny)

    def reload_overload(self):
        fn = self.cc.orig_filename
        self.load_overload(fn)
        self.tick()

    def load_overload(self, filename):
        self.selection.empty()
        self.marked.empty()
        if self.cc is not None:
            del self.cc
        self.cc = CubeComplex()
        self.cc.load_overload(filename)
        s = self.selection

        self.marked.verts.items = [v for v in self.cc.verts if v.marked_on_load]
        self.marked.faces.items = [f for f in self.cc.faces if f.marked_on_load]
        self.marked.cubes.items = [c for c in self.cc.cubes if c.marked_on_load]
        self.update_marked()
        
        s.verts.append(self.cc.verts[0])
        s.edges.append(self.cc.edges[0])
        s.faces.append(self.cc.faces[0])
        s.cubes.append(self.cc.cubes[0])
        self.tick()
        #elf.test.items = self.cc.verts[:6]
        
    def export_overload(self, filename=None):
        self.cc.export_overload(filename=filename)
        
    def begin_draw(self, mode):
        if self.cc is None:
            if self.args.infile:
                self.load_overload(self.args.infile)
            else:
                self.cc = CubeComplex()

        if PICK in mode:
            glBindFramebuffer(GL_FRAMEBUFFER, self.fbo)
        else:
            glBindFramebuffer(GL_FRAMEBUFFER, 0)
       
        glMatrixMode(GL_MODELVIEW)
        glClearColor(0.0, 0.0, 0.0, 1.0)
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT)

        glLoadIdentity()
        self.camera.doTransform()

    def draw_ring(self, x, y, z):
        u,v,d = screenCoords(x,y,z)
        orthoquad(u,v,d, RENDER, self.ringtex, 12)

    def draw_backbuffer(self):
        if not self.glready:
            return
        mode = self.controls[CTL_SELECT_MODE]
        self.begin_draw((PICK, mode))
        self.cc.draw((PICK, mode), self.drawopts)

        self.cc.calc_clipped_edges_2d()
        
    def tick(self):
        if not self.glready:
            return
        self.begin_draw((RENDER,))

        mvmat = glGetDoublev(GL_MODELVIEW_MATRIX)
        pmat = glGetDoublev(GL_PROJECTION_MATRIX)

        #self.dmeshtest.draw((RENDER,), mvmat, pmat)
        #self.dmeshtest2.draw((RENDER,), mvmat, pmat)
        
        self.cc.draw((RENDER,), self.drawopts)
    
        glColor4f(1.0,0.0,1.0,1.0)
        
        self.draw_ring(*self.camera.position)

        glColor4f(1.0,0.25,0.0,1.0)
        self.selection.draw((SELECT,self.controls[CTL_SELECT_MODE]))

        glColor4f(0.0,0.75,1.0,1.0)
        self.marked.draw((MARK,))

        if self.marquee:
            self.marquee.draw()

        #self.tmp_marquee.draw()
        if self.spreview and self.show_preview:
            self.spreview.draw()
            
        if self.edit_preview:
            glColor4f(0.0,1.0,0.0,1.0)        
            self.edit_preview.draw(mode=(RENDER,))
            
        self._swap_buffers()

if __name__ == "__main__":

    CubeProto()


