#version 130
in vec3 a_b1;
in vec3 a_b2;
//out vec3 pos2;
uniform float k;
uniform float alpha;
uniform float snap_dist;

void main() {

  vec3 diff = abs(a_b2 - a_b1);
  float t2;

  t2 = k / length(diff);

  vec3 pos = a_b1*(1.0-t2) + a_b2*t2;
  

  gl_Position = gl_ModelViewProjectionMatrix * vec4(pos, 1.0);

  gl_FrontColor = vec4(0.0,1.0,0.0,alpha); 
  if (t2 > 1.0 || t2 < 0.0) {
    gl_FrontColor = vec4(1.0,0.0,0.0,alpha);
  }
}
