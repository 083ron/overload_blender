from .util import *
from OpenGL.GL import *
from .edge import *
import math

class Face:
    
    def __init__(self, cc, edges, verts = None,
                 uvs=None, oris=None, tex=None):
        self.cc = cc
        self.edges = edges
        self.verts = verts
        self.tex = tex
        cc.faces.append(self)
        self.cubes = []
        self.uvs = uvs
        if uvs is None:
            self.uvs = [(0.0,0.0)]*len(self.edges)
        self.split = None
        self.index = len(cc.faces)-1
        for e in edges:
            cc.edges[e].faces.append(self.index)

            #if oris is None:
        self.orient()
        #else:
        #    self.oris = oris
        self.marked_on_load = False

    def contained_in_screen_rect(self, verts2d, x,y,x2, y2):
        for vid in self.verts:
            if not self.cc.verts[vid].contained_in_screen_rect(verts2d, x,y,x2, y2): return False
        return True
            
    def overlaps_screen_rect(self, verts2d, x,y, x2, y2):
        for eid in self.edges:
            if self.cc.edges[eid].overlaps_screen_rect(verts2d, x,y,x2, y2): return True
        return False

        
    def flip(self):
        self.edges = self.edges[::-1]
        self.oris = [-i for i in self.oris[::-1]]
        tmp = self.verts[1]
        self.verts[1] = self.verts[3]
        self.verts[3] = tmp
        tmp = self.uvs[1]
        self.uvs[1] = self.uvs[3]
        self.uvs[3] = tmp
        
    def orient(self):
        """
        Sort our edes into loop order, build a list of senses
        and calculate a normal vector for this poly
        """
        nedges = [self.edges[0]]
        nuvs = [self.uvs[0]]

        self.oris = [1]
        edgecp = self.edges[:]
        edgecp.remove(self.edges[0])
        ivc = self.cc.edges[self.edges[0]].isvs[1]
        if self.verts:
            if self.verts[1] != ivc:
                ivc = self.cc.edges[self.edges[0]].isvs[0]
                self.oris = [-1]
        iverts2 = [ivc]
        while edgecp:
            for edge in edgecp:
                if ivc in self.cc.edges[edge].isvs:
                    nedges.append(edge)
                    edgecp.remove(edge)
                    if self.cc.edges[edge].isvs[0] == ivc:
                        self.oris.append(1)
                        ivc = self.cc.edges[edge].isvs[1]
                    else:
                        self.oris.append(-1)
                        ivc = self.cc.edges[edge].isvs[0]
                    iverts2.append(ivc)
                    break
            else:
                raise "Multiple Loops"
        self.edges = nedges[:]
 
        if self.verts is None:
            self.verts = [iverts2[-1]]+iverts2[:-1]

        v = []
        for i in range(4):
            v.append(self.cc.verts[self.verts[i]].dpos)

        self.normal = trinorm(v[0],v[2],v[1])

        if self.normal == (0.0,0.0,0.0):
            self.normal = trinorm(v[0],v[3],v[2])

        if self.normal == (0.0,0.0,0.0):
            print "WARNING: face %d has invalid normal"%self.index

        
    def centroid(self):
        """
        Get the face centroid as an xyz triplet
        """

        ct = [0.0,0.0,0.0]
        for edge in self.edges:
            for i in range(3):
                ct[i] += self.cc.edges[edge].eval(0.5)[i]
        ct = [a/float(len(self.edges)) for a in ct]
        return ct

    def common_edge(self, other):
        """
        Find the set of common edge IDs between this face and another
        """
        #print self, other
        return list(set(self.edges).intersection(set(other.edges)))

    def other_cube(self, icube):
        if len(self.cubes) == 2:
            if self.cubes[0] == icube:
                return self.cubes[1]
            else:
                return self.cubes[0]
        else:
            return None

    def corners(self):
        corners = []
        for i in range (4):
            e = self.cc.edges[self.edges[i]]
            if self.oris[i] == 1:
                vtx = e.isvs[0]
            else:
                vtx = e.isvs[1]
            corners.append(vtx)
        return corners

    def draw(self, mode, opts):
        """
        Draw the face
        """
        if 'connectivity' in opts:
            if len(self.cubes) > opts['connectivity']:
                return
        
        ct = self.centroid()
        corners = self.corners()
        if RENDER in mode:
            #glUniform3f(opts['sloc'], ct[0], ct[1], ct[2])
            if self.tex:
                glEnable(GL_TEXTURE_2D)
                glBindTexture(GL_TEXTURE_2D,self.tex)
            glEnable(GL_CULL_FACE)
            glCullFace(GL_FRONT)
 
            glBegin(GL_QUADS)
            for i in range (4):
                u,v  = self.uvs[i]
                vtx = corners[i]
                if math.isnan(u) or  math.isnan(v):
                    glTexCoord2f(0.0,0.0)
                else:
                    glTexCoord2f(u,v)
                glp(self.cc.verts[vtx].dpos)
            glEnd()
            glDisable(GL_CULL_FACE)

        elif PICK in mode and (SIDE in mode or SEGMENT in mode):
            glEnable(GL_CULL_FACE)
            glCullFace(GL_FRONT)
            g = (self.index+1)/ID_MAP

            glBegin(GL_QUADS)
            glColor4f(0.0,g,0.0,1.0)
            for i in range (4):
                vtx = corners[i]
                glp(self.cc.verts[vtx].dpos)
            glEnd()
            glDisable(GL_CULL_FACE)
        elif MARK in mode or SELECT in mode:
            set_select_colour(mode, SIDE)            
            glBegin(GL_LINE_LOOP)
            for i in range (4):
                vtx = corners[i]
                c =[0.0,0.0,0.0]
                for j in range(3):
                    c[j] = 0.8*self.cc.verts[vtx].dpos[j]+0.2*ct[j]
                
                glp(c)
            glEnd()
            
        if LABEL in mode:
            self.draw_label()

    def draw_label(self):
        """
        Draw the faces ID as a string.
        """
        glColor3f(1,0,1)
        glPrint(self.centroid(),str(self.index))

    def square_uvs(self):
        us, vs = zip(*self.uvs)

        # UV centroid
        uvc = (sum(us)/4.0, sum(vs)/4.0)

        # Translate as close to the origin as possible
        iu = int(uvc[0])
        iv = int(uvc[1])
        us = [u-iu for u in us]
        vs = [v-iv for v in vs]

        targetus = [0.0,1.0,1.0,0.0]
        targetvs = [0.0,0.0,1.0,1.0]
        
        # Try all four orientations and pick the closest
        mn = None
        for i in range(4):
            dst = sum([(us[(i+j)%4]-targetus[j])**2.0 +
                       (vs[(i+j)%4]-targetvs[j])**2.0 for j in range(4)])
            if mn is None:
                dstmin = dst
                imin = i
            elif dst < dstmin:
                dstmin = dst
                imin = i

        for i in range(4):
            us[(imin+i)%4] = targetus[i]
            vs[(imin+i)%4] = targetvs[i]
            
        self.uvs = zip(us,vs)
        print self.uvs

    def flip_uvs(self):
        us, vs = zip(*self.uvs)
        vs = [-v for v in vs]
        self.uvs = zip(us,vs)
        
    def split_preview(self, sp, edge, t, sns, lev):
        """
        Create a temp split edge across this face, starting
        from edge at position t.
        """
        cc = self.cc

        if self.index not in sp.faces:
            self, sp, edge, t, sns, lev
            vs = []
            #print lev*"  "+"SP face %d"%self.index
            i = self.edges.index(edge.index)
            io = (i+2)%4
            to = t
            sns2 = sns
            if self.oris[i] == self.oris[io]:
                to = 1.0-t
                sns2 = -sns

            e = cc.edges[self.edges[io]]
            vs.append(edge.split_preview(sp,t, sns, lev+1))
            vs.append(e.split_preview(sp,to, sns2,lev+1))

            if self.index not in sp.faces:
                se = Edge(self.cc, [v.index for v in vs])
                sp.faces[self.index] = se

        return sp.faces[self.index]

    def __repr__(self):
        st = ""
        st+="Face %d: e=%s, o=%s, cubes=%s, verts=%s\n"%(self.index, self.edges, self.oris, self.cubes, self.verts)
        return st
