from PIL import Image
from OpenGL.GL import *
from OpenGL.GLU import gluPerspective, gluProject
#from OpenGL.GLUT import *
from hsplit.Constants import *
import hsplit.shaders as shaders
from .config import config
import os
texs = {}

def glPrint(ct, string):
    pass
    #    prog = glGetIntegerv(GL_CURRENT_PROGRAM)
#    glUseProgram(0)
    
#    glDisable(GL_LIGHTING)
#    glRasterPos3f(ct[0],ct[1],ct[2]);
#    for char in string:
#        glutBitmapCharacter(GLUT_BITMAP_9_BY_15, ord(char));
#    glEnable(GL_LIGHTING)
#    glUseProgram(prog)

def getTexture(imageName):
    im = Image.open(imageName)
    try:
        ix, iy, image = im.size[0], \
                        im.size[1], \
                        im.tobytes("raw", "RGBA", 0, -1)
    except SystemError:
        ix, iy, image = im.size[0], \
                        im.size[1], \
                        im.tobytes("raw", "RGBX", 0, -1)

    ID = glGenTextures(1)
    glBindTexture(GL_TEXTURE_2D, ID)
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR)
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR)
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT)
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT)
    glPixelStorei(GL_UNPACK_ALIGNMENT,1)
    glTexImage2D(GL_TEXTURE_2D, 0, 4, \
                 ix, iy, 0, GL_RGBA, GL_UNSIGNED_BYTE, image)
    return ID

def beginOrthoMode():
    vPort = glGetIntegerv(GL_VIEWPORT)
    glMatrixMode(GL_PROJECTION)
    glPushMatrix()
    glLoadIdentity()
    try:
        glOrtho(0, vPort[2], 0, vPort[3], -1, 1)
    except:
        print(vPort)
    glMatrixMode(GL_MODELVIEW)
    glPushMatrix()
    glLoadIdentity()

def endOrthoMode():
    glPopMatrix()
    glMatrixMode(GL_PROJECTION)
    glPopMatrix()
    glMatrixMode(GL_MODELVIEW)

def screenCoords(x,y,z):
    model = glGetDoublev( GL_MODELVIEW_MATRIX )
    proj = glGetDoublev( GL_PROJECTION_MATRIX )
    view = glGetIntegerv( GL_VIEWPORT )
    u,v,d = gluProject(x,y,z,model, proj, view)
    return u,v,d

def orthoquad(u,v,d, mode, texid,size):
    if mode == RENDER:
        glUseProgram(shaders.textureprognofog)
        glEnable(GL_TEXTURE_2D)
        glEnable(GL_BLEND)
        glBindTexture(GL_TEXTURE_2D,texid)
    else:
        glUseProgram(shaders.selectprog)
        
    beginOrthoMode()
    glDisable(GL_LIGHTING)
#    glDisable(GL_DEPTH_TEST)
#    glDepthMask(GL_FALSE)

    glBegin(GL_QUADS)
    glTexCoord2f(0.0,0.0)
    glVertex3fv((u-size,v-size,-d))
    glTexCoord2f(1.0,0.0)
    glVertex3fv((u+size,v-size,-d))
    glTexCoord2f(1.0,1.0)
    glVertex3fv((u+size,v+size,-d))
    glTexCoord2f(0.0,1.0)
    glVertex3fv((u-size,v+size,-d))
    glEnd()

 #   glDepthMask(GL_TRUE)
 #   glEnable(GL_DEPTH_TEST)
    glDisable(GL_TEXTURE_2D)
    glDisable(GL_BLEND)
    glEnable(GL_LIGHTING)
    endOrthoMode()

def loadOverloadTexture(tex_name):
    print "LOT:", tex_name
    try:
        i = loadImage(os.path.join(config['LEVEL_TEXTURES'],tex_name+".png"))
    except:
        try:
            i = loadImage(os.path.join(config['LEVEL_TEXTURES'],'..','DecalTextures',tex_name+".png"))
        except:
            i = loadImage("hsplit/placeholder.png")
    return i
    
def loadImage(fname):
    print "LI:",fname
    if fname in texs:
        #print "c"
        return texs[fname]
    else:
        ID = getTexture(fname)
        texs[fname]=ID
        #print ID
        return ID

def printOpenGLError():
    err = glGetError()
    if (err != GL_NO_ERROR):
        print('GLERROR: ', gluErrorString(err))
