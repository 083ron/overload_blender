from edit import *
from vmath import *
from util import *
def bisectors_from_marked_edges(cc, marked):
    marked_vert_ids = [v.index for v in marked.verts]

    for edge in marked.edges:
        v = edge.isvs[0]
        other_v = edge.isvs[1]
        if v not in marked_vert_ids:
            v = edge.isvs[1]
            other_v = edge.isvs[0]
            if v not in marked_vert_ids:
                return None

        exf = cc.exterior_edge_parents(edge)
        p = []
        for face in exf:
            for edge_id2 in cc.verts[v].edges:
                if edge_id2 in face.edges and edge_id2 != edge.index:
                    edge2 = cc.edges[edge_id2]
                    v1 = cc.verts[edge2.isvs[0]].dpos
                    v2 = cc.verts[edge2.isvs[1]].dpos
                    v3 = cc.verts[other_v].dpos
                    p.append(VertexGroup(v1, v2, sadd(v1, trinorm(v1,v2,v3))))

        p3 = VertexGroup(v1,v2,v3)
        v4 = VertexGroup(cc.verts[other_v].dpos)
        print p3,v4,((p[0]/p[1])^p3), v4|((p[0]/p[1])^p3)

        print "A",cc.verts[other_v].dpos, "B", (v4|((p[0]/p[1])^p3)).dpos
        cc.verts[other_v].dpos = (v4|((p[0]/p[1])^p3)).dpos[0]
