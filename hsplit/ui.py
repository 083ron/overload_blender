from Tkinter import *
import tkFileDialog
from .config import config
import ttk
import os
from hsplit.win32extras import borderless
from .Constants import *

def foo(*args):
    print("Button press", args)

default = {'side':TOP, 'fill':X, 'padx':6, 'pady':3}

def hello():
    print "hello!"

def open_menu(ogl):
    opendir = config['OPEN_LOCATION']
    openfile = tkFileDialog.askopenfilename(initialdir=opendir)
    if openfile != "":
        config['OPEN_LOCATION'] = os.path.dirname(openfile)
        ogl.load_overload(openfile)
        config.write()

def saveas_menu(ogl):
    savedir = config['OPEN_LOCATION']
    savefile = tkFileDialog.asksaveasfilename(initialdir=savedir)
    if savefile != "":
        config['OPEN_LOCATION'] = os.path.dirname(savefile)
        ogl.export_overload(filename=savefile)
        config.write()

def texfolder_menu(ogl):
    texdir = config['LEVEL_TEXTURES']
    texdir = tkFileDialog.askdirectory(initialdir=texdir)
    if texdir != "":
        config['LEVEL_TEXTURES'] = texdir
        config.write()

def tab_next(s2, ogl):
    s2.next()
    return "break"

def tab_prev(s2, ogl):
    s2.prev()
    return "break"
    
def build_ui(args):
    tk = Tk()

    frame1 = Frame(tk, width=300, height=20, bg="#111516")
    frame1.pack(side=LEFT, fill=Y)

    #frame2 = Frame(tk, width=10, height=30, bg="#111516")
    #frame2.pack(side=TOP, fill=X)
    
    from hsplit.proto import CubeProto
    ogl = CubeProto(tk, args, width=1500, height=900, app_title="0beron's Cube Wrangler")
    ogl.pack(side=RIGHT, fill=BOTH, expand=YES)

    menubar = Menu(tk)
    filemenu = Menu(menubar, tearoff=0)
    filemenu.add_command(label="Open...",    command=lambda:open_menu(ogl))
    filemenu.add_command(label="Reload",     command=ogl.reload_overload)
    filemenu.add_command(label="Save",       command=ogl.export_overload)
    filemenu.add_command(label="Save As...", command=lambda:saveas_menu(ogl))
    filemenu.add_separator()
    filemenu.add_command(label="Set Texture Folder...", command=lambda:texfolder_menu(ogl))
    filemenu.add_separator()
    filemenu.add_command(label="Exit",       command=tk.quit)

    editmenu = Menu(menubar, tearoff=0)
    editmenu.add_command(label="Square Up Marked UVs",
                          command=ogl.square_marked_uvs)

    editmenu.add_command(label="Flip Marked UVs",
                          command=ogl.flip_marked_uvs)

    editmenu.add_command(label="Align marked side UVs along marked Edges",
                          command=ogl.align_marked_uvs_to_edges)

    editmenu.add_command(label="Bisectors from marked edges",
                          command=ogl.bisectors_from_marked_edges)
    
    debugmenu = Menu(menubar, tearoff=0)
    debugmenu.add_command(label="Dump Backbuffer",
                          command=ogl.dump_selection_backbuffer)

    menubar.add_cascade(label="File", menu=filemenu)
    menubar.add_cascade(label="Edit", menu=editmenu)
    menubar.add_cascade(label="Debug", menu=debugmenu)
    tk.config(menu=menubar)

    selector_style = { 'width':150,
                       'highlightthickness':0,
                       'height':30,
                       'bd':0 }
    selector_style_wide = selector_style.copy()
    selector_style_wide['width'] = 300
    
    ff = FloatingFrame(tk, ogl, 30, 30)
    s2 = Selector(ff, ogl, CTL_SELECT_MODE,
                  label="Mode: ",
                  options=[('SEGMENT', SEGMENT),
                           ('SIDE', SIDE),
                           ('EDGE', EDGE),
                           ('VERTEX', VERTEX)],
                  **selector_style)
    
    snap_dist_control = Selector(ff, ogl, CTL_SNAP_DIST,
                                 label="Snap Distance: ",
                                 options=[('2', 0.2),
                                          ('1', 0.1),
                                          ('0.5', 0.05),
                                          ('0.25', 0.025),
                                          ('0.125', 0.0125),
                                          ('0.0675', 0.00675)],
                                 **selector_style)
    snap_dist_control.index = 1

    snap_control = Selector(ff, ogl, CTL_SNAP, label="Snap: ",
                            options=[('OFF', SNAP_OFF),
                                     ('ON', SNAP_ON)],
                                 **selector_style)

    split_type_control = Selector(ff, ogl, CTL_SPLIT_TYPE, label="Split Type: ",
                                  options=[('PROPORTIONAL', SPLIT_PROPORTIONAL),
                                           ('CONSTANT', SPLIT_CONSTANT)],
#                                           ('REVERSE CONSTANT', SPLIT_REVERSE_CONSTANT)],
                                  **selector_style_wide)
    
    vcx = IntVar()
    vcy = IntVar()
    vcz = IntVar()   
    axes = (vcx, vcy, vcz)
    
    s2.grid(row=0, column=0)
    selected_lbl = Label(ff, text="Selected: 0/0/0/0", anchor=E)
    #selected_lbl.configure(bg="blue")
    selected_lbl.grid(row=0, column=1)
    ogl.controls['LABEL'] = selected_lbl

    snap_control.grid(row=1, column=0)
    snap_dist_control.grid(row=1, column=1)
    split_type_control.grid(row=2, column=0, columnspan = 2)

    ff.geometry("284x90")
    
    spacer = Frame(frame1, height=3, bg="#111516")
    spacer.pack(side=TOP)
    
    split_btn = Button(frame1, text="Split Tool",
                         command=ogl.begin_edge_split)
    split_btn.pack(**default)
    
    sep = ttk.Separator(frame1)
    sep.pack(**default)
    
    geometry_edit_cmd(frame1, ogl, axes, "Project Marked Verts to\n Selected Side", "vm|ss", "vm")
    geometry_edit_cmd(frame1, ogl, axes, "Project Marked Sides to\n Selected Side", "sm|ss", "sm")
    geometry_edit_cmd(frame1, ogl, axes, "Project Marked Sides to\n Selected Side\nalong Selected Edge", "sm|(ss,es)", "sm")

    sep = ttk.Separator(frame1)
    sep.pack(**default)
    eframe = Frame(frame1, bg="#111516")
   
    src_label = Label(eframe, text="New Location:", fg="#cccccc", bg="#111516", anchor=W)
    src_label.pack(fill=X)
    src_str = Entry(eframe)
    src_str.pack()

    dest_label = Label(eframe, text="Objects to Move:", fg="#cccccc", bg="#111516", anchor=W)
    dest_label.pack(fill=X)
    dest_str = Entry(eframe)
    dest_str.pack()

    eframe.pack(**default)
    
    preview_btn = Button(frame1, text="Preview Edit",
                         command=lambda: ogl.preview_edit(src_str.get(),dest_str.get(),axes))
    preview_btn.pack(**default)

    apply_btn = Button(frame1, text="Apply Edit", command=lambda: ogl.apply_edit(axes))
    apply_btn.pack(**default)
    
   
    cframe = Frame(frame1, bg="#111516")
    chk_x = Checkbutton(cframe, text="X", variable = vcx)
    chk_y = Checkbutton(cframe, text="Y", variable = vcy)
    chk_z = Checkbutton(cframe, text="Z", variable = vcz)

    chk_x.select()
    chk_y.select()
    chk_z.select()

    chk_x.pack(side=LEFT)
    chk_y.pack(side=LEFT)
    chk_z.pack(side=LEFT)

    cframe.pack()
    
    ff.wm_attributes("-alpha",0.5)
    tk.bind("<KeyPress-Tab>", lambda e:tab_next(s2, ogl))
    tk.bind("<Shift-Tab>", lambda e:tab_prev(s2, ogl))
    
    ogl.after(100,ogl.setup_opengl)
    ogl.after(150,ogl.tick)
    ogl.after(160,ogl.draw_backbuffer)
    ogl.after(5,s2.draw)
    ogl.after(10,lambda : borderless(ff))

    tk.mainloop()


    
def geometry_edit_cmd(master, ogl, axes, name, src, dest):
    proj_to_side = Label(master, text=name, fg="#cccccc", bg="#111516")
    proj_to_side.pack(side=TOP)
    gframe = Frame(master, bg="#111516")
    preview = Button(gframe, text="Preview",
                         command=lambda: ogl.preview_edit(src,dest,axes))
    aply = Button(gframe, text="Apply",
                         command=lambda: ogl.apply_edit(axes))
    preview.pack(side=LEFT, pady=3, padx=6)
    aply.pack(side=LEFT, pady=3, padx=6)
    gframe.pack(side=TOP)

class FloatingFrame(Toplevel):

    def __init__(self, master, geom_parent, xoff, yoff, *args, **kwargs):
        Toplevel.__init__(self, *args, **kwargs)
        self.geom_parent = geom_parent
        self.xoff = xoff
        self.yoff = yoff
        self.wm_attributes("-alpha",0.9)
        self.transient(master=master)
        master.winfo_toplevel().bind("<Configure>", self.reposition, "+")

    def reposition(self, e=None):
        x = int(self.geom_parent.winfo_rootx())+self.xoff
        y = int(self.geom_parent.winfo_rooty())+self.yoff
        self.wm_geometry("+%s+%s"%(x,y))

class Selector(Canvas):

    def __init__(self, master, target, name, *args, **kwargs):
        self.options = ['---']
        self.target = target
        self.name = name
        if 'options' in kwargs:
            self.options = kwargs['options']
            del kwargs['options']

        self.label = ""
        if 'label' in kwargs:
            self.label = kwargs['label']
            del kwargs['label']
            
        Canvas.__init__(self, master, *args, **kwargs)
        self.master = master
        self.index = 0
        #self.configure(bg="red")
        self.bind("<1>", lambda e:self.next())
        self.bind("<3>", lambda e:self.prev())
        self.bind("<Configure>", self.draw)
        self.target.notify(self.name, self.get_selected())
        
    def draw(self,e=None):
        self.delete('all')
        self.create_rectangle(2,2,self.winfo_width()-3,self.winfo_height()-3,width=1,tags="border")
        self.create_text(6,15,text=self.label+self.get_label_value(), tags="label", anchor=W)

    def next(self):
        self.index += 1
        self.update()

    def prev(self):
        self.index -= 1
        self.update()
        
    def update(self):
        self.index = self.index%len(self.options)
        self.itemconfig("label", text=self.label+self.get_label_value())
        self.draw()
        self.target.notify(self.name, self.get_selected())

    def get_label_value(self):
        item = self.options[self.index]
        if type(item) == tuple:
            return item[0]
        else:
            return item

    def get_selected(self):
        item = self.options[self.index]
        if type(item) == tuple:
            return item[1]
        else:
            return item

