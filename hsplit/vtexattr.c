#version 330

attribute vec3 vert;
attribute vec2 uV;
uniform mat4 mvMatrix;
uniform mat4 pMatrix;
out vec2 UV;
      
void main() {
  gl_Position = pMatrix * mvMatrix * vec4(vert, 1.0);
  UV = uV;
}
