import wx
import os
from overload.obj2dmesh import load_obj, write_dmesh, AUTO

class DropTarget(wx.FileDropTarget):

    def __init__(self, panel):
        wx.FileDropTarget.__init__(self)
        self.panel = panel
 
    def OnDropFiles(self, x, y, files):
        tx = self.panel.txt
        tx.SetInsertionPointEnd()
        tx.WriteText("\n%d file(s) dropped.\n\n" % (len(files)))

        try:
            scale = float(self.panel.scale.GetLineText(0))
        except:
            scale = 1.0

        for filepath in files:
            ext = os.path.splitext(filepath)[1]
            if ext == ".obj":
                tx.WriteText("Converting OBJ "+ filepath + '\n')
                try:
                    obj = load_obj(filepath)
                    out = write_dmesh(obj, style=AUTO, scale=scale, outputfile="")
                    tx.WriteText("Dmesh written to "+ out + '\n\n')
                except:
                    tx.WriteText("Failed.\n")
            else:
                tx.WriteText("Skipped file %s (no .obj extension).\n"%filepath)
                    
        return True
 

class Obj2DMeshPanel(wx.Panel):

    def __init__(self, parent):
        wx.Panel.__init__(self, parent=parent)
 
        drop_target = DropTarget(self)
        self.txt = wx.TextCtrl(self, style=wx.HSCROLL|wx.TE_READONLY|wx.TE_MULTILINE)
        self.txt.SetDropTarget(drop_target)

        self.scale = wx.TextCtrl(self)
        
        lbl_scale = wx.StaticText(self, label="Scale Factor (leave blank for default of 1.0):")
        lbl = wx.StaticText(self, label="Drag obj files for conversion here:")
        sizerH = wx.BoxSizer(wx.HORIZONTAL)
        sizerH.Add(lbl_scale, 0, wx.ALL, 4)
        sizerH.Add(self.scale, 0, wx.ALL, 4)
        sizer = wx.BoxSizer(wx.VERTICAL)
        sizer.Add(sizerH, 0, wx.ALL, 4)
        sizer.Add(lbl, 0, wx.ALL, 4)
        sizer.Add(self.txt, 1, wx.EXPAND|wx.ALL, 4)
        self.SetSizer(sizer)
 

class Obj2DMeshFrame(wx.Frame):

    def __init__(self):
        wx.Frame.__init__(self, parent=None, title="OBJ2DMESH", size=(1000,500))
        panel = Obj2DMeshPanel(self)
        self.Show()

if __name__ == "__main__":
    app = wx.App(False)
    frame = Obj2DMeshFrame()
    app.MainLoop()
