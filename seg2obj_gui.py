import wx
import os
import json
import traceback
from overload.seg2obj import write_obj
from overload.seg2obj_config import config

class DropTarget(wx.FileDropTarget):

    def __init__(self, panel):
        wx.FileDropTarget.__init__(self)
        self.panel = panel
 
    def OnDropFiles(self, x, y, files):
        tx = self.panel.txt
        tx.SetInsertionPointEnd()
        tx.WriteText("\n%d file(s) dropped.\n\n" % (len(files)))

        try:
            scale = float(self.panel.scale.GetLineText(0))
        except:
            scale = 1.0

        for filepath in files:
            ext = os.path.splitext(filepath)[1]
            segs = self.panel.rb_segs.GetValue()
            sides = self.panel.rb_sides.GetValue()

            if ext == ".overload":
                if segs:
                    tx.WriteText("Converting Marked segments in "+ filepath + '\n')
                else:
                    tx.WriteText("Converting Marked sides in "+ filepath + '\n')
                try:
                    fp = open(filepath, "r")
                    ovl_json = json.load(fp)
                    out, msg = write_obj(filepath, ovl_json, segs, sides, outputfile="")
                    tx.WriteText(msg+"\n")
                    tx.WriteText("Obj written to "+ out + '\n\n')
                except:
                    tx.WriteText("Failed:\n")
                    tx.WriteText(traceback.format_exc())
            else:
                tx.WriteText("Skipped file %s (no .overload extension).\n"%filepath)
                    
        return True
 

class Seg2ObjPanel(wx.Panel):

    def __init__(self, parent):
        wx.Panel.__init__(self, parent=parent)
 
        drop_target = DropTarget(self)
        self.txt = wx.TextCtrl(self, style=wx.HSCROLL|wx.TE_READONLY|wx.TE_MULTILINE)
        self.txt.SetDropTarget(drop_target)

        #self.scale = wx.TextCtrl(self)
        self.rb_sides = wx.RadioButton(self, label="Marked Sides", style=wx.RB_GROUP)
        self.rb_segs = wx.RadioButton(self, label="Marked Segments (exterior sides)")
        lbl = wx.StaticText(self, label="Drag overload files for conversion here:")
        sizerH = wx.BoxSizer(wx.HORIZONTAL)
        sizerH.Add( self.rb_sides, 0, wx.ALL, 4)
        sizerH.Add( self.rb_segs, 0, wx.ALL, 4)
        sizer = wx.BoxSizer(wx.VERTICAL)
        sizer.Add(sizerH, 0, wx.ALL, 4)
        sizer.Add(lbl, 0, wx.ALL, 4)
        sizer.Add(self.txt, 1, wx.EXPAND|wx.ALL, 4)
        self.SetSizer(sizer)

class Seg2ObjFrame(wx.Frame):

    def __init__(self):
        wx.Frame.__init__(self, parent=None, title="SEG2OBJ", size=(1000,500))
        self.panel = Seg2ObjPanel(self)
        self.Show()

if __name__ == "__main__":
    app = wx.App(False)
    frame = Seg2ObjFrame()

    ltex = config['LEVEL_TEXTURES']

    if not os.path.exists(os.path.join(ltex, "Ice_01a.png")):
        dlg = wx.TextEntryDialog(frame, 'Level Textures Location:','Set Level Textures Location')
        dlg.SetValue("")
        if dlg.ShowModal() == wx.ID_OK:
            config['LEVEL_TEXTURES'] = dlg.GetValue()
            config.write()
        else:
            config['LEVEL_TEXTURES'] = ""
        dlg.Destroy()

    ltex = config['LEVEL_TEXTURES']
    if os.path.exists(os.path.join(ltex, "Ice_01a.png")):
        frame.panel.txt.WriteText("Found Overload level textures in %s\n"%ltex)
    else:
        frame.panel.txt.WriteText("Overload level textures not found in %s\n"%ltex)
        
    app.MainLoop()
